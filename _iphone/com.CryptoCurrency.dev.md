---
wsId: 
title: "Crypto App - Widgets, Alerts"
altTitle: 
authors:

appId: com.CryptoCurrency.dev
appCountry: us
idd: 1339112917
released: 2018-02-21
updated: 2021-06-22
version: "2.3.2"
stars: 4.74612
reviews: 1418
size: 70728704
website: https://thecrypto.app
repository: 
issue: 
icon: com.CryptoCurrency.dev.jpg
bugbounty: 
verdict: wip
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


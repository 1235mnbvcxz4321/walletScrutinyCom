---
wsId: etoro
title: "eToro Cryptocurrency Trading"
altTitle: 
authors:
- leo
appId: com.etoro.openbook
appCountry: 
idd: 674984916
released: 2017-06-26
updated: 2021-08-08
version: "339.0.0"
stars: 4.14727
reviews: 3816
size: 144707584
website: http://www.etoro.com
repository: 
issue: 
icon: com.etoro.openbook.jpg
bugbounty: 
verdict: custodial
date: 2021-04-25
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: etoro
providerFacebook: eToro
providerReddit: 

redirect_from:

---

Etoro is used to speculate on assets more than to actually transfer them but in
the case of Bitcoin, according to
[the Help Center](https://www.etoro.com/customer-service/help/1422157482/can-i-withdraw-my-cryptocurrencies-from-the-platform/)
you can actually send Bitcoins from this app ... if you are in the right
jurisdiction ...
[further restrictions apply](https://etoro.nanorep.co/widget/widget.html?kb=156763&account=etoro#onloadquestionid=1306615492) ...

So all in all this could pass as a custodial app.

As a custodial app it is **not verifiable**.

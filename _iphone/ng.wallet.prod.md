---
wsId: WalletsAfrica
title: "Wallets Africa"
altTitle: 
authors:
- kiwilamb
- leo
appId: ng.wallet.prod
appCountry: 
idd: 1280830303
released: 2017-10-14
updated: 2021-08-09
version: "2.479"
stars: 4.33333
reviews: 87
size: 63284224
website: http://wallets.africa
repository: 
issue: 
icon: ng.wallet.prod.jpg
bugbounty: 
verdict: custodial
date: 2021-04-24
signer: 
reviewArchive:


providerTwitter: walletsafrica
providerLinkedIn: 
providerFacebook: walletsafrica
providerReddit: 

redirect_from:

---

Wallets Africa is quite a broad product, the lack of source code makes it
impossible to verify this app and there are no statements on their website as to
management of private keys.

Our verdict: This “wallet” is probably custodial and therefore is
**not verifiable**.

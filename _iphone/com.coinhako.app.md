---
wsId: 
title: "Coinhako: Bitcoin Wallet Asia"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.coinhako.app
appCountry: 
idd: 1137855704
released: 2016-09-04
updated: 2021-08-10
version: "3.6.0"
stars: 3.76923
reviews: 13
size: 48232448
website: https://www.coinhako.com
repository: 
issue: 
icon: com.coinhako.app.jpg
bugbounty: 
verdict: custodial
date: 2021-04-24
signer: 
reviewArchive:


providerTwitter: coinhako
providerLinkedIn: coinhako
providerFacebook: coinhako
providerReddit: 

redirect_from:

---

Having a scan over the providers website and faq articles does not reveal any
claims regarding the management of private keys.
We would have to assume this wallet is custodial.

Our verdict: This “wallet” is probably custodial and therefore is **not verifiable**.

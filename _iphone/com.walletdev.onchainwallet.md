---
wsId: HuobiWallet
title: "Huobi Wallet - Safe & Reliable"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.walletdev.onchainwallet
appCountry: 
idd: 1433883012
released: 2018-09-29
updated: 2021-08-13
version: "2.9.0"
stars: 4.54082
reviews: 294
size: 102219776
website: https://www.huobiwallet.com/en/
repository: 
issue: 
icon: com.walletdev.onchainwallet.jpg
bugbounty: 
verdict: nosource
date: 2021-04-20
signer: 
reviewArchive:


providerTwitter: HuobiWallet
providerLinkedIn: 
providerFacebook: HuobiWallet
providerReddit: 

redirect_from:

---

From the description on the App Store the wallet provider clearly states the
private keys are in control of the user:

> Huobi Wallet users have sole control over their own private keys and thus have
  full control over their assets. There are no third parties involved in
  management of private keys.

However the non-custodial claims of the provider cannot be verified as no source
code is available.

Our verdict: This 'wallet' is possibly non-custodial but does not provide public
source and therefore is **not verifiable**.

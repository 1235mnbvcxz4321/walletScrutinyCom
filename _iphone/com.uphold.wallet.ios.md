---
wsId: UpholdbuyandsellBitcoin
title: "Uphold: buy and sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.uphold.wallet.ios
appCountry: 
idd: 1101145849
released: 2016-04-19
updated: 2021-08-17
version: "4.17.6"
stars: 3.59161
reviews: 4863
size: 69816320
website: https://uphold.com
repository: 
issue: 
icon: com.uphold.wallet.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: UpholdInc
providerLinkedIn: upholdinc
providerFacebook: UpholdInc
providerReddit: 

redirect_from:

---

This app appears to be an interface to a custodial trading platform. In the
App Store description we read:

> - Uphold is fully reserved. Unlike banks, we don’t loan out your money. To
    prove it, we publish our holdings in real-time.

If they hold your money, you don't. As a custodial service this app is **not
verifiable**.

---
wsId: bithumbko
title: "Bithumb"
altTitle: 
authors:
- leo
appId: com.btckorea.bithumb
appCountry: 
idd: 1299421592
released: 2017-12-05
updated: 2021-08-19
version: "1.6.2"
stars: 1.95652
reviews: 23
size: 84903936
website: https://en.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: bithumb
providerReddit: 

redirect_from:

---

This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.

---
wsId: Luno
title: "Luno Bitcoin & Cryptocurrency"
altTitle: 
authors:
- leo
appId: za.co.Bitx
appCountry: 
idd: 927362479
released: 2014-11-03
updated: 2021-08-11
version: "7.20.0"
stars: 4.42361
reviews: 3338
size: 98608128
website: https://www.luno.com
repository: 
issue: 
icon: za.co.Bitx.jpg
bugbounty: 
verdict: custodial
date: 2021-05-12
signer: 
reviewArchive:


providerTwitter: LunoGlobal
providerLinkedIn: lunoglobal
providerFacebook: luno
providerReddit: 

redirect_from:

---

This app's Android version had a clear statement about being custodial in the
Play Store description but on the App Store, no claims are made that let you
infer the type of custody.

On the website under [security](https://www.luno.com/en/security) we found the
same claim as in the Android review though:

> **Deep freeze storage**<br>
  The majority of customer Bitcoin funds are kept in what we call “deep freeze”
  storage. These are multi-signature wallets, with private keys stored in
  different bank vaults.

which again is a clear statement of them holding your funds. As a custodial
service, this app is **not verifiable**.

---
wsId: Coinpal
title: "Coinpal - Bitcoin Wallet"
altTitle: 
authors:
- kiwilamb
appId: me.coinpal.xapp
appCountry: 
idd: 1351428916
released: 2018-06-26
updated: 2018-06-26
version: "1.0"
stars: 5
reviews: 5
size: 77613056
website: https://coinpal.me
repository: 
issue: 
icon: me.coinpal.xapp.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-06-04
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: coinpalapp
providerLinkedIn: coinpal
providerFacebook: coinpal.me
providerReddit: 

redirect_from:

---

The providers [App store description](https://apps.apple.com/us/app/coinpal-bitcoin-wallet/id1351428916) states the customer is in control of the private keys.

> Device-based security: all private keys are stored locally, not in the cloud

This is a non-cusdodial wallet however

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

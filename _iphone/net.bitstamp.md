---
wsId: Bitstamp
title: "Bitstamp – crypto exchange app"
altTitle: 
authors:
- leo
appId: net.bitstamp
appCountry: 
idd: 1406825640
released: 2019-01-30
updated: 2021-07-29
version: "2.3.2"
stars: 4.7925
reviews: 4723
size: 98003968
website: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.jpg
bugbounty: 
verdict: custodial
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:

---

Just like on Play Store {% include walletLink.html wallet='android/net.bitstamp.app' %}, they claim:

> Convenient, but secure<br>
  ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.

---
wsId: ZelCore
title: "ZelCore"
altTitle: 
authors:
- leo
appId: com.zelcash.zelcore
appCountry: 
idd: 1436296839
released: 2018-09-23
updated: 2021-08-13
version: "v5.0.0"
stars: 4.31579
reviews: 57
size: 60203008
website: https://zelcore.io
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


---
wsId: ezdefi
title: "ezDeFi-Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.ezdefi.nexty
appCountry: 
idd: 1492046549
released: 2019-12-18
updated: 2021-08-03
version: "0.3.8"
stars: 4.5
reviews: 16
size: 63313920
website: https://ezdefi.com/
repository: 
issue: 
icon: com.ezdefi.nexty.jpg
bugbounty: 
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: ezDeFi
providerLinkedIn: 
providerFacebook: ezdefi
providerReddit: 

redirect_from:

---

Features like

> By eliminating encryption phrase, new users can simply make purchases with
  just a wallet password or biometric.

sound very custodial. Although this is

> A new Ez Mode [...] to make cryptocurrencies accessible to new users.

there are no explicit claims about the app being non-custodial otherwise, which
is why we have to assume it's custodial all the way and thus **not verifiable**.

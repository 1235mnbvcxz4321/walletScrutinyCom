---
wsId: lykkex
title: "Lykke: Buy. Sell. Invest."
altTitle: 
authors:
- leo
appId: com.lykkex.Lykke-Wallet
appCountry: de
idd: 1112839581
released: 2016-05-20
updated: 2021-05-30
version: "7.36"
stars: 4.54167
reviews: 24
size: 118240256
website: https://lykke.com/wallet
repository: 
issue: 
icon: com.lykkex.Lykke-Wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-03-09
signer: 
reviewArchive:


providerTwitter: Lykke
providerLinkedIn: lykke
providerFacebook: LykkeCity
providerReddit: lykke

redirect_from:

---

Lykke appears to be a Swiss exchange:

> Lykke is your gateway to the future of investment, allowing you to securely
  buy, sell and store Bitcoin & other cryptocurrencies on the fully regulated
  Swiss-based Lykke Exchange & wallet.

but there is no claim about the wallet being self-custodial or even hints like
references to industry standards in self-custodial wallets.

We only see

> - Store your Crypto-Assets safely in our secure Blockchain private wallet

but that could mean anything. For now we assume this app is the interface to an
account on the custodial exchange of the same name which makes it
**not verifiable**.

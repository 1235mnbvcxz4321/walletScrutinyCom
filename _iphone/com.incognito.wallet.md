---
wsId: incognito
title: "Incognito crypto wallet"
altTitle: 
authors:
- leo
appId: com.incognito.wallet
appCountry: 
idd: 1475631606
released: 2019-08-21
updated: 2021-08-02
version: "4.5.0"
stars: 3.96341
reviews: 82
size: 50023424
website: https://incognito.org
repository: https://github.com/incognitochain/incognito-wallet
issue: 
icon: com.incognito.wallet.jpg
bugbounty: 
verdict: wip
date: 2020-12-21
signer: 
reviewArchive:


providerTwitter: incognitochain
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of {% include walletLink.html wallet='android/com.incognito.wallet' %}
on Android.

Here we read the same claim as for Android:

> Don’t leave yourself exposed. Go Incognito. It’s non-custodial, decentralized,
  and completely open-source.

and get to the same conclusion about the state of their source code: Without
build instructions and all the files, this app cannot be reproduced and remains
**not verifiable**.

---
wsId: cobowallet
title: "Cobo Crypto Wallet: BTC & DASH"
altTitle: 
authors:
- leo
appId: cobo.wallet
appCountry: 
idd: 1406282615
released: 2018-08-05
updated: 2021-08-13
version: "5.12.0"
stars: 3
reviews: 2
size: 129631232
website: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


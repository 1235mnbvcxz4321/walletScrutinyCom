---
wsId: CoinPayments
title: "CoinPayments - Crypto Wallet"
altTitle: 
authors:
- leo
appId: net.coinpayments.coinpaymentsapp
appCountry: 
idd: 1162855939
released: 2019-02-07
updated: 2021-07-19
version: "2.3.1"
stars: 3.97368
reviews: 38
size: 144096256
website: https://www.coinpayments.net/
repository: 
issue: 
icon: net.coinpayments.coinpaymentsapp.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: CoinPaymentsNET
providerLinkedIn: coinpayments-inc
providerFacebook: CoinPayments
providerReddit: 

redirect_from:

---

The description is not very clear but sounds a bit like this app is custodial.

Their website is mainly about payment integrations for merchants but also has a
[page dedicated to the mobile apps](https://www.coinpayments.net/apps) which is
not very detailed but our guess for now is that

> Now you can easily access your CoinPayments account to send and receive coins,
  accept POS payments in person, and exchange many of our coins anywhere you
  have internet access.

means you can access your coins which are stored on their servers thus this is a
custodial app.

Our verdict: **not verifiable**.

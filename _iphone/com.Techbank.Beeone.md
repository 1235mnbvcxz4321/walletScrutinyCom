---
wsId: TECHBANK
title: "TECHBANK"
altTitle: 
authors:
- leo
appId: com.Techbank.Beeone
appCountry: 
idd: 1473120022
released: 2019-07-19
updated: 2021-07-28
version: "1.0.49"
stars: 4.11111
reviews: 9
size: 67168256
website: https://techbank.finance/
repository: 
issue: 
icon: com.Techbank.Beeone.jpg
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

They claim a million "members" and list big banks as their "acquirers" but there
is little to back those claims. 10k downloads on Play Store and eight ratings on
App Store don't look like a million users. The reviews on both platforms also
are abysmal.

Neither on the description nor their website do we find claims about this app being
a non-custodial wallet and as the name Tech**Bank** sounds rather custodial, we
file it as such and conclude this app is **not verifiable**.

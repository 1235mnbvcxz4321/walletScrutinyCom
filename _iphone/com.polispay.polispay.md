---
wsId: PolisPay
title: "PolisPay - Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.polispay.polispay
appCountry: 
idd: 1351572060
released: 2019-02-20
updated: 2021-05-03
version: "8.9.2"
stars: 3.83333
reviews: 6
size: 34938880
website: 
repository: 
issue: 
icon: com.polispay.polispay.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


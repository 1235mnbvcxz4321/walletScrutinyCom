---
wsId: ChangeNOW
title: "ChangeNOW Crypto Exchange pro"
altTitle: 
authors:
- leo
appId: io.changenow
appCountry: 
idd: 1518003605
released: 2020-06-29
updated: 2021-07-29
version: "1.6.1"
stars: 4.30818
reviews: 318
size: 37434368
website: http://changenow.io
repository: 
issue: 
icon: io.changenow.jpg
bugbounty: 
verdict: nosource
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: ChangeNOW_io
providerLinkedIn: 
providerFacebook: ChangeNOW.io
providerReddit: ChangeNOW_io

redirect_from:

---

> We focus on simplicity and safety — the service is registration-free and non-custodial.

> With ChangeNOW, you remain in full control over your digital assets.

That's a claim. Let's see if it is verifiable ...

There is no claim of public source anywhere and
[neither does GitHub know](https://github.com/search?q=%22io.changenow.changenow%22)
this app, so it's at best closed source and thus **not verifiable**.

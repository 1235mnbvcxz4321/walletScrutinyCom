---
wsId: Quppy
title: "Quppy: Bitcoin Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.quppy.wallet
appCountry: 
idd: 1417802076
released: 2018-08-09
updated: 2021-08-19
version: "1.0.57"
stars: 2.81818
reviews: 11
size: 46212096
website: https://quppy.com
repository: 
issue: 
icon: com.quppy.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: QuppyPay
providerLinkedIn: quppy
providerFacebook: quppyPay
providerReddit: 

redirect_from:

---

This provider loses no word on security or where the keys are stored. We assume
it is a custodial offering and therefore **not verifiable**.

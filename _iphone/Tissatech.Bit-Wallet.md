---
wsId: BitWallet
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
appId: Tissatech.Bit-Wallet
appCountry: 
idd: 1331439005
released: 2019-02-09
updated: 2021-07-31
version: "1.6.3"
stars: 4.84353
reviews: 1112
size: 15966208
website: 
repository: 
issue: 
icon: Tissatech.Bit-Wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: bitwalletinc
providerLinkedIn: 
providerFacebook: BitWalletInc
providerReddit: 

redirect_from:

---

This appears to be primarily an exchange and as there are no claims of you being
in sole control of your funds, we have to assume it is a custodial service and
therefore **not verifiable**.

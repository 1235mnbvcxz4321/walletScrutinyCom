---
wsId: Celsius
title: "Celsius - Safe Crypto Platform"
altTitle: 
authors:
- leo
appId: network.celsius.wallet
appCountry: 
idd: 1387885523
released: 2018-06-20
updated: 2021-08-12
version: "v4.10.0"
stars: 3.82648
reviews: 1337
size: 44462080
website: https://celsius.network/
repository: 
issue: 
icon: network.celsius.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: celsiusnetwork
providerLinkedIn: celsiusnetwork
providerFacebook: CelsiusNetwork
providerReddit: 

redirect_from:

---

> Use our fully functioning & secure crypto wallet & crypto lending platform to
  transfer and withdraw your Ethereum, Bitcoin, and over 30 other
  cryptocurrencies, free.

sounds like also a Bitcoin wallet.

The focus on "lending platform" doesn't make us hope for non-custodial parts to
it though ...

And sure enough, nowhere on the website can we find about this app being
non-custodial. As a custodial app, it is **not verifiable**.

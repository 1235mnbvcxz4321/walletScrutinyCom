---
wsId: bitcoinblack
title: "Bitcoin Black Wallet"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.bitcoinblack.wallet
appCountry: 
idd: 1523044877
released: 2020-07-20
updated: 2020-09-08
version: "1.0.5"
stars: 4.23529
reviews: 34
size: 80958464
website: https://bitcoin.black
repository: 
issue: 
icon: com.bitcoinblack.wallet.jpg
bugbounty: 
verdict: defunct
date: 2021-08-08
signer: 
reviewArchive:
- date: 2021-04-29
  version: "1.0.5"
  appHash: 
  gitRevision: 8846f7c2efdc1cf24d876fec2622625a77fe31a5
  verdict: nobtc

providerTwitter: BCB_Official1
providerLinkedIn: 
providerFacebook: bitcoinblackofficial
providerReddit: AllAboardBitcoinBlack

redirect_from:

---

**Update 2021-08-13**: This app is not anymore.


This is a wallet for an alt coin "Bitcoin Black"
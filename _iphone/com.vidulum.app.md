---
wsId: Vidulum
title: "Vidulum"
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2021-05-12
version: "1.2.1"
stars: 4.33333
reviews: 6
size: 62133248
website: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


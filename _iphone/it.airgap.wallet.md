---
wsId: AirGapWallet
title: "AirGap Wallet"
altTitle: 
authors:

appId: it.airgap.wallet
appCountry: 
idd: 1420996542
released: 2018-08-24
updated: 2021-08-02
version: "3.8.0"
stars: 3.88889
reviews: 9
size: 102132736
website: 
repository: 
issue: 
icon: it.airgap.wallet.jpg
bugbounty: 
verdict: nowallet
date: 2021-03-07
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---


---
wsId: bittrex
title: "Bittrex"
altTitle: 
authors:
- leo
appId: com.bittrex.trade
appCountry: 
idd: 1465314783
released: 2019-12-19
updated: 2021-08-17
version: "1.15.0"
stars: 2.63991
reviews: 461
size: 81533952
website: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.jpg
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: BittrexGlobal
providerLinkedIn: 
providerFacebook: BittrexGlobal
providerReddit: 

redirect_from:

---

This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.

---
wsId: OWNR
title: "OWNR crypto wallet for PC"
altTitle: 
authors:
- leo
appId: com.ownrwallet.desktop
appCountry: 
idd: 1520395378
released: 2020-08-13
updated: 2021-08-13
version: "1.6.4"
stars: 
reviews: 
size: 124692798
website: https://ownrwallet.com
repository: 
issue: 
icon: com.ownrwallet.desktop.png
bugbounty: 
verdict: nosource
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: ownrwallet
providerLinkedIn: 
providerFacebook: ownrwallet
providerReddit: ownrwallet

redirect_from:

---


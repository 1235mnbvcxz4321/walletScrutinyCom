---
wsId: bluewallet
title: "BlueWallet - Bitcoin wallet"
altTitle: 
authors:
- leo
appId: io.bluewallet.bluewallet
appCountry: 
idd: 1376878040
released: 2018-05-27
updated: 2021-07-28
version: "6.2.2"
stars: 4.20376
reviews: 265
size: 46880768
website: https://bluewallet.io
repository: https://github.com/bluewallet/bluewallet
issue: https://github.com/BlueWallet/BlueWallet/issues/2364
icon: io.bluewallet.bluewallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-03
signer: 
reviewArchive:


providerTwitter: bluewalletio
providerLinkedIn: 
providerFacebook: 
providerReddit: bluewallet

redirect_from:

---

**Update 2021-03-22:** Already in January the provider added *"- This wallet is
hosted by BlueWallet."* without any further explanation that this means that
BlueWallet which appears to not be a legal entity is in control of the Bitcoins
on those LN accounts. They also closed
[the issue about the disclaimer](https://github.com/BlueWallet/BlueWallet/issues/2364).

**Update 2020-07-14**: This app was earlier classified as non-custodial but as their main
selling point is their *"Unfairly cheap and blazing fast transactions on
Bitcoin"* Lighning Wallet and that wallet is custodial without warning the user
about this fact, we have to change our verdict although in summary the verdict
remains **not verifiable**.

[This issue](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/117)
was brought up by [Lynn](https://gitlab.com/losnappas).

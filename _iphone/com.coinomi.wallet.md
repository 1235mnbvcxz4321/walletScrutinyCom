---
wsId: coinomi
title: "Coinomi Wallet"
altTitle: 
authors:
- leo
appId: com.coinomi.wallet
appCountry: 
idd: 1333588809
released: 2018-03-22
updated: 2021-08-15
version: "1.11.0"
stars: 4.54893
reviews: 1257
size: 128095232
website: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: CoinomiWallet
providerLinkedIn: coinomi
providerFacebook: coinomi
providerReddit: COINOMI

redirect_from:

---

This wallet claims to be non-custodial but
[went closed-source](https://github.com/bitcoin-dot-org/bitcoin.org/issues/1622)
in early 2016
[according to them](https://twitter.com/CoinomiWallet/status/945048682927394817)

> **coinomi**<br>
  @CoinomiWallet<br>
  No, we moved to closed source to protect the users from getting ripped off by
  scammers. Our website is open source and there are at least 10 clones at the
  time of the writing stealing users' funds (by stealing their private keys).<br>
  6:48 PM · Dec 24, 2017

Our verdict: This app is **not verifiable**.

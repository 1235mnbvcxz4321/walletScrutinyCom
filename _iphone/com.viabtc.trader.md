---
wsId: CoinEx
title: "CoinEx-Cryptocurrency Exchange"
altTitle: 
authors:
- leo
appId: com.viabtc.trader
appCountry: 
idd: 1264462812
released: 2017-08-23
updated: 2021-08-18
version: "3.1.1"
stars: 4.57721
reviews: 1185
size: 131442688
website: https://www.coinex.com
repository: 
issue: 
icon: com.viabtc.trader.jpg
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: coinexcom
providerLinkedIn: 
providerFacebook: TheCoinEx
providerReddit: Coinex

redirect_from:

---

Unfortunately there is no easily accessible information on the App Store or
their website about the app's security. For now we assume it is
custodial and thus **not verifiable**.

---
wsId: prizmbit
title: "Prizm paramining wallet"
altTitle: 
authors:
- kiwilamb
appId: prizmbit.com
appCountry: 
idd: 1459094607
released: 2019-06-14
updated: 2021-06-24
version: "1.6.6"
stars: 3
reviews: 2
size: 39063552
website: https://prizmbit.com/
repository: 
issue: 
icon: prizmbit.com.jpg
bugbounty: 
verdict: custodial
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: prizmbit
providerLinkedIn: 
providerFacebook: prizmbit
providerReddit: 

redirect_from:

---

There is no statement regarding how private keys are managed in the app store description or on the [providers website](https://prizmbit.com/) or FAQ.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
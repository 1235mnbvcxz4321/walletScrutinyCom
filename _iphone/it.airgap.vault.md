---
wsId: AirGapVault
title: "AirGap Vault - Secure Secrets"
altTitle: 
authors:
- leo
appId: it.airgap.vault
appCountry: 
idd: 1417126841
released: 2018-08-24
updated: 2021-08-02
version: "3.8.0"
stars: 4.33333
reviews: 6
size: 87980032
website: 
repository: 
issue: 
icon: it.airgap.vault.jpg
bugbounty: 
verdict: wip
date: 2020-12-19
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---


---
wsId: indodax
title: "Indodax Trading Platform"
altTitle: 
authors:
- leo
appId: id.co.bitcoin.Bitcoin-Trading-Platform
appCountry: 
idd: 1349104693
released: 2018-03-29
updated: 2021-08-13
version: "3.1.1"
stars: 
reviews: 
size: 70433792
website: https://indodax.com
repository: 
issue: 
icon: id.co.bitcoin.Bitcoin-Trading-Platform.jpg
bugbounty: 
verdict: custodial
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: indodax
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the interface to

> Indodax is Indonesia’s largest crypto asset marketplace

and on their website they claim:

> **Security**<br>
  Every transaction is protected with Multi-factor Authentication, combining
  email verification and Google Authenticator SMS to guarantee that your
  transaction is truly signed and validated only by you.

With no further explanation and as this is an exchange, we assume the app is a
custodial offering and thus **not verifiable**.
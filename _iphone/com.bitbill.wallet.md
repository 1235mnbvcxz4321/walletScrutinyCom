---
wsId: ownbit
title: "Ownbit: Cold & MultiSig Wallet"
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2021-08-01
version: "4.31.1"
stars: 4.42593
reviews: 54
size: 117084160
website: 
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


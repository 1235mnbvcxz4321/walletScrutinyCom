---
wsId: strike
title: "Strike: Send and Spend Bitcoin"
altTitle: 
authors:

appId: com.jackmallers.Strike
appCountry: 
idd: 1488724463
released: 2020-08-10
updated: 2021-08-18
version: "27.0"
stars: 4.53184
reviews: 581
size: 24802304
website: https://beta.strike.me/
repository: 
issue: 
icon: com.jackmallers.Strike.jpg
bugbounty: 
verdict: nobtc
date: 2021-01-18
signer: 
reviewArchive:


providerTwitter: ln_strike
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app does not hold Bitcoins but it allows to interact with Bitcoin wallets
in that you can send to and receive from Bitcoin wallets but your balance is
always in fiat.

The provider claims this wallet is non-custodial but ...

> **Is Strike custodial?**<br>
  No. Users never custody Bitcoin through Strike. Strike is the custodian of the
  fiat deposited onto the app. All fiat is FDIC-insured and users are free to
  withdraw these funds to their linked account at any time.

that sounds like a "custodial wallet", doesn't it? It's **not a Bitcoin wallet**
though. We could also go with "custodial" as all the
funds are being held by the provider and it sure enough is promoted as a Bitcoin
wallet. Please raise an issue
[on our GitLab](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/new)
if you feel strong about this verdict.

---
wsId: mw.org.freewallet
title: "Multi Crypto Wallet－Freewallet"
altTitle: 
authors:
- leo
appId: mw.org.freewallet.app
appCountry: 
idd: 1274003898
released: 2017-09-01
updated: 2021-05-27
version: "1.15.5"
stars: 4.09517
reviews: 1429
size: 45112320
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

redirect_from:

---

According to the description

> In addition, the majority of cryptocurrency assets on the platform are stored
  in an offline vault. Your coins will be kept in cold storage with state of the
  art security protecting them.

This is a custodial app.

Our verdict: **not verifiable**.

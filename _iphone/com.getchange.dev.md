---
wsId: getchange
title: "Change: Cryptocurrency to Cash"
altTitle: 

appId: com.getchange.dev
idd: 1442085358
released: 2018-11-15
updated: 2021-01-16
version: "10.9.132"
stars: 4.41176
reviews: 17
size: 128389120
website: 
repository: 
issue: 
icon: com.getchange.dev.jpg
bugbounty: 
verdict: defunct
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---
**Update:** We did not get to review this app before it was removed from the App
Store.

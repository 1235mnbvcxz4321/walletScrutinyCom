---
wsId: 
title: "MonaWallet"
altTitle: 
authors:

appId: jp.pronama.monawallet
appCountry: us
idd: 1343235820
released: 2018-02-22
updated: 2019-04-10
version: "2.0.0"
stars: 
reviews: 
size: 42539008
website: https://monawallet.net
repository: 
issue: 
icon: jp.pronama.monawallet.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-07-29
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


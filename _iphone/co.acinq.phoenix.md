---
wsId: phoenix
title: "Phoenix Wallet"
altTitle: 
authors:

appId: co.acinq.phoenix
appCountry: us
idd: 1544097028
released: 2021-07-13
updated: 2021-08-05
version: "1.0.5"
stars: 4.2
reviews: 5
size: 19543040
website: https://phoenix.acinq.co
repository: https://github.com/ACINQ/phoenix-kmm
issue: 
icon: co.acinq.phoenix.jpg
bugbounty: 
verdict: wip
date: 2021-08-16
signer: 
reviewArchive:


providerTwitter: PhoenixWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


---
wsId: ccwallet
title: "CCWallet: Your Bitcoin Wallet"
altTitle: 
authors:
- leo
- emanuel
appId: ccx.wallet
appCountry: 
idd: 1463774169
released: 2019-06-21
updated: 2020-07-30
version: "1.1"
stars: 4.41176
reviews: 17
size: 28720128
website: https://ccwalletapp.com/
repository: https://github.com/coincasso/ccwallet
issue: https://github.com/coincasso/ccwallet/issues/1
icon: ccx.wallet.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-07
  version: "1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nonverifiable

providerTwitter: CoinCasso
providerLinkedIn: coincasso
providerFacebook: ccwallet.mobileapp
providerReddit: 

redirect_from:

---

Same as the {% include walletLink.html wallet='android/com.ccwalletapp' %} on Play Store, this is an open
source, Bitcoin, self-custodial wallet but [as with all iPhone apps](/apple),
it is **not verifiable**.

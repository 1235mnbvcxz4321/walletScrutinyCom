---
wsId: krcokeypair
title: "KeyWallet Touch - Bitcoin"
altTitle: 
authors:

appId: kr.co.keypair.keywalletTouchiOS
appCountry: 
idd: 1473941321
released: 2019-07-28
updated: 2020-12-14
version: "1.1.27"
stars: 4
reviews: 1
size: 20524032
website: http://keywalletpro.io
repository: 
issue: 
icon: kr.co.keypair.keywalletTouchiOS.jpg
bugbounty: 
verdict: wip
date: 2021-03-07
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.

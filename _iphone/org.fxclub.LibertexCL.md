---
wsId: libertex
title: "Libertex - Forex Trading App"
altTitle: 
authors:

appId: org.fxclub.LibertexCL
appCountry: cl
idd: 1078997450
released: 2016-02-10
updated: 2019-05-25
version: "2.21.0"
stars: 4.4985
reviews: 333
size: 30611456
website: https://libertex.org/
repository: 
issue: 
icon: org.fxclub.LibertexCL.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-09
  version: "2.21.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


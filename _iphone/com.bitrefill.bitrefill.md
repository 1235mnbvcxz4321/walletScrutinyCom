---
wsId: bitrefill
title: "Bitrefill"
altTitle: 
authors:
- leo
appId: com.bitrefill.bitrefill
appCountry: 
idd: 1378102623
released: 2018-06-05
updated: 2019-09-23
version: "1.13"
stars: 4.58333
reviews: 36
size: 5557248
website: https://www.bitrefill.com
repository: 
issue: 
icon: com.bitrefill.bitrefill.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-25
  version: "1.13"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: bitrefill
providerLinkedIn: 
providerFacebook: bitrefill
providerReddit: Bitrefill

redirect_from:

---

While the primary purpose of this app is to buy stuff with Bitcoin and it appears
to be possible to use the app without putting money into it, the app also can
hold a balance, so it appears to be a wallet. At least we take that from the
screenshots.

As the description has no claims to the contrary and we can't find anything about
the app on their website except for a link to the Play Store, we have to assume
it is a custodial service.

Our verdict: The app is **not verifiable**.

---
wsId: evercoin
title: "Evercoin: Bitcoin, Ripple, ETH"
altTitle: 
authors:
- leo
appId: com.evercoinInc.evercoin
appCountry: 
idd: 1277924158
released: 2017-09-16
updated: 2020-11-30
version: "1.9.5"
stars: 4.65244
reviews: 2765
size: 63333376
website: https://evercoin.com
repository: 
issue: 
icon: com.evercoinInc.evercoin.jpg
bugbounty: 
verdict: nosource
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: everc0in
providerLinkedIn: 
providerFacebook: evercoin
providerReddit: 

redirect_from:

---

This app's description says:

> Evercoin is an integrated non-custodial wallet for managing and exchanging
  cryptocurrencies.

So ... is there source code to reproduce the build?

Unfortunately there is no mention of source code anywhere. Absent source code
this app is **not verifiable**.

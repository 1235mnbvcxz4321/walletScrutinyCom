---
wsId: coinbaseBSB
title: "Coinbase – Buy & sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.vilcsak.bitcoin2
appCountry: 
idd: 886427730
released: 2014-06-22
updated: 2021-08-17
version: "9.37.2"
stars: 4.69146
reviews: 1399547
size: 102391808
website: http://www.coinbase.com
repository: 
issue: 
icon: com.vilcsak.bitcoin2.jpg
bugbounty: 
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: coinbase
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:

---

This app's provider claims:

> Over 98% of cryptocurrency is stored securely offline and the rest is
  protected by industry-leading online security.

which clearly means it is a custodial offering.

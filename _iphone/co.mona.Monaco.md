---
wsId: mona
title: "Crypto.com - Buy Bitcoin Now"
altTitle: 
authors:
- leo
appId: co.mona.Monaco
appCountry: 
idd: 1262148500
released: 2017-08-31
updated: 2021-08-09
version: "3.108"
stars: 3.92699
reviews: 24613
size: 278082560
website: https://crypto.com/
repository: 
issue: 
icon: co.mona.Monaco.jpg
bugbounty: 
verdict: custodial
date: 2021-01-11
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

As their app {% include walletLink.html wallet='android/co.mona.android' %} on Play Store,
this app is custodial
and thus **not verifiable**.

---
wsId: coinspace
title: "Coin Bitcoin & Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.coinspace.wallet
appCountry: 
idd: 980719434
released: 2015-12-14
updated: 2021-08-20
version: "5.0.0"
stars: 4.42856
reviews: 126
size: 31947776
website: https://coin.space/
repository: https://github.com/CoinSpace/CoinSpace
issue: 
icon: com.coinspace.wallet.jpg
bugbounty: https://www.openbugbounty.org//bugbounty/CoinAppWallet/
verdict: nonverifiable
date: 2020-12-20
signer: 
reviewArchive:


providerTwitter: coinappwallet
providerLinkedIn: coin-space
providerFacebook: coinappwallet
providerReddit: 

redirect_from:

---

On the website the provider claims:

> keys are stored locally, on your device

and there is a public source repository on GitHub but as iPhone apps are
all currently not reproducible, the app remains **not verifiable**.

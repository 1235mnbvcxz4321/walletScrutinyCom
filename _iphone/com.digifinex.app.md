---
wsId: digifinex
title: "DigiFinex - Bitcoin Exchange"
altTitle: 
authors:
- leo
appId: com.digifinex.app
appCountry: 
idd: 1397735851
released: 2018-08-25
updated: 2021-08-19
version: "2021.08.18"
stars: 3
reviews: 14
size: 400433152
website: https://www.digifinex.com
repository: 
issue: 
icon: com.digifinex.app.jpg
bugbounty: 
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: DigiFinex
providerLinkedIn: digifinex-global
providerFacebook: digifinex.global
providerReddit: DigiFinex

redirect_from:

---

> DigiFinex is a world’s leading crypto finance exchange

doesn't sound like "wallet" is their primary business and as we can't find any
claims to the contrary, we have to assume this is a custodial offering and thus
**not verifiable**.

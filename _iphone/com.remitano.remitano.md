---
wsId: Remitano
title: "Remitano"
altTitle: 
authors:
- leo
appId: com.remitano.remitano
appCountry: 
idd: 1116327021
released: 2016-05-28
updated: 2021-08-10
version: "5.57.0"
stars: 4.76792
reviews: 8006
size: 52062208
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.jpg
bugbounty: 
verdict: custodial
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:

---

This app is an interface to an exchange which holds your coins. On the App Store
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.

---
wsId: Totalcoin
title: "Bitcoin Wallet App - Totalcoin"
altTitle: 
authors:
- leo
appId: io.totalcoin.wallet
appCountry: 
idd: 1392398906
released: 2018-07-05
updated: 2021-05-05
version: "3.0.0"
stars: 4.47825
reviews: 92
size: 44261376
website: http://totalcoin.io
repository: 
issue: 
icon: io.totalcoin.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: totalcoin.io
providerReddit: 

redirect_from:

---

On the wallet's website there is no claim about custodianship which makes us
assume it is a custodial product.

As such it is **not verifiable**.

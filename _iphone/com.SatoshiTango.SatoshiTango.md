---
wsId: SatoshiTango
title: "SatoshiTango"
altTitle: 
authors:
- leo
appId: com.SatoshiTango.SatoshiTango
appCountry: 
idd: 1002555958
released: 2015-07-08
updated: 2021-08-20
version: "4.0.2"
stars: 4.21875
reviews: 32
size: 108969984
website: http://www.satoshitango.com
repository: 
issue: 
icon: com.SatoshiTango.SatoshiTango.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: satoshitango
providerLinkedIn: 
providerFacebook: satoshitangoargentina
providerReddit: 

redirect_from:

---

> Buy and sell BTC, ETH, LTC, XRP and BCH and monitor your balance and
  transactions.<br>
  Pay in local currency and hold a balance in fiat currency. Store your cryptos
  and much more!

sounds like a wallet that also supports Bitcoin but there is not much
information on who gets to control the keys.

We have to assume this is a custodial offering and thus **not verifiable**.

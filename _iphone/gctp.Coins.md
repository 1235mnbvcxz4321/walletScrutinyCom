---
wsId: coinsph
title: "Coins– Load, Bills, Bitcoin"
altTitle: 
authors:
- leo
appId: gctp.Coins
appCountry: 
idd: 972324049
released: 2015-04-04
updated: 2021-08-13
version: "2.17.5"
stars: 4.72152
reviews: 2977
size: 121068544
website: https://coins.ph/
repository: 
issue: 
icon: gctp.Coins.jpg
bugbounty: 
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: coinsph
providerLinkedIn: coins-ph
providerFacebook: coinsph
providerReddit: 

redirect_from:

---

The app appears to have tons of features but nowhere can we find a word about
where the bitcoins are stored.
[Their FAQ](https://support.coins.ph/hc/en-us/categories/202504637-Safety-Security)
is talking a lot about "account is deactivated" or "temporarily disabled" which
are concepts not known in self-custodial wallets.

We assume this app is custodial and therefore **not verifiable**.
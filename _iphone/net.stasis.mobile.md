---
wsId: STASISStablecoinWallet
title: "STASIS Stablecoin Wallet"
altTitle: 
authors:
- leo
appId: net.stasis.mobile
appCountry: 
idd: 1371949230
released: 2018-07-06
updated: 2021-07-27
version: "7.12"
stars: 3.66667
reviews: 3
size: 22810624
website: https://stasis.net
repository: 
issue: 
icon: net.stasis.mobile.jpg
bugbounty: 
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: stasisnet
providerLinkedIn: stasisnet
providerFacebook: stasisnet
providerReddit: 

redirect_from:

---

**Update 2021-05-09**: The review is based on the Play Store app. The App Store
app is currently not available and if that remains, the app will be considered
defunct.

On their website there is no mention of being non-custodial and
certainly there is no source code available. Until we hear opposing claims
we consider it a custodial app and therefore **not verifiable**.

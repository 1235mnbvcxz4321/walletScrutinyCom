---
wsId: coindirect
title: "Coindirect"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.node.coindirect
appCountry: 
idd: 1438224938
released: 2018-10-25
updated: 2021-07-21
version: "1.2.7"
stars: 2.55555
reviews: 9
size: 34306048
website: https://www.coindirect.com/
repository: 
issue: 
icon: com.node.coindirect.jpg
bugbounty: 
verdict: custodial
date: 2021-04-27
signer: 
reviewArchive:


providerTwitter: coindirectcom
providerLinkedIn: coindirect
providerFacebook: coindirectcom
providerReddit: 

redirect_from:

---

The website states:

> The majority of our users’ Bitcoins are kept securely in cold storage. This is
  equivalent to a digital bank vault.

this leads us to conclude the wallet funds are in control of the provider and
hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

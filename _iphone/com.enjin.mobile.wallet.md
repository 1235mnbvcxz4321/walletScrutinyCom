---
wsId: enjin
title: "Enjin: NFT Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.enjin.mobile.wallet
appCountry: 
idd: 1349078375
released: 2018-03-12
updated: 2021-06-21
version: "1.15.0"
stars: 4.47795
reviews: 567
size: 43804672
website: https://enjin.io/products/wallet
repository: 
issue: 
icon: com.enjin.mobile.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: enjin
providerLinkedIn: enjin
providerFacebook: enjinsocial
providerReddit: EnjinCoin

redirect_from:

---

Enjin: Blockchain & Crypto Wallet
description starts promising:

> "Your private keys are your own"

They advertise advanced securing techniques among which are:

> An extensive independent security audit and penetration test found no security
  issues.

(You can read the report
[here](https://cdn.enjin.io/files/pdfs/enjin-wallet-security-audit.pdf))

But source code isn't available on [their website](https://github.com/enjin).
So the user is left with only one choice: trust.

Our verdict: **not verifiable**.


Other observations
------------------

> in-app browsing:
> "ENJOY SEAMLESS BROWSING
> Interact with any DApp with the single click of a button—without leaving the
> safety of your crypto wallet."

looks very advanced, the list of features is tremendous. also an old player:

> ABOUT ENJIN<br>
  Founded in 2009 and based in Singapore, Enjin offers an ecosystem of
  integrated, user-first blockchain products that enable anyone to easily
  manage, explore, distribute, and integrate blockchain-based assets.

on their main page, they advertise advanced securing techniques amongst which are:

> * Custom ARM instructions ensure that sensitive data is instantly deleted from
    your phone's memory.
> * Enjin Keyboard. Built from scratch to protect you from any form of data
    sniffing or keyloggers.

---
wsId: FlareWallet
title: "Flare Wallet"
altTitle: 

appId: org.flarewallet.flare
idd: 1496651406
released: 2020-02-11
updated: 2021-01-02
version: "1.3.9"
stars: 4.16129
reviews: 31
size: 24001536
website: https://flarewallet.io
repository: 
issue: 
icon: org.flarewallet.flare.jpg
bugbounty: 
verdict: defunct
date: 2021-02-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update:** We did not get to review this app before it was removed from the App
Store.

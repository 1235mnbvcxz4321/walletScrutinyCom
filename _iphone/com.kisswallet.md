---
wsId: 
title: "Moonshine Wallet"
altTitle: 
authors:

appId: com.kisswallet
appCountry: 
idd: 1449385577
released: 2020-02-16
updated: 2020-05-30
version: "0.3.6"
stars: 5
reviews: 3
size: 73128960
website: https://moonshinewallet.com/
repository: 
issue: 
icon: com.kisswallet.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-06-25
  version: "0.3.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


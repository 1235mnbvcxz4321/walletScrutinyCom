---
wsId: WalletofSatoshi
title: "Wallet of Satoshi"
altTitle: 
authors:
- leo
appId: com.livingroomofsatoshi.wallet
appCountry: 
idd: 1438599608
released: 2019-05-20
updated: 2021-08-02
version: "1.11.4"
stars: 2.98182
reviews: 55
size: 33009664
website: https://www.walletofsatoshi.com
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: walletofsatoshi
providerLinkedIn: 
providerFacebook: walletofsatoshi
providerReddit: 

redirect_from:

---

This is a custodial wallet according to their website's FAQ:

> It is a zero-configuration custodial wallet with a focus on simplicity and the
  best possible user experience.

and therefore **not verifiable**.

---
wsId: nash
title: "Nash: Buy & sell crypto"
altTitle: 
authors:
- kiwilamb
appId: io.nash.app
appCountry: 
idd: 1475759236
released: 2019-09-06
updated: 2021-08-17
version: "2.88"
stars: 4.64151
reviews: 106
size: 86514688
website: https://nash.io
repository: 
issue: 
icon: io.nash.app.jpg
bugbounty: 
verdict: nosource
date: 2021-04-30
signer: 
reviewArchive:


providerTwitter: nashsocial
providerLinkedIn: nashsocial
providerFacebook: 
providerReddit: 

redirect_from:

---

This statement in the description from the [App store](https://apps.apple.com/us/app/nash-app/id1475759236?mt=8) below is a claim only the user has access to the private keys.

> Nash doesn’t take control of your funds – unlike Coinbase, Kraken or Binance. We’re the only fully-featured exchange where you can trade Bitcoin without giving up your private keys.

With keys in control of the user, we need to find the source code in order to check reproducibility.
However we are unable to locate a public source repository.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

---
wsId: metamask
title: "MetaMask - Blockchain Wallet"
altTitle: 
authors:
- leo
appId: io.metamask.MetaMask
appCountry: 
idd: 1438144202
released: 2020-09-03
updated: 2021-08-18
version: "3.1.0"
stars: 3.17996
reviews: 928
size: 42217472
website: https://metamask.io/
repository: 
issue: 
icon: io.metamask.MetaMask.jpg
bugbounty: 
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is an ETH-only app and thus not a Bitcoin wallet.

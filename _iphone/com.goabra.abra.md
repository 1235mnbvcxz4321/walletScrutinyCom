---
wsId: goabra
title: "Abra: Bitcoin & Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.goabra.abra
appCountry: 
idd: 966301394
released: 2015-03-12
updated: 2021-08-03
version: "104.0.0"
stars: 4.56565
reviews: 15911
size: 113041408
website: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive:


providerTwitter: AbraGlobal
providerLinkedIn: abra
providerFacebook: GoAbraGlobal
providerReddit: 

redirect_from:

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/com.plutus.wallet' %}.

Just like the Android version, this wallet is **not verifiable**.

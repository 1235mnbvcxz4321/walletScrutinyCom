---
wsId: bitplaza
title: "Bitplaza - Spend Bitcoin"
altTitle: 
authors:

appId: com.bitplaza
appCountry: 
idd: 1438228771
released: 2018-10-09
updated: 2018-10-09
version: "1.0"
stars: 4.3
reviews: 10
size: 32704512
website: https://www.bitplazashopping.com/
repository: 
issue: 
icon: com.bitplaza.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-11
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nowallet

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app is a market place with no integrated wallet.

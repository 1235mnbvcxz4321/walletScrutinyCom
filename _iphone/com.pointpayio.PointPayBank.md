---
wsId: pointpay
title: "PointPay Bank - Crypto Wallet"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.pointpayio.PointPayBank
appCountry: 
idd: 1512836089
released: 2020-07-30
updated: 2021-08-12
version: "5.8.1"
stars: 4.83784
reviews: 518
size: 63953920
website: https://wallet.pointpay.io
repository: 
issue: 
icon: com.pointpayio.PointPayBank.jpg
bugbounty: 
verdict: custodial
date: 2021-04-27
signer: 
reviewArchive:


providerTwitter: PointPay1
providerLinkedIn: pointpay
providerFacebook: PointPayLtd
providerReddit: PointPay

redirect_from:

---

The PointPay Bank website has very little information about how they manage
private keys of the users.
The only basic statement is...

> We use strong military-grade encryption to store private keys

we will have to conclude the wallet funds are in control of the provider and
hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

---
wsId: coincola
title: "CoinCola – Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.coincola.beta
appCountry: 
idd: 1234231551
released: 2017-06-06
updated: 2021-06-16
version: "4.8.1"
stars: 4
reviews: 188
size: 150502400
website: https://www.coincola.com
repository: 
issue: 
icon: com.coincola.beta.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: CoinCola_Global
providerLinkedIn: coincola
providerFacebook: CoinCola
providerReddit: coincolaofficial

redirect_from:

---

> SAFE AND SECURE<br>
  Our team uses bank-level encryption, cold storage and SSL for the highest
  level of security.

Cold storage has only a meaning in the context of a custodial app. As such it
is **not verifiable**.

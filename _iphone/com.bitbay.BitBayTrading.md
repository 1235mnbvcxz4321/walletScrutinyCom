---
wsId: bitpaytrading
title: "BitBay - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.bitbay.BitBayTrading
appCountry: 
idd: 1409644952
released: 2018-11-20
updated: 2021-08-09
version: "1.3.26"
stars: 3.63636
reviews: 22
size: 90708992
website: https://bitbay.net
repository: 
issue: 
icon: com.bitbay.BitBayTrading.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: BitBay
providerLinkedIn: bitbay
providerFacebook: BitBay
providerReddit: BitBayExchange

redirect_from:

---

This app's description loses no word on who holds the keys to your coins. Their
website is mainly about the exchange and not about the mobile appp but there is
[a site about that](https://bitbay.net/en/mobile), too. There they only talk
about exchange features, too and lose no word about who holds the keys which
probably means this app is a custodial offering and therefore **not verifiable**.

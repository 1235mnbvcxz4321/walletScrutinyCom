---
wsId: voyager
title: "Voyager - Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.investvoyager.voyager-ios
appCountry: 
idd: 1396178579
released: 2019-02-13
updated: 2021-07-29
version: "2.9.23"
stars: 4.73939
reviews: 72433
size: 62888960
website: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.voyager-ios.jpg
bugbounty: 
verdict: custodial
date: 2021-01-02
signer: 
reviewArchive:


providerTwitter: investvoyager
providerLinkedIn: investvoyager
providerFacebook: InvestVoyager
providerReddit: Invest_Voyager

redirect_from:

---

On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.

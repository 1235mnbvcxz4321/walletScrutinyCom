---
wsId: nexo
title: "Nexo - Crypto Account"
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
appCountry: 
idd: 1455341917
released: 2019-06-30
updated: 2021-08-12
version: "2.0.5"
stars: 3.62012
reviews: 487
size: 49939456
website: https://nexo.io
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-22
signer: 
reviewArchive:


providerTwitter: NexoFinance
providerLinkedIn: 
providerFacebook: nexofinance
providerReddit: Nexo

redirect_from:

---

In the description on the App Store we read:

> • Your Nexo account is secured through BitGo, the leader in crypto
  custodianship, and benefits from top-tier insurance.

which makes it a custodial app. The custodian is claimed to be "BitGo" so as a
user you have to trust BitGo to not lose the coins and Nexo to actually not hold
all or part of the coins. In any case this app is **not verifiable**.

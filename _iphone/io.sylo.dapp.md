---
wsId: Sylo
title: "Sylo"
altTitle: 
authors:
- leo
appId: io.sylo.dapp
appCountry: 
idd: 1452964749
released: 2019-09-10
updated: 2021-08-07
version: "3.1.21"
stars: 4.85246
reviews: 61
size: 192482304
website: https://www.sylo.io/wallet/
repository: 
issue: 
icon: io.sylo.dapp.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


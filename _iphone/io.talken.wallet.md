---
wsId: Talken
title: "Talken Multi-chain NFT Wallet"
altTitle: 
authors:
- kiwilamb
appId: io.talken.wallet
appCountry: 
idd: 1459475831
released: 2019-09-25
updated: 2021-08-10
version: "1.0.21"
stars: 5
reviews: 8
size: 67850240
website: https://talken.io/
repository: 
issue: 
icon: io.talken.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: Talken_
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

With this statement below from the providers [App store description](https://apps.apple.com/app/apple-store/id1459475831), it is clear that the user is not in control of the wallets private keys.

> Easy and secure wallet
> Easy wallet service without managing private keys and mnemonics.

Our Verdict: This "wallet" is custodial and therefor **not verifiable**


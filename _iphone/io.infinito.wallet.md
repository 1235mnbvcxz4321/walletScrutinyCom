---
wsId: InfinitoWallet
title: "Infinito Wallet - Crypto Safe"
altTitle: 
authors:
- leo
appId: io.infinito.wallet
appCountry: 
idd: 1315572736
released: 2018-01-17
updated: 2021-07-22
version: "2.35.6"
stars: 4.18032
reviews: 183
size: 98968576
website: https://www.infinitowallet.io
repository: 
issue: 
icon: io.infinito.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: Infinito_Ltd
providerLinkedIn: infinitoservices
providerFacebook: InfinitoWallet
providerReddit: 

redirect_from:

---

Right on the App Store description we find:

> It's free, easy to use and secure - you control your private keys &
  passphrases!

So it is not a custodial app. How about source code?

Unfortunately we can't find any source code.

Our verdict: This app is **not verifiable**.

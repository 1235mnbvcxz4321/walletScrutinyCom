---
wsId: ViaWallet
title: "ViaWallet - Multi-chain Wallet"
altTitle: 
authors:
- leo
appId: com.viabtc.ViaWallet
appCountry: 
idd: 1462031389
released: 2019-05-21
updated: 2021-08-07
version: "2.4.0"
stars: 4.04762
reviews: 21
size: 77440000
website: https://viawallet.com
repository: 
issue: 
icon: com.viabtc.ViaWallet.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


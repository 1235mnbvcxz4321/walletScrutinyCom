---
wsId: FreeWallet
title: "FreeWallet"
altTitle: 
authors:
- leo
appId: io.freewallet.mobile
appCountry: 
idd: 1151168579
released: 2016-11-05
updated: 2019-03-18
version: "1.0.9"
stars: 3.25
reviews: 8
size: 13651968
website: https://freewallet.io
repository: https://github.com/jdogresorg/freewallet-mobile
issue: 
icon: io.freewallet.mobile.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2020-12-21
  version: "1.0.9"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: freewallet
providerLinkedIn: 
providerFacebook: freewallet.io
providerReddit: 

redirect_from:

---

In the description we can read:

> Secure<br>
  Wallet Passphrase & private keys never leave device

and

> FreeWallet is an open source mobile wallet which supports Bitcoin.

so it's a non-custodial, open source Bitcoin wallet but can we verify the
claims?

On their website we find a link to their GitHub and from their to the mobile
wallet's repository. There we find no claims of reproducibility and not even
build isntructions, so this app is **not verifiable**.

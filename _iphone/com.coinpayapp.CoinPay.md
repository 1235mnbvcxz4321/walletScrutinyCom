---
wsId: coinpayapp
title: "Coin Pay Wallet: Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.coinpayapp.CoinPay
appCountry: 
idd: 1477731032
released: 2019-12-04
updated: 2021-07-24
version: "2021.07.23"
stars: 4.07692
reviews: 117
size: 21129216
website: https://www.coinpayapp.com
repository: 
issue: 
icon: com.coinpayapp.CoinPay.jpg
bugbounty: 
verdict: nosource
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

> We are a non-custodial wallet, so users can send Bitcoin and other
  cryptocurrencies globally with direct access to the blockchain.

... so this is (claiming to be) a non-custodial Bitcoin wallet. Can we verify
this?

The answer is "no". There is no source code linked on their website or the App
Store description. This app is closed source and thus **not verifiable**.

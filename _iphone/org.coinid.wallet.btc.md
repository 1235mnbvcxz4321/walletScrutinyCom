---
wsId: 
title: "Bitcoin Wallet for COINiD"
altTitle: 
authors:

appId: org.coinid.wallet.btc
appCountry: 
idd: 1370200585
released: 2018-10-10
updated: 2021-02-20
version: "1.8.0"
stars: 4.68182
reviews: 44
size: 18523136
website: https://coinid.org
repository: 
issue: 
icon: org.coinid.wallet.btc.jpg
bugbounty: 
verdict: wip
date: 2021-06-25
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


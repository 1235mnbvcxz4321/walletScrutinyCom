---
wsId: exscudo
title: "Channels. Кошелек и обменник"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.exscudo.channels
appCountry: ru
idd: 1367425342
released: 2018-05-16
updated: 2021-07-23
version: "2.18"
stars: 4.58065
reviews: 31
size: 179470336
website: https://channels.chat
repository: 
issue: 
icon: com.exscudo.channels.jpg
bugbounty: 
verdict: nosource
date: 2021-04-24
signer: 
reviewArchive:


providerTwitter: ex_scudo
providerLinkedIn: 
providerFacebook: exscudo
providerReddit: EXSCUDO

redirect_from:

---

The App Store description states...

> All funds are fully protected and no one except the owner can access them.

so the provider is sort of claiming the wallet is non-custodial.
However no source code can be found in a public repository for validation.

Our verdict: This ‘wallet’ claims to be non-custodial, however with no source
code this is **not verifiable**.

---
wsId: MathWallet
title: "Math Wallet-Blockchain Wallet"
altTitle: 
authors:
- leo
appId: com.medishares.ios.pro
appCountry: 
idd: 1383637331
released: 2019-04-30
updated: 2019-07-10
version: "3.1.0"
stars: 2.92683
reviews: 41
size: 135909376
website: https://www.mathwallet.org
repository: 
issue: 
icon: com.medishares.ios.pro.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2020-12-22
  version: "3.1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


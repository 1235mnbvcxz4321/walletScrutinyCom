---
wsId: 
title: "Airbitz - Bitcoin Wallet"
altTitle: 
authors:

appId: co.airbitz.airbitz
appCountry: 
idd: 843536046
released: 2014-03-28
updated: 2018-09-21
version: "2.4.12"
stars: 3.93902
reviews: 82
size: 56670208
website: http://airbitz.co
repository: 
issue: 
icon: co.airbitz.airbitz.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-06-25
  version: "2.4.12"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


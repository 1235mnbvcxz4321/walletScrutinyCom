---
wsId: bitpie
title: "Bitpie-Universal Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2021-08-11
version: "5.0.043"
stars: 3.57143
reviews: 28
size: 318623744
website: 
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
verdict: wip
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


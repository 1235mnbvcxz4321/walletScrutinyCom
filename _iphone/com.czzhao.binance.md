---
wsId: binance
title: "Binance: Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.czzhao.binance
appCountry: 
idd: 1436799971
released: 2018-10-06
updated: 2021-08-20
version: "2.34.1"
stars: 4.73241
reviews: 70704
size: 380882944
website: https://www.binance.com
repository: 
issue: 
icon: com.czzhao.binance.jpg
bugbounty: 
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: binance
providerLinkedIn: 
providerFacebook: binance
providerReddit: binance

redirect_from:

---

In the description the provider claims:

> Your funds are protected by our Secure Asset Fund for Users (SAFU Funds) which
  means we have your back.

which sounds very custodial and as such the app is **not verifiable**.

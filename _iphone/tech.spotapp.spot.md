---
wsId: SpotWalletapp
title: "Spot Wallet - Buy Bitcoin BTC"
altTitle: 
authors:
- leo
appId: tech.spotapp.spot
appCountry: 
idd: 1390560448
released: 2018-08-07
updated: 2021-08-12
version: "3.5.2"
stars: 4.61381
reviews: 4516
size: 103172096
website: https://spot-bitcoin.com
repository: 
issue: 
icon: tech.spotapp.spot.jpg
bugbounty: 
verdict: nosource
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: spot_bitcoin
providerLinkedIn: spot-bitcoin
providerFacebook: spot.bitcoin
providerReddit: 

redirect_from:

---

On their website we read:

> **You control your Bitcoins.**<br>
  PayPal, Coinbase & Binance control your funds. We don't. You have entire
  control over your Bitcoins. We use the best technologies to ensure that your
  funds are always safe.

but as we cannot find any source code to check this claim, the wallet gets the
verdict **not verifiable**.

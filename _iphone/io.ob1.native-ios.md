---
wsId: Haven
title: "Haven - Private Shopping"
altTitle: 

appId: io.ob1.native-ios
idd: 1318395690
released: 2019-08-19
updated: 2020-09-28
version: "1.3.8"
stars: 4.57979
reviews: 188
size: 82487296
website: https://gethaven.app/
repository: https://github.com/OpenBazaar/haven
issue: 
icon: io.ob1.native-ios.jpg
bugbounty: 
verdict: defunct
date: 2021-01-15
signer: 
reviewArchive:


providerTwitter: HavenPrivacy
providerLinkedIn: 
providerFacebook: 
providerReddit: havenapp

redirect_from:

---

**Update:** As of now, this app is not to be found on the App Store.

This provider had tweeted us a link to their source code after reviewing the
android version but in the App Store description or their website there is still
no [link to their code](https://github.com/OpenBazaar/haven).

And just like for the Android version, there is no complete build instructions
to reproduce the app. This app is **not verifiable**.

---
wsId: koinal
title: "Koinal: Buy Bitcoin instantly"
altTitle: 
authors:
- leo
appId: com.koinal.ios
appCountry: gb
idd: 1442764361
released: 2020-06-08
updated: 2021-08-11
version: "1.2.9"
stars: 4.91531
reviews: 2515
size: 61379584
website: https://www.koinal.io
repository: 
issue: 
icon: com.koinal.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: koinal_io
providerLinkedIn: koinal-io
providerFacebook: Koinal.io
providerReddit: 

redirect_from:

---

> Secure!<br>
  We take our system’s security and user safety extremely seriously. All Koinal
  systems use 256BIT RAPID SSL and Google two factor authentication. We can
  proudly state that our systems are extremely secure and we have a laser focus
  on protecting your data and investments!

This is their statement on security but it reads more like they are talking
about their servers than their wallet here. That would imply a custodial wallet.

As there are no further claims on the website neither, we assume the app is
custodial and thus **not verifiable**.

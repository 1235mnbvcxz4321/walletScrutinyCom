---
wsId: CoinDeal
title: "CoinDeal - Bitcoin Buy & Sell"
altTitle: 
authors:
- kiwilamb
appId: pl.icoindeal.CoinDeal
appCountry: 
idd: 1482619122
released: 2019-11-11
updated: 2020-08-18
version: "1.0.6"
stars: 1
reviews: 1
size: 24220672
website: https://coindeal.com/
repository: 
issue: 
icon: pl.icoindeal.CoinDeal.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-04
  version: "1.0.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: coindealcom
providerLinkedIn: coindealcom
providerFacebook: coindealcom
providerReddit: 

redirect_from:

---

This is an exchange based app wallet, meaning it is mainly developed to manage trading on an exchange.
The exchange provider typically stores users bitcoins, partly in cold storage, partly hot.

This leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.


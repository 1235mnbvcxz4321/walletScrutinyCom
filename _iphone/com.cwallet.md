---
wsId: Xapa
title: "Xapa"
altTitle: 
authors:
- kiwilamb
appId: com.cwallet
appCountry: 
idd: 1471981089
released: 2019-07-26
updated: 2021-04-08
version: "0.738.47"
stars: 4.18182
reviews: 11
size: 44941312
website: https://xapawallet.com/
repository: 
issue: 
icon: com.cwallet.jpg
bugbounty: 
verdict: custodial
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: WalletXapa
providerLinkedIn: 
providerFacebook: walletxapa
providerReddit: 

redirect_from:

---

This app is an exchange based wallet and as such typically private keys are custodial, we can verify this from the [providers own site](https://xapawallet.com/#1603507874611-a20bac97-a1d1) , as they make a clear statement of how the private keys are managed.

> Where are my private keys?
> As XapaWallet is custodial, so your private key stores in Xapa Server with complex security.

Our Verdict: This "wallet" is custodial and therefor **not verifiable**



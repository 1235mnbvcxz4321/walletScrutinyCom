---
wsId: Vauld
title: "Vauld"
altTitle: 
authors:
- kiwilamb
appId: com.bankofhodlers.app
appCountry: 
idd: 1509251174
released: 2020-05-12
updated: 2021-08-04
version: "2.3.2"
stars: 3.57143
reviews: 14
size: 65345536
website: https://www.vauld.com/
repository: 
issue: 
icon: com.bankofhodlers.app.jpg
bugbounty: 
verdict: custodial
date: 2021-05-08
signer: 
reviewArchive:


providerTwitter: Vauld_
providerLinkedIn: vauld
providerFacebook: VauldOfficial
providerReddit: BankofHodlers

redirect_from:

---

The Vauld website Help Center had an article "Security at Vauld" which covers a number of security risk.<br>
A statement of the management of the users "funds" makes it pretty clear the wallets private keys are in control of the provider.

> Our funds are managed through a multi signature system with the signatories being our co-founders.

Our verdict: This wallet is custodial and therefore **not verifiable**.

---
wsId: klever
title: "Klever Wallet: Bitcoin & more"
altTitle: 
authors:
- leo
appId: cash.klever.blockchain.wallet
appCountry: 
idd: 1525584688
released: 2020-08-26
updated: 2021-08-20
version: "4.4.1"
stars: 4.33251
reviews: 412
size: 130532352
website: https://klever.io
repository: 
issue: 
icon: cash.klever.blockchain.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-22
signer: 
reviewArchive:


providerTwitter: klever_io
providerLinkedIn: 
providerFacebook: klever.io
providerReddit: 

redirect_from:

---

On their website we read:

> **Peer-to-Peer**<br>
  Klever is a decentralized p2p and self-custody wallet network. Your Keys, your
  crypto.

so they claim the app is self-custodial but we cannot find any source code which
makes the app **not verifiable**.

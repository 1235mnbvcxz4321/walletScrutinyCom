---
wsId: geminiwallet
title: "Gemini: Buy Bitcoin Instantly"
altTitle: 
authors:
- leo
appId: com.gemini.ios
appCountry: 
idd: 1408914447
released: 2018-12-11
updated: 2021-08-18
version: "3.29.0"
stars: 4.77562
reviews: 54836
size: 110225408
website: http://gemini.com
repository: 
issue: 
icon: com.gemini.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive:


providerTwitter: gemini
providerLinkedIn: geminitrust
providerFacebook: GeminiTrust
providerReddit: 

redirect_from:

---

This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.

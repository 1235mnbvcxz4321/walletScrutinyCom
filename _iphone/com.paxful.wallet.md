---
wsId: Paxful
title: "Paxful | Marketplace & Wallet"
altTitle: 
authors:
- leo
appId: com.paxful.wallet
appCountry: 
idd: 1443813253
released: 2019-05-09
updated: 2021-07-23
version: "2.5.2"
stars: 3.84082
reviews: 2450
size: 60389376
website: https://paxful.com
repository: 
issue: 
icon: com.paxful.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: paxful
providerLinkedIn: 
providerFacebook: paxful
providerReddit: paxful

redirect_from:

---

In the App Store description we can read:

> **A TRUSTED WALLET**<br>
  Safely store your crypto in your own personal mobile cryptocurrency wallet,
  which you’ll receive for free upon creating your account. Enable two-factor
  authentication for an added layer of protection. Take it everywhere you go and
  check your balance any time.

which really doesn't say much about who is actually holding the bitcoins.

On the website we found:

> **Get a free wallet**<br>
  Get a life-time free Bitcoin wallet maintained by BitGo, the leading provider
  of secure Bitcoin wallets.

which tells us this provider delegates custody to BitGo: BitGo is one of the
major custodian for exchanges and other services in the space.

As a custodial offering this app is **not verifiable**.

---
title: "BitBox01 (Digital BitBox)"
appId: bitbox01
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.amazon.com/Digital-Bitbox-DBB1707-Cryptocurrency-Hardware/dp/B06XGKQ929
shop: https://www.amazon.com/Digital-Bitbox-DBB1707-Cryptocurrency-Hardware/dp/B06XGKQ929
company: Shift Crypto
companywebsite: https://shiftcrypto.ch
country: CH
price: 79USD
repository: https://github.com/digitalbitbox
issue:
icon: bitbox01.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-17
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: Shiftcrypto
providerReddit: 
---

This product has been superseded by the {% include walletLink.html wallet='hardware/bitBox2' verdict='true' %} device as [stated by the provider](https://shiftcrypto.support/help/en-us/16-bitbox01/143-bitbox01-end-of-sale-life-faqs) it is end of life. Bitbox1 owners can get a discount on the later bitbox2. Bitbox1 is considered discontinued.

For lack of a screen, this product would otherwise fall into our category {% include verdictBadge.html verdict="noita" type='medium' %}.

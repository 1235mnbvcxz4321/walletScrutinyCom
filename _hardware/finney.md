---
title: "FINNEY"
appId: finney
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://sirinlabs.com/
shop: https://shop.sirinlabs.com/products/finney
company: Sirin Labs
companywebsite: https://sirinlabs.com/
country: UK
price: 999USD
repository: 
issue:
icon: finney.png
bugbounty:
verdict: wip
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: SIRINLABS
providerLinkedIn: sirin-labs
providerFacebook: SirinLabs
providerReddit: 
---


---
title: "SatoChip"
appId: satochip
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 1]
weight: 10
website: https://satochip.io
shop: https://satochip.io/shop/
company: SatoChip
companywebsite: https://satochip.io
country: BE
price: 25EUR
repository: https://github.com/Toporin
issue:
icon: satochip.png
bugbounty:
verdict: noita # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: satochipwallet
providerLinkedIn: satochip
providerFacebook: 
providerReddit: 
---


This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.
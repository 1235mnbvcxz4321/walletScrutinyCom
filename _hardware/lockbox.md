---
title: "LockBox"
appId: lockbox
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [17.4, 57, 9.1]
weight: 16.2
website: https://www.blockchain.com/
shop: https://www.blockchain.com/
company: Blockchain
companywebsite: https://www.blockchain.com/
country: UK
price: 
repository: https://github.com/blockchain/
issue:
icon: lockbox.png
bugbounty:
verdict: wip
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: blockchain
providerLinkedIn: blockchain
providerFacebook: blockchain
providerReddit: 
---

This hardware wallet is a rebrand of a {% include walletLink.html wallet='hardware/ledgerNanoS' verdict=true %}.

It seems this hardware wallet is discontinued but it still has support pages available on [blockchain.com](https://support.blockchain.com/hc/en-us/categories/360001092591-Lockbox)

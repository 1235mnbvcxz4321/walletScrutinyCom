---
title: "Pi Wallet"
appId: piwallet
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite: https://www.pi-wallet.com
country: 
price: 
repository: 
issue:
icon: piwallet.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

The [provider's website](https://www.pi-wallet.com) is inaccessible and we considered it defunct.

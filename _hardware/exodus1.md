---
title: "HTC Exodus 1"
appId: exodus1
authors:
- kiwilamb
- leo
released: 2018-10-23
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.htcexodus.com/us/cryptophone/exodus1/
shop: 
company: HTC
companywebsite: https://www.htcexodus.com
country: US
price: 
repository: 
issue:
icon: exodus1.png
bugbounty:
verdict: wip
date: 2021-07-11
signer:
reviewArchive:


providerTwitter: htcexodus
providerLinkedIn: 
providerFacebook: htcexodus
providerReddit: HTCExodus
---


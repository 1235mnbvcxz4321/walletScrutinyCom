---
title: "Asamacura"
appId: asamacura
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: Asamacura
companywebsite: 
country: 
price: 
repository: 
issue:
icon: asamacura.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware wallet looks to be defunct, there is very little information that can be found about this wallet.
The only link for this wallet is a listing on the [Amazon.com site.](https://www.amazon.com/dp/B07FKWKBJ2)
No company or other product information can be found.

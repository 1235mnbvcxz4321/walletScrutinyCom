---
title: "KASSE HK-1000"
appId: kasse
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions:
weight: 
website: https://kasseusa.com/kasse-hk-1000-cryptocurrency-hardware-wallet/
shop: https://kasseusa.com/kasse-hk-1000-cryptocurrency-hardware-wallet/
company: KASSE USA (HyundaiPay)
companywebsite: https://kasseusa.com
country: US
price: 42.95USD
repository: 
issue:
icon: kasse.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: KasseUSA
providerLinkedIn: 
providerFacebook: KasseUSA
providerReddit: 
---


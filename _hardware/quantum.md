---
title: "QUANTUM"
appId: quantum
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [25, 65, 8]
weight: 12
website: https://security-arts.com/home
shop: https://security-arts.com/order
company: SecurityArts
companywebsite: https://security-arts.com
country: UA
price: 49USD
repository: 
issue:
icon: quantum.png
bugbounty:
verdict: wip 
date: 2021-07-27
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---


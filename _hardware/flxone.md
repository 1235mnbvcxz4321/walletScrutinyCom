---
title: "FLX One"
appId: flxone
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [32, 73, 12]
weight: 15
website: https://flxwallet.com/specifications.html
shop: https://www.amazon.com/FLX-One-Simple-use-Cryptocurrency/dp/B07JHMDV9X/ref=sr_1_2?ie=UTF8&qid=1540514793&sr=8-2&keywords=flx+wallet
company: FLX Partnership Limited
companywebsite: https://flxwallet.com/
country: US
price: 69.99USD
repository: 
issue:
icon: flxone.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---


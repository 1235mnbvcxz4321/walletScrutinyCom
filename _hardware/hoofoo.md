---
title: "HooFoo"
appId: hoofoo
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://hoofoo.io/products/hoofoo-hardware-wallet?variant=13765070225526
shop: https://hoofoo.io/products/hoofoo-hardware-wallet?variant=13765070225526
company: HooFoo Inc
companywebsite: https://hoofoo.io
country: US
price: 289USD
repository: 
issue:
icon: hoofoo.png
bugbounty:
verdict: unreleased
date: 2021-07-29
signer:
reviewArchive:


providerTwitter: hoofoowallet
providerLinkedIn: 
providerFacebook: hoofoohardwarewallet
providerReddit: 
---

On the [provider's website](https://hoofoo.io/products/hoofoo-hardware-wallet?variant=13765070225526) it states that this hardware wallet is coming soon, so a work in progress.
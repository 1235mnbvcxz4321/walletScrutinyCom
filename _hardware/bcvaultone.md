---
title: "BC Vault One"
appId: bcvaultone
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [57, 100, 10]
weight: 41
website: https://bc-vault.com/
shop: https://bc-vault.com/shop/
company: REAL SECURITY D.O.O..
companywebsite: https://www.real-sec.com/
country: SI
price: 132EUR
repository: 
issue:
icon: bcvaultone.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: bc_vault
providerLinkedIn: bcvault
providerFacebook: bcvault
providerReddit: 
---


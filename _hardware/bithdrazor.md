---
title: "BITHD Razor"
appId: bithdrazor
authors:
- kiwilamb
released: 2018-01-01
discontinued: # date
updated:
version:
dimensions: [83, 51, 2.2]
weight: 18
website: https://bithd.com/BITHD-Razor.html
shop: https://bithd.com/BITHD-Razor.html
company: BitHD
companywebsite: https://bithd.com
country: CH
price: 
repository: https://github.com/bithd
issue:
icon: bithdrazor.png
bugbounty:
verdict: wip
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---


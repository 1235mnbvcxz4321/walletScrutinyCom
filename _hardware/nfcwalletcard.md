---
title: "NFC Wallet Card"
appId: nfcwalletcard
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite: https://nfcwalletcard.com
country: 
price: 
repository: 
issue:
icon: nfcwalletcard.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

The [provider's website](https://nfcwalletcard.com) is inaccessible and we considered it defunct.
---
title: "Esecubit"
appId: esecubit
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite: https://www.esecubit.com
country: 
price: 
repository: 
issue:
icon: esecubit.png
bugbounty:
verdict: defunct
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: EsecuBit
providerReddit: 
---

This hardware wallet looks to be defunct, the provider’s [main site is not accessible](https://www.esecubit.com).


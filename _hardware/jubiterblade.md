---
title: "JuBiter Blade"
appId: jubiterblade
authors:
- kiwilamb
released: 2018-11-01
discontinued: # date
updated:
version:
dimensions: [38, 64, 2.5]
weight: 
website: https://www.jubiterwallet.com/index.html
shop: https://www.amazon.com/gp/product/B07K446Y57
company: Feitian Technologies Co.,Ltd.
companywebsite: https://www.ftsafe.com/
country: CH
price: 79USD
repository: 
issue:
icon: jubiterblade.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: JuBiterWallet
providerLinkedIn: jubiter-wallet
providerFacebook: JuBiterWallet
providerReddit: 
---


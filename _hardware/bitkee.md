---
title: "Bitkee"
appId: bitkee
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite: https://bitkee.com
country: 
price: 
repository: 
issue:
icon: bitkee.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

On the [bitkee site](https://bitkee.com) they state the production of this wallet has been discontinued.

---
title: "CoolWallet Pro"
appId: coolwalletpro
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 0.8]
weight: 6
website: https://www.coolwallet.io/product/coolwallet-pro/
shop: https://www.coolwallet.io/product/coolwallet-pro/
company: CoolBitX
companywebsite: https://coolbitx.com/
country: TW
price: 149USD
repository: 
issue:
icon: coolwalletpro.png
bugbounty:
verdict: wip
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: coolwallet
providerLinkedIn: 
providerFacebook: coolwallet
providerReddit: 
---


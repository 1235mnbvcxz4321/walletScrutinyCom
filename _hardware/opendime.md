---
title: "Opendime"
appId: opendime
authors:
- kiwilamb
- felipe
- leo
released: 2016-04-01
discontinued: # date
updated: 
version: 
dimensions:
weight: 
website: https://opendime.com/
shop: https://store.coinkite.com/store/opendime
country: CA
price: 49USD (3 pack)
repository: https://github.com/opendime
issue: 
icon: opendime.png
bugbounty: 
verdict: plainkey
date: 2021-07-17
signer: 
reviewArchive:
- date: 2021-07-11  
  version: "" 
  appHash: 
  gitRevision: 
  verdict: noita

providerTwitter: COLDCARDwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

To spend the funds, the private keys need to get exposed to potentially insecure
systems. A virus on that system could empty the wallet the second the device
gets plugged into the USB port.

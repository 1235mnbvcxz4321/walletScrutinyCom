---
title: "AT.Wallet"
appId: atwallet
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 2.2]
weight: 
website: https://authentrend.com/at-wallet/
shop: https://www.amazon.com/dp/B084Q4FRPT
company: AuthenTrend
companywebsite: https://authentrend.com/
country: TW
price: 120USD
repository: 
issue:
icon: atwallet.png
bugbounty:
verdict: wip
date: 2021-07-23
signer:
reviewArchive:


providerTwitter: authentrend
providerLinkedIn: authentrend
providerFacebook: authentrend
providerReddit: 
---


---
title: "Ballet Crypto"
appId: balletcrypto
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 86, 1]
weight: 35
website: https://www.balletcrypto.com/en/buy/
shop: https://www.balletcrypto.com/en/buy/
company: Ballet
companywebsite: https://www.balletcrypto.com
country: US
price: 35USD
repository: 
issue:
icon: balletcrypto.png
bugbounty:
verdict: prefilled
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: BalletCrypto
providerLinkedIn: balletcrypto
providerFacebook: balletcrypto.global
providerReddit: 
---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.


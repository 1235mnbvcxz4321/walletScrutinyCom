---
title: "Ledger HW.1"
appId: ledgerhw1
authors:
- kiwilamb
released:
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: Ledger
companywebsite: https://www.ledger.com
country: FR
price: 
repository: https://github.com/LedgerHQ
issue:
icon: ledgerhw1.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: Ledger
providerLinkedIn: ledgerhq
providerFacebook: Ledger
providerReddit: 
---

The [provider's website has a clear statement](https://support.ledger.com/hc/en-us/articles/360010500620-Discontinued-products?docs=true) that this wallet is discontinued and we considered it defunct.

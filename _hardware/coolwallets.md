---
title: "CoolWallet S"
appId: coolwallets
authors:
- kiwilamb
released: 2018-01-01
discontinued: # date
updated:
version:
dimensions: [54, 85, 0.8]
weight: 6
website: https://www.coolwallet.io/coolwallet_s
shop: https://www.coolwallet.io/product/coolwallet/
company: CoolBitX
companywebsite: https://coolbitx.com/
country: TW
price: 99USD
repository: 
issue:
icon: coolwallets.png
bugbounty:
verdict: wip
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: coolwallet
providerLinkedIn: 
providerFacebook: coolwallet
providerReddit: 
---


---
title: "BlochsTech card"
appId: blochstech
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 1]
weight: 
website: http://www.blochstech.com/
shop: http://www.blochstech.com/
company: BlochsTech
companywebsite: http://www.blochstech.com
country: DK
price: 19.95EUR
repository: https://github.com/BlochsTech/BitcoinCardTerminal
issue:
icon: blochstech.png
bugbounty:
verdict: prefilled
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.
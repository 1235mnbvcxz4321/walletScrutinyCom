---
title: "Blockstream Jade"
appId: blockstreamjade
authors:
- kiwilamb
released: 44197
discontinued: # date
updated:
version:
dimensions: [24, 60, 17]
weight: 
website: https://blockstream.com/jade/
shop: https://store.blockstream.com/product/blockstream-jade-token/
company: Blockstream Corporation Inc.
companywebsite: https://blockstream.com/
country: CA
price: 39.99USD
repository: https://github.com/Blockstream/jade
issue:
icon: blockstreamjade.png
bugbounty:
verdict: wip 
date: 2021-07-27
signer:
reviewArchive:


providerTwitter: Blockstream
providerLinkedIn: blockstream
providerFacebook: Blockstream
providerReddit: 
---


---
title: "Swiss Bank in Your Pocket"
appId: swissbankinyourpocket
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: https://www.amazon.com/Swiss-Bank-Your-Pocket-SBIYP/dp/B0773KZCMK
company: Swiss Bank in Your Pocket
companywebsite: https://swissbankinyourpocket.com/
country: 
price: 99USD
repository: 
issue:
icon: swissbankinyourpocket.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-15
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

It is difficult to determine if this wallet ever existed, launched in 2017, it is highly likely this device is not available anymore.
We will consider this device defunct as there is no clear links to buy this product and the [main site] (https://swissbankinyourpocket.com/) is unavailable.

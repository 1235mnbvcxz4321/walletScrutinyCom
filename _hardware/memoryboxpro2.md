---
title: "Memory BOX Pro 2.0"
appId: memoryboxpro2
authors:
- kiwilamb
released: 2019-01-01
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.starteos.io/en/product
shop: 
company: Starteos
companywebsite: https://www.starteos.io
country: CH
price: 
repository: 
issue:
icon: memoryboxpro2.png
bugbounty:
verdict: wip 
date: 2021-07-24
signer:
reviewArchive:


providerTwitter: Starteos
providerLinkedIn: 
providerFacebook: starteos.io.7
providerReddit: Starteos
---


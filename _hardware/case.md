---
title: "Case"
appId: case
authors:
- kiwilamb
released: 
discontinued: 2017-11-01
updated:
version:
dimensions: 
weight: 
website: https://choosecase.com
shop: https://choosecase.com
company: CryptoLabs
companywebsite: https://choosecase.com
country: US
price: 
repository: 
issue:
icon: case.png
bugbounty:
verdict: defunct
date: 2021-07-18
signer:
reviewArchive:


providerTwitter: CaseWallet
providerLinkedIn: 
providerFacebook: casewallet
providerReddit: 
---

There are a numnber of reasons why this verdict is defunct.
The provider website states the hardware wallet is "soldout".
Last post on Twitter September 2017.
Last post on facebook August 2017.
The [providers website](https://choosecase.com) secure SSL certificate is no longer valid and expired on 12/05/2018.

Also found a post on a [wordpress blog](https://choosecase.wordpress.com/), that states the wallet is discontinued Nov 2017.

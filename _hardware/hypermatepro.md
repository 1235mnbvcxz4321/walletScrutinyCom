---
title: "HyperMate Pro"
appId: hypermatepro
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [44, 70, 4.5]
weight: 
website: https://hyperpay.tech/hypermatepro/pro
shop: https://shop.hyperpay.tech/collections/hypermate-g/products/hypermate-pro
company: HyperPay
companywebsite: https://hyperpay.tech/
country: HK
price: 229USD
repository: https://github.com/hyperpayorg/hardwallet
issue:
icon: hypermatepro.png
bugbounty:
verdict: wip
date: 2021-08-03
signer:
reviewArchive:


providerTwitter: HyperPay_tech
providerLinkedIn: 
providerFacebook: hyperpayofficial
providerReddit: 
---


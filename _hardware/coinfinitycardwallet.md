---
title: "Coinfinity CardWallet"
appId: coinfinitycardwallet
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 86, 1]
weight: 
website: https://www.cardwallet.com/en/home/
shop: https://www.cardwallet.com/en/shop/
company: Coinfinity
companywebsite: https://coinfinity.co
country: AT
price: 39.9EUR
repository: 
issue:
icon: coinfinitycardwallet.png
bugbounty:
verdict: prefilled
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: CardwalletCom
providerLinkedIn: coinfinity
providerFacebook: cardwalletcoinfinity
providerReddit: 
---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.
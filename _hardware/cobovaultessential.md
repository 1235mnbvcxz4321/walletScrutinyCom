---
title: "Keystone"
appId: cobovaultessential
authors:
- kiwilamb
released: 2018-03-01
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://keyst.one/
shop: 
company: Keystone
companywebsite: https://shop.keyst.one/products/keystone-essential
country: CH
price: 119USD
repository: https://github.com/KeystoneHQ
issue:
icon: cobovaultessential.png
bugbounty:
verdict: wip
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: KeystoneWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

Cobovault has been renamed to Keystone, previously product were known as Cobovault Essential, Cobovault Pro and Cobovault Ultimate.



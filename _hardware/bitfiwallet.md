---
title: "Bitfi.2 Wallet"
appId: bitfiwallet
authors:
- kiwilamb
released: 2019-03-01
discontinued: # date
updated:
version:
dimensions: [115, 67, 8]
weight: 
website: https://bitfi.com/
shop: https://bitfi.com/order
company: Bitfi, Inc
companywebsite: https://bitfi.com
country: 
price: 199USD
repository: https://github.com/Bitfi
issue:
icon: bitfiwallet.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: thebitfi
providerLinkedIn: bitfi
providerFacebook: TheBitfi
providerReddit: 
---


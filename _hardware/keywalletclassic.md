---
title: "KeyWallet Classic"
appId: keywalletclassic
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: http://www.keywallet.co.kr/
shop: 
company: 
companywebsite: http://www.keywallet.co.kr
country: 
price: 
repository: 
issue:
icon: keywalletclassic.png
bugbounty:
verdict: defunct
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware wallet looks to be defunct, the provider's [main site is not accessible](http://www.keywallet.co.kr) 

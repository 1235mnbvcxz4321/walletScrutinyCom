---
title: "Archos Safe-T Touch"
appId: archossafettouch
authors:
- kiwilamb
released: 2019-03-31 # actually "Q1" according to the website
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.archos.com/nz/products/crypto/archos_safettouch/index.html
shop: https://shop.archos.com/gb/hardware-wallets/719-archos-safe-t-touch-0690590037359.html
company: Archos
companywebsite: https://www.archos.com/
country: FR
price: 129.99EUR
repository: 
issue:
icon: archossafettouch.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: ARCHOS
providerLinkedIn: archos
providerFacebook: ArchosOfficial
providerReddit: 
---


---
title: "Sirinv3"
appId: sirinv3
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://sirinlabs.com/
shop: https://shop.sirinlabs.com/products/sirin-v3%E2%84%A2
company: Sirin Labs
companywebsite: https://sirinlabs.com/
country: UK
price: 2650USD
repository: 
issue:
icon: sirinv3.png
bugbounty:
verdict: wip
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: SIRINLABS
providerLinkedIn: sirin-labs
providerFacebook: SirinLabs
providerReddit: 
---


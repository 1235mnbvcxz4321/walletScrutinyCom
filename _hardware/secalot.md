---
title: "Secalot"
appId: secalot
authors:
- kiwilamb
released: 2018-01-01
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.secalot.com/
shop: https://www.secalot.com/product/secalot-dongle/
company: 
companywebsite: https://www.secalot.com
country: 
price: 50EUR
repository: 
issue:
icon: secalot.png
bugbounty:
verdict: noita
date: 2021-07-21
signer:
reviewArchive:


providerTwitter: SecalotDongle
providerLinkedIn: 
providerFacebook: secalotDongle
providerReddit: 
---

This USB device lacks a screen and does not allow a user to verify what is actually being signed.
A transaction is sent to the device for signing and the user blindly presses a button on the device as confirmation.


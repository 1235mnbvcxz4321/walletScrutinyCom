---
wsId: 
title: "Bitcoin Global: P2P platform for crypto exchange"
altTitle: 
authors:

users: 5000
appId: com.bitcoinglobal
released: 2020-09-25
updated: 2021-08-20
version: "2.7.1"
stars: 4.7
ratings: 37
reviews: 17
size: 26M
website: 
repository: 
issue: 
icon: com.bitcoinglobal.png
bugbounty: 
verdict: wip
date: 2021-04-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



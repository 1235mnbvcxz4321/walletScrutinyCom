---
wsId: 
title: "Bitberry : Safe Cryptocurrency Wallet"
altTitle: 
authors:

users: 100000
appId: com.rootone.wallet
released: 2018-10-08
updated: 2021-06-17
version: "1.4.4"
stars: 3.0
ratings: 583
reviews: 456
size: 6.6M
website: 
repository: 
issue: 
icon: com.rootone.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



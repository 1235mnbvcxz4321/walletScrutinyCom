---
wsId: 
title: "MEW wallet – Ethereum wallet"
altTitle: 
authors:

users: 500000
appId: com.myetherwallet.mewwallet
released: 2020-03-11
updated: 2021-08-06
version: "2.0.2"
stars: 3.4
ratings: 4852
reviews: 2298
size: 49M
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



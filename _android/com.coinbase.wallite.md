---
wsId: 
title: "Coinbase Wallet Lite"
altTitle: 
authors:

users: 50000
appId: com.coinbase.wallite
released: 2019-04-16
updated: 2020-07-17
version: "0.5-alpha"
stars: 3.7
ratings: 285
reviews: 180
size: 6.7M
website: https://wallet.coinbase.com
repository: 
issue: 
icon: com.coinbase.wallite.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-31
  version: "0.5-alpha"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nobtc

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is provided by the same developer as
{% include walletLink.html wallet='android/com.coinbase.android' %}, the
[provided website](https://wallet.coinbase.com/) links to yet another app,
{% include walletLink.html wallet='android/org.toshi' %} though. It's a bit confusing.

Reading the description it turns out, this app is for ETH only.

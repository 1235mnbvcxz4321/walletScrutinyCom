---
wsId: 
title: "Decrypt - Bitcoin & crypto news"
altTitle: 
authors:

users: 100000
appId: co.decrypt.app
released: 2020-05-06
updated: 2021-07-13
version: "3.0"
stars: 4.2
ratings: 2008
reviews: 973
size: 31M
website: 
repository: 
issue: 
icon: co.decrypt.app.png
bugbounty: 
verdict: nowallet
date: 2020-12-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.decrypt.app/
---


This app only provides news about Bitcoin but no wallet itself.

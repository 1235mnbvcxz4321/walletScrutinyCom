---
wsId: 
title: "Ciwa - CITEX Wallet"
altTitle: 
authors:

users: 500
appId: com.citex.wallet
released: 2019-11-09
updated: 2020-02-08
version: "1.3"
stars: 3.0
ratings: 12
reviews: 5
size: 11M
website: 
repository: 
issue: 
icon: com.citex.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



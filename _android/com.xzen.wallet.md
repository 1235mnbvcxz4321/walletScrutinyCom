---
wsId: 
title: "XZEN — Bitcoin Wallet and Exchange"
altTitle: 
authors:
- leo
users: 5000
appId: com.xzen.wallet
released: 2018-12-25
updated: 2019-09-19
version: "1.2.6.0"
stars: 3.7
ratings: 42
reviews: 31
size: 26M
website: http://www.xzen.io/app
repository: 
issue: 
icon: com.xzen.wallet.png
bugbounty: 
verdict: defunct
date: 2021-05-15
signer: 
reviewArchive:
- date: 2020-05-29
  version: "1.2.6.0"
  appHash: 
  gitRevision: 13d8e0095e0944d8d255811487d819fafc74c5e1
  verdict: custodial

providerTwitter: XZENwallet
providerLinkedIn: 
providerFacebook: xzenwallet
providerReddit: 

redirect_from:
  - /com.xzen.wallet/
  - /posts/com.xzen.wallet/
---


While this app still appears to be on Google Play,
[their website](https://xzen.io/) now reads:

> **XZEN wallet is closed.**<br>
  In case of any issues, please contact us by email using the form below.

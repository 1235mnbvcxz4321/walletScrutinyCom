---
wsId: 
title: "Spark Lightning Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: com.spark.wallet
released: 2018-12-10
updated: 2020-09-03
version: "0.0.0"
stars: 4.2
ratings: 9
reviews: 3
size: 8.0M
website: https://github.com/shesek/spark-wallet
repository: 
issue: 
icon: com.spark.wallet.png
bugbounty: 
verdict: wip
date: 2019-12-29
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.spark.wallet/
  - /posts/com.spark.wallet/
---



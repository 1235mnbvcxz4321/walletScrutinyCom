---
wsId: 
title: "Beehives Wallet - Bitcoin Crypto Wallet"
altTitle: 
authors:

users: 1000
appId: tech.vontech.blockchain.jetoken
released: 2019-04-10
updated: 2019-06-21
version: "1.0.12"
stars: 4.6
ratings: 28
reviews: 12
size: 7.6M
website: 
repository: 
issue: 
icon: tech.vontech.blockchain.jetoken.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.0.12"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



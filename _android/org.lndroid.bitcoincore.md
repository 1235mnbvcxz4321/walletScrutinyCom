---
wsId: 
title: "BitcoinCore for Android"
altTitle: 
authors:

users: 1000
appId: org.lndroid.bitcoincore
released: 2020-07-02
updated: 2020-07-08
version: "0.6"
stars: 4.1
ratings: 8
reviews: 1
size: 7.5M
website: 
repository: 
issue: 
icon: org.lndroid.bitcoincore.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-08
  version: "0.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/189 -->

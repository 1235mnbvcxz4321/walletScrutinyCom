---
wsId: 
title: "SimpleSwap: Cryptocurrency Instant Exchange"
altTitle: 
authors:

users: 10000
appId: com.simpleswapapp
released: 2020-05-23
updated: 2021-08-17
version: "3.1.6"
stars: 4.3
ratings: 269
reviews: 210
size: 38M
website: 
repository: 
issue: 
icon: com.simpleswapapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



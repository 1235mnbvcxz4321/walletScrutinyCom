---
wsId: 
title: "Elaphant - BTC ETH ELA Wallet"
altTitle: 
authors:

users: 100
appId: app.elaphant.wallets
released: 2020-10-08
updated: 2021-05-03
version: "1.99.15"
stars: 3.9
ratings: 7
reviews: 4
size: 95M
website: 
repository: 
issue: 
icon: app.elaphant.wallets.png
bugbounty: 
verdict: fewusers
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: Bexplus
title: "Bitcoin Wallet for Margin Trading - Bexplus App"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.lingxi.bexplus
released: 2018-10-29
updated: 2021-07-21
version: "2.1.1"
stars: 4.7
ratings: 4930
reviews: 1927
size: 11M
website: https://www.bexplus.com
repository: 
issue: 
icon: com.lingxi.bexplus.png
bugbounty: 
verdict: custodial
date: 2021-04-22
signer: 
reviewArchive:


providerTwitter: BexplusExchange
providerLinkedIn: 
providerFacebook: 
providerReddit: Bexplus

redirect_from:

---


The Bexplus website states under the mobile wallet section "Assets Security"

> Assets are stored in cold storage against stealing and loss

this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.


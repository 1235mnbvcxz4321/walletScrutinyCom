---
wsId: AirGapWallet
title: "AirGap Wallet - Tezos, Cosmos, Ethereum, Bitcoin"
altTitle: 
authors:

users: 5000
appId: it.airgap.wallet
released: 2018-08-06
updated: 2021-07-29
version: "3.8.0"
stars: 4.1
ratings: 91
reviews: 41
size: 73M
website: https://www.airgap.it
repository: 
issue: 
icon: it.airgap.wallet.png
bugbounty: 
verdict: nowallet
date: 2019-12-29
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:
  - /it.airgap.wallet/
  - /posts/it.airgap.wallet/
---


This appears to not be a wallet as to our understanding, it does not hold any
private keys but delegates that part to {% include walletLink.html wallet='android/it.airgap.vault' %}
without which it does not work.

> **AirGap Vault**, the private key is generated and securely stored in the
  AirGap Vault app. **You have to install AirGap Vault to use AirGap Wallet**
  https://play.google.com/store/apps/details?id=it.airgap.vault
  

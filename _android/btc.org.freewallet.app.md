---
wsId: 
title: "Bitcoin Wallet. Buy & Exchange BTC coin－Freewallet"
altTitle: 
authors:
- leo
users: 1000000
appId: btc.org.freewallet.app
released: 2016-06-13
updated: 2021-06-01
version: "2.5.9"
stars: 4.3
ratings: 5479
reviews: 2716
size: 7.3M
website: https://freewallet.org
repository: 
issue: 
icon: btc.org.freewallet.app.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: Freewallet_org

redirect_from:
  - /btc.org.freewallet.app/
  - /posts/btc.org.freewallet.app/
---


According to their description on Google Play, this is a custodial app:

> The Freewallet team keeps most of our customers’ coins in offline cold storage
to ensure the safety of your funds.

Our verdict: This app is **not verifiable**.

---
wsId: 
title: "Kriptomat - The easiest way to buy and own Bitcoin"
altTitle: 
authors:

users: 10000
appId: io.kriptomat.app
released: 2018-12-17
updated: 2021-08-12
version: "1.8.3"
stars: 4.1
ratings: 776
reviews: 385
size: 45M
website: 
repository: 
issue: 
icon: io.kriptomat.app.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



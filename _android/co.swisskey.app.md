---
wsId: 
title: "Swiss Key"
altTitle: 
authors:

users: 100
appId: co.swisskey.app
released: 2020-02-04
updated: 2020-02-04
version: "0.0.1"
stars: 0.0
ratings: 
reviews: 
size: 18M
website: 
repository: 
issue: 
icon: co.swisskey.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "0.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



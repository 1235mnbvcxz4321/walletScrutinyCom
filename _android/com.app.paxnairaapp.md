---
wsId: 
title: "Paxnaira - Buy & Sell Bitcoin"
altTitle: 
authors:

users: 1000
appId: com.app.paxnairaapp
released: 2019-07-29
updated: 2019-07-29
version: "1.0"
stars: 3.7
ratings: 22
reviews: 18
size: 3.5M
website: 
repository: 
issue: 
icon: com.app.paxnairaapp.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



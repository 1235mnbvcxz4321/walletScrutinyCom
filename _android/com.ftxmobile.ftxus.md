---
wsId: ftxus
title: "FTX US"
altTitle: 
authors:
- leo
users: 10000
appId: com.ftxmobile.ftxus
released: 2020-05-18
updated: 2021-01-28
version: "1.1.0"
stars: 3.8
ratings: 259
reviews: 234
size: 46M
website: 
repository: 
issue: 
icon: com.ftxmobile.ftxus.png
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: ftx_us
providerLinkedIn: 
providerFacebook: FTXUS
providerReddit: 

redirect_from:

---


**Update 2021-08-09**: There are a total of 6 related apps that all appear to belong to the same "FTX":

* {% include walletLink.html wallet='android/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='iphone/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftx' %}
* {% include walletLink.html wallet='iphone/org.reactjs.native.example.FTXMobile.FTX' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftxus' %}
* {% include walletLink.html wallet='iphone/com.ftx.FTXMobile.FTXUS' %}

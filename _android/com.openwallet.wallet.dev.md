---
wsId: 
title: "Murex | Universal"
altTitle: 
authors:

users: 500
appId: com.openwallet.wallet.dev
released: 
updated: 2017-09-18
version: "v1.0.1"
stars: 4.5
ratings: 13
reviews: 11
size: 5.7M
website: 
repository: 
issue: 
icon: com.openwallet.wallet.dev.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "v1.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



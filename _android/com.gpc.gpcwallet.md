---
wsId: 
title: "GPC Wallet"
altTitle: 
authors:

users: 10
appId: com.gpc.gpcwallet
released: 2020-07-14
updated: 2020-08-05
version: "1.6"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: 
repository: 
issue: 
icon: com.gpc.gpcwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



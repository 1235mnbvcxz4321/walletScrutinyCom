---
wsId: 
title: "Korvio Wallet - Multichain Blockchain Technology"
altTitle: 
authors:

users: 10000
appId: com.wallet.korvio
released: 2019-06-21
updated: 2021-03-18
version: "1.0.25"
stars: 3.9
ratings: 469
reviews: 169
size: 20M
website: 
repository: 
issue: 
icon: com.wallet.korvio.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



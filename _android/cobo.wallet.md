---
wsId: cobowallet
title: "Cobo: Support crypto savings, PoS, gain products."
altTitle: 
authors:
- leo
users: 100000
appId: cobo.wallet
released: 2018-07-01
updated: 2021-08-13
version: "5.12.0"
stars: 3.2
ratings: 3953
reviews: 2409
size: 37M
website: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.png
bugbounty: 
verdict: nosource
date: 2020-06-18
signer: 
reviewArchive:


providerTwitter: Cobo_Wallet
providerLinkedIn: coboofficial
providerFacebook: coboOfficial
providerReddit: 

redirect_from:
  - /cobo/
  - /cobo.wallet/
  - /posts/2019/11/cobo/
  - /posts/cobo.wallet/
---


**Update:** This wallet was one of the first wallets that was analyzed and back
then the custodial wallet was detrimental to call the full wallet custodial.
As it apparently also has a non-custodial part, this verdict is not accurate
anymore.

This app is at least primarily a custodial wallet:

> **Cloud Wallet**
> 
> Designed for user convenience and worry-free investing
> 
> Generate stable returns on your crypto assets
> 
> Free, instant transfers between Cobo Wallet users

but also has a non-custodial

> **HD Wallet**
> 
> Take full control of your assets and private keys for maximum safety
> 
> Easily import your mnemonic seeds from other wallets you may own
> 
> Support for DApps
> 
> EOS & Tron voting

As there is no source code available, claims cannot be checked, which is why the
verdict has to be: this wallet is **not verifiable**.

---
wsId: 
title: "GetCoins Wallet"
altTitle: 
authors:

users: 1000
appId: com.getcoins.gcwallet2
released: 2019-03-21
updated: 2020-05-13
version: "3.6.0"
stars: 2.6
ratings: 14
reviews: 11
size: 18M
website: 
repository: 
issue: 
icon: com.getcoins.gcwallet2.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "3.6.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



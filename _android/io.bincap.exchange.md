---
wsId: 
title: "Bincap — bitcoin exchange and wallet"
altTitle: 
authors:

users: 500
appId: io.bincap.exchange
released: 2019-08-08
updated: 2020-05-02
version: "2.0.1"
stars: 4.4
ratings: 13
reviews: 12
size: 6.7M
website: 
repository: 
issue: 
icon: io.bincap.exchange.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "2.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "MELONA for BitMEX"
altTitle: 
authors:

users: 1000
appId: artifyapp.com.coconut
released: 2018-07-05
updated: 2018-10-22
version: "1.1.1"
stars: 3.9
ratings: 12
reviews: 6
size: 2.8M
website: 
repository: 
issue: 
icon: artifyapp.com.coconut.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



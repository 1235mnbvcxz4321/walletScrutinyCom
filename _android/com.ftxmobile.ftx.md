---
wsId: FTXPro
title: "FTX Pro"
altTitle: 
authors:
- leo
users: 100000
appId: com.ftxmobile.ftx
released: 2020-05-08
updated: 2021-07-30
version: "1.1.3"
stars: 4.2
ratings: 4370
reviews: 1960
size: 62M
website: https://ftx.com
repository: 
issue: 
icon: com.ftxmobile.ftx.png
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: FTX_Official
providerLinkedIn: 
providerFacebook: ftx.official
providerReddit: 

redirect_from:

---


**Update 2021-08-09**: There are a total of 6 related apps that all appear to belong to the same "FTX":

* {% include walletLink.html wallet='android/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='iphone/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftx' %}
* {% include walletLink.html wallet='iphone/org.reactjs.native.example.FTXMobile.FTX' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftxus' %}
* {% include walletLink.html wallet='iphone/com.ftx.FTXMobile.FTXUS' %}
 
On their description there is not much to be found about it being even a wallet
for Bitcoin but as you can deposit Bitcoins into your account and withdraw them,
it technically works like a wallet but

> FTX is a cryptocurrency derivatives exchange built by traders, for traders.

and that is most likely custodial. Absent contrary claims we file it as such and
assume it is **not verifiable**.

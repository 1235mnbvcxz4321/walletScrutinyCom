---
wsId: ViaWallet
title: "ViaWallet - Multi chain Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.viabtc.wallet
released: 2019-05-15
updated: 2021-08-09
version: "2.4.0"
stars: 4.1
ratings: 324
reviews: 132
size: 54M
website: https://viawallet.com
repository: 
issue: 
icon: com.viabtc.wallet.png
bugbounty: 
verdict: nosource
date: 2020-04-27
signer: 
reviewArchive:


providerTwitter: viawallet
providerLinkedIn: 
providerFacebook: ViaWallet
providerReddit: 

redirect_from:
  - /com.viabtc.wallet/
  - /posts/com.viabtc.wallet/
---


This app's description contains

> Users' self-control for private key to manage assets

which sounds like a claim to being non-custodial.

Also on their [FAQ](https://support.viawallet.com/hc/en-us/articles/900000212786-Is-my-digital-assets-safely-stored-in-ViaWallet-)
we read:

> **Is my digital assets safely stored in ViaWallet?**
> ViaWallet is a subsidiary brand of ViaBTC, which was founded in May 2016 as an
  innovation-intensive startup dedicated to cryptocurrency with rich
  technological prowess and experience in global operation of blockchain
  industry. With the most state-of-the-art Fintech, we aims to provide a
  self-controlled and easy-accessible multi-cryptocurrency wallet across
  devices, safeguarded with industry-leading security for your digital assets.

which says "self-controlled" but nowhere do we find a hint at this wallet's
source code. We assume it's claiming to be non-custodial but remain with the
verdict: **not verifiable**.

---
wsId: 
title: "2cash - Money for Everyone"
altTitle: 
authors:

users: 5000
appId: com.voicapps.app2cash
released: 2019-09-17
updated: 2020-11-23
version: "1.02.4"
stars: 4.3
ratings: 54
reviews: 23
size: 40M
website: 
repository: 
issue: 
icon: com.voicapps.app2cash.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



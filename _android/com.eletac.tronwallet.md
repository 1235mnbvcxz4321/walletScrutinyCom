---
wsId: visionwallet
title: "Vision: Bitcoin and Crypto Wallet"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.eletac.tronwallet
released: 2018-05-13
updated: 2021-05-30
version: "1.2.9"
stars: 4.4
ratings: 1172
reviews: 605
size: 94M
website: https://www.vision-crypto.com
repository: 
issue: 
icon: com.eletac.tronwallet.png
bugbounty: 
verdict: nosource
date: 2021-04-23
signer: 
reviewArchive:


providerTwitter: VisionCryptoApp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


We do not have to look far to find the wallet claims to be non-custodial.

> When you create a wallet, it is very important that you store your received recovery phrase safely, because nobody but you has and should have access to your wallet.

However such claims need to be verified and this wallets source code is nowhere to be found.

Our verdict: This ‘wallet’ claims to be non-custodial, however with no source code this wallet is **not verifiable**.


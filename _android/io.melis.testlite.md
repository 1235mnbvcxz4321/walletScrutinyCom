---
wsId: 
title: "Melis Lite Testnet"
altTitle: 
authors:

users: 10
appId: io.melis.testlite
released: 2019-08-06
updated: 2019-08-06
version: "0.4.0"
stars: 0.0
ratings: 
reviews: 
size: 16M
website: 
repository: 
issue: 
icon: io.melis.testlite.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "0.4.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



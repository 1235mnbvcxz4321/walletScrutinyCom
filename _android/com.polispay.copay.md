---
wsId: PolisPay
title: "PolisPay - Cryptocurrency wallet"
altTitle: 
authors:
- leo
users: 5000
appId: com.polispay.copay
released: 2018-02-21
updated: 2021-05-03
version: "8.9.2"
stars: 4.2
ratings: 128
reviews: 76
size: 10M
website: https://www.polispay.com
repository: 
issue: 
icon: com.polispay.copay.png
bugbounty: 
verdict: nosource
date: 2020-04-07
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.polispay.copay/
  - /posts/com.polispay.copay/
---


This app appears to be a [CoPay](/copay/) clone given its app ID:
`com.polispay.copay`.

In the app's description we read:

> A unique wallet based on mnemonic phrases and public and private extended keys
> PolisPay wallet is impossible to hack.

which sounds like a claim to be non-custodial. Let's see if there is source code
somewhere ... nope. No mention of its source code or a repository.

This app is **not verifiable**.


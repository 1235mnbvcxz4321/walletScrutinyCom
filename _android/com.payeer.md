---
wsId: 
title: "PAYEER"
altTitle: 
authors:
- danny
users: 1000000
appId: com.payeer
released: 2018-01-03
updated: 2020-12-24
version: "Varies with device"
stars: 4.2
ratings: 31893
reviews: 17429
size: Varies with device
website: https://payeer.com
repository: 
issue: 
icon: com.payeer.png
bugbounty: 
verdict: custodial
date: 2021-08-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: payeercoins
providerReddit: 

redirect_from:

---


Taken from the Play Store description:
> Exchange Fiat and Cryptocurrencies on Trade Platform or Convert automatically, Fund up your account with VISA and MasterCard, Make payments worldwide between more than 20.000.000 PAYEER accounts from 127 countries of the world!

Sounds like an exchange. PAYEER isn't mainly focused on cryptocurrencies though it still refers to itself as an e-wallet.

There's no word about security or self-custody. PAYEER sounds more like a custodial offering and therefore **not verifiable**.

---
wsId: CoinEx
title: "CoinEx - A Trustworthy Cryptocurrency Exchange"
altTitle: 
authors:
- leo
users: 500000
appId: com.coinex.trade.play
released: 2019-12-27
updated: 2021-08-09
version: "3.1.1"
stars: 4.7
ratings: 30539
reviews: 9393
size: 15M
website: https://www.coinex.co
repository: 
issue: 
icon: com.coinex.trade.play.png
bugbounty: 
verdict: custodial
date: 2020-04-15
signer: 
reviewArchive:


providerTwitter: coinexcom
providerLinkedIn: 
providerFacebook: TheCoinEx
providerReddit: Coinex

redirect_from:
  - /com.coinex.trade.play/
  - /posts/com.coinex.trade.play/
---


The description on Google Play starts not so promising:

> Meet the top cryptocurrency trading app！

Trading apps are usually custodial. Unfortunately there is no easily accessible
information on their website about the app neither. For now we assume it is
custodial and thus **not verifiable**.

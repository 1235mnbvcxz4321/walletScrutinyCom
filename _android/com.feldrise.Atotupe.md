---
wsId: 
title: "Atotupe Wallet Companion"
altTitle: 
authors:

users: 5
appId: com.feldrise.Atotupe
released: 
updated: 2018-09-04
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: 
repository: 
issue: 
icon: com.feldrise.Atotupe.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "Varies with device"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



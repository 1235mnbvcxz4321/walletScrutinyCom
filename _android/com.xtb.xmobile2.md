---
wsId: 
title: "XTB - Forex, Indices, Commodities, CFDs"
altTitle: 
authors:
- danny
users: 1000000
appId: com.xtb.xmobile2
released: 2014-12-17
updated: 2021-08-11
version: "Varies with device"
stars: 4.5
ratings: 14489
reviews: 4899
size: Varies with device
website: http://www.xtb.com/
repository: 
issue: 
icon: com.xtb.xmobile2.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: xtbuk
providerLinkedIn: xtb-international
providerFacebook: xtb
providerReddit: 

redirect_from:

---


The app description:

>Trade with the Best CFD Broker for 2021* using the award-winning mobile app (Best Mobile App for Investing 2019 & 2020 according to the Rankia awards).

Under their list of trading instruments

>Cryptocurrencies (e.g. Bitcoin, Ethereum, Ripple)


XTB is a brokerage. It allows you to trade or invest in Bitcoin through  CFDs, meaning you don't actually own the Bitcoin. The verdict here is {% include verdictBadge.html verdict="nosendreceive" type='short' %}

---
wsId: GreenBitcoinWallet
title: "Green: Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.greenaddress.greenbits_android_wallet
released: 2015-01-01
updated: 2021-08-02
version: "3.6.4"
stars: 3.8
ratings: 818
reviews: 453
size: 76M
website: https://blockstream.com/green
repository: https://github.com/Blockstream/green_android/
issue: https://github.com/Blockstream/green_android/issues/124
icon: com.greenaddress.greenbits_android_wallet.png
bugbounty: 
verdict: reproducible
date: 2021-08-03
signer: 32f9cc00b13fbeace51e2fb51df482044e42ad34a9bd912f179fedb16a42970e
reviewArchive:
- date: 2021-07-13
  version: "3.6.3"
  appHash: 6779507d1ad1da738312c43fbe6380f6d3e8947d66cd5d89de0fe62fc242217b
  gitRevision: 6af0e73625f44d1f6cd9230b1e2b6eff28d71719
  verdict: reproducible
- date: 2021-06-04
  version: "3.6.0"
  appHash: e93bcf3bcad8b84568a3101c4a87b9c9bb684c7c544fc6b05f204d9fa5fbb57d
  gitRevision: b754fe651fcfdf446b444bf92cd92864316a7b57
  verdict: reproducible
- date: 2021-05-15
  version: "3.5.9"
  appHash: 76cc3df154ff6d47b5366a328515cf14c1d550ca7d71063d851c1949324ef4fa
  gitRevision: f19096afe2eb9df9f5be796428c76a142d2bb2eb
  verdict: nonverifiable
- date: 2021-05-03
  version: "3.5.9"
  appHash: f62c0b4b4882ad15a561fbabcccc34739d66470e752a21a4eab4037736593476
  gitRevision: 13d8e0095e0944d8d255811487d819fafc74c5e1
  verdict: nonverifiable
- date: 2021-05-03
  version: "3.5.8"
  appHash: 950446b62e9e1a536a2341155949750b9856a24768374aac3ce74f2e91394356
  gitRevision: a393a14039f9ee960578b19999c30df46191dd01
  verdict: reproducible
- date: 2021-04-01
  version: "3.5.4"
  appHash: 4ed9729881676b84d7ed65b0f0bd583c11c465186e896e96888c5d323e8c5002
  gitRevision: 512303fa6c495727005df2cb8e1c853128ee03ca
  verdict: reproducible
- date: 2021-02-05
  version: "3.4.9"
  appHash: fd146d68348e332a6a6e2f548c473599ba684cbb6107328a3871de09259f00e5
  gitRevision: 58e8db9ca9f5b5ec1a1881ad5a74fb402bcf3438
  verdict: reproducible
- date: 2021-01-18
  version: "3.4.8"
  appHash: ef539fe60af20a538eb4bdaad37a8cde12cb873cca97800749a71a5add0e9ff7
  gitRevision: 8eb6570934dad87e4b32ae8c8d7e6f06ed8aae43
  verdict: reproducible
- date: 2021-01-03
  version: "3.4.5"
  appHash: efa5e3e56b1081bb66ca99d3fea7d5b1375a8f30696acf0938a324ba12c5458c
  gitRevision: 6f2db0822de5b16cad4f1a7459068a27a50c4896
  verdict: reproducible
- date: 2020-12-15
  version: "3.4.4"
  appHash: d54f84856c25c302978ed5c23ad01c3c0c89930f8f9cd2098558563d9f8b1a3e
  gitRevision: 9817359e09ab4cc05136dc7dfbb40d950750ec4f
  verdict: reproducible
- date: 2020-11-08
  version: "3.4.2"
  appHash: e631aef67a2d50cced4f3a2a850e6f32950e0a91e12678678441defa3da71681
  gitRevision: e855e82e36403f60cfebfd66a8126f9c7dc1cfd4
  verdict: reproducible
- date: 2020-10-17
  version: "3.4.1"
  appHash: 991b1d5672faed19ee8e96a66f7b6812e23971eaf28187424c9af41c4ff16d82
  gitRevision: 84ced1caca6883b917853741705a4b70e7c40ef9
  verdict: reproducible
- date: 2020-10-07
  version: "3.4.0"
  appHash: fb7d9611ad878ef4116b525a50255f3b16725ec673a5c717f14c5d021b242188
  gitRevision: 84ced1caca6883b917853741705a4b70e7c40ef9
  verdict: reproducible
- date: 2020-08-31
  version: "3.3.9"
  appHash: 6a4a4bb05c0c087c4b85486f01982a8bf1bde91a70c587e22929e9faed3eb6ed
  gitRevision: ea0cf3403d57a0e33f7d7627d9854b737fc0d62e
  verdict: nonverifiable
- date: 2020-07-11
  version: "3.3.8"
  appHash: 3a0a02ea8ccd791ab3ec24bac4d45249164f5c53366538b5befcbd4df3f6edb3
  gitRevision: 09ea9943f4ad41f83d28027e3275105483849996
  verdict: reproducible
- date: 2020-05-06
  version: "3.3.7"
  appHash: 847a67a5cfa498cf2e137b0a4306202322a35d4e3fba6bb90a269709e26e11ab
  gitRevision: 4392a6481b289b2cb82d790db15fa8feadf40b1d
  verdict: reproducible
- date: 2020-04-26
  version: "3.3.6"
  appHash: b91e3c6e35aa9223c7d1f62498b162fde226db38049016a354f87578fde371ab
  gitRevision: 9cc435e13b5b513f7ecdaa1baee739b2683d2ba5
  verdict: reproducible
- date: 2020-03-14
  version: "3.3.5"
  appHash: e30092950197aa2801b0f958a90496cf182f76e790f2d7e82e08dbe01b7c32c8
  gitRevision: a6b2771dbc314160ba304573fd0a6cc5d6d1ccb9
  verdict: reproducible
- date: 2020-02-17
  version: "3.3.4"
  appHash: f88686a2e41718b82ba8d2f5ff7eb8d0ada044d29711e75f0128104bbee40baf
  gitRevision: bb658ba9291af3d814ba8a00c4726c9584e379d1
  verdict: reproducible
- date: 2020-01-18
  version: "3.3.2"
  appHash: 7e48e2ff0e8d484f4000b7d96bbdc6b0939a76d8ca80355b5ebedbf68511f77c
  gitRevision: d47e69dd99ac700665328b92b3026a2cf6e36960
  verdict: reproducible
- date: 2020-01-09
  version: "3.3.0"
  appHash: b2e3f2d437bba5d97f3b331aac20616d3312e34d25023c38c42483974828cdec
  gitRevision: 0d558ec280ca9606901f6557622af98e0cbdc97b
  verdict: reproducible
- date: 2019-11-23
  version: "3.2.7"
  appHash: 8b2e67fc333eeef5b10ce6f9f5fc3e4ca104c1eca9c539b73805276e09d838db
  gitRevision: 3d972d9773b0fd2fb1602d31117a50be01d48610
  verdict: reproducible

providerTwitter: Blockstream
providerLinkedIn: blockstream
providerFacebook: Blockstream
providerReddit: 

redirect_from:
  - /greenwallet/
  - /com.greenaddress.greenbits_android_wallet/
  - /posts/2019/11/greenwallet/
  - /posts/com.greenaddress.greenbits_android_wallet/
---


With
[this script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh)
we get:

```
$ ./test.sh /path/to/Green\ 3.6.4\ \(com.greenaddress.greenbits_android_wallet\).apk 95987f9de6b69899c2400af4508befdf9f483b7e
...
Results:
appId:          com.greenaddress.greenbits_android_wallet
signer:         32f9cc00b13fbeace51e2fb51df482044e42ad34a9bd912f179fedb16a42970e
apkVersionName: 3.6.4
apkVersionCode: 22000364
verdict:        reproducible
appHash:        9a796e5b8986c727e0cd112899c40cdd832c94805aa9a547a7daf95ec5ec9dc5
commit:         95987f9de6b69899c2400af4508befdf9f483b7e

Diff:
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000364/META-INF: GREENADD.RSA
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000364/META-INF: GREENADD.SF
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000364/META-INF: MANIFEST.MF

Revision, tag (and its signature):


```

which is what we want to see to give it the verdict **reproducible**.

This revision was initially not reproducible as the provider
[didn't tag the release](https://github.com/Blockstream/green_android/issues/124),
and there isn't a signature as usual on the commit neither.

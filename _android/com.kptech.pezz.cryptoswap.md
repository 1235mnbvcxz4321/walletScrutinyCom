---
wsId: 
title: "Crypto Swap Crypto Exchange"
altTitle: 
authors:

users: 500
appId: com.kptech.pezz.cryptoswap
released: 2019-03-25
updated: 2019-05-31
version: "1.8"
stars: 0.0
ratings: 
reviews: 
size: 2.9M
website: 
repository: 
issue: 
icon: com.kptech.pezz.cryptoswap.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "UZNEX - UZbekistan Cryptocurrency Exchange"
altTitle: 
authors:

users: 1000
appId: com.uznex.app
released: 2020-04-28
updated: 2020-04-28
version: "1.0.0"
stars: 2.6
ratings: 39
reviews: 23
size: 11M
website: 
repository: 
issue: 
icon: com.uznex.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



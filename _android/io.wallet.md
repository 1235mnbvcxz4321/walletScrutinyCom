---
wsId: 
title: "wallet.io—Multi-Chain (BTC, ETH, EOS, Cosmos ...)"
altTitle: 
authors:

users: 10000
appId: io.wallet
released: 2019-08-24
updated: 2021-08-04
version: "1.13.8"
stars: 3.7
ratings: 67
reviews: 30
size: 25M
website: 
repository: 
issue: 
icon: io.wallet.png
bugbounty: 
verdict: wip
date: 2021-06-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



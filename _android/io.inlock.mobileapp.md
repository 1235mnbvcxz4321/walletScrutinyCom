---
wsId: 
title: "INLOCK: Crypto Savings Account"
altTitle: 
authors:

users: 1000
appId: io.inlock.mobileapp
released: 2020-08-04
updated: 2021-08-04
version: "1.4.2"
stars: 4.8
ratings: 59
reviews: 28
size: 29M
website: 
repository: 
issue: 
icon: io.inlock.mobileapp.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



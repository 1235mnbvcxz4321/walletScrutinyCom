---
wsId: bitpaytrading
title: "Bitcoin & Crypto Exchange - BitBay"
altTitle: 
authors:
- leo
users: 100000
appId: net.bitbay.bitcoin
released: 2018-11-19
updated: 2021-08-05
version: "1.1.25"
stars: 2.7
ratings: 951
reviews: 515
size: 16M
website: https://bitbay.net
repository: 
issue: 
icon: net.bitbay.bitcoin.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: BitBay
providerLinkedIn: bitbay
providerFacebook: BitBay
providerReddit: BitBayExchange

redirect_from:

---


This app's description loses no word on who holds the keys to your coins. Their
website is mainly about the exchange and not about the mobile appp but there is
[a site about that](https://bitbay.net/en/mobile), too. There they only talk
about exchange features, too and lose no word about who holds the keys which
probably means this app is a custodial offering and therefore **not verifiable**.

---
wsId: 
title: "LiqAPI"
altTitle: 
authors:

users: 100
appId: net.benoitbasset.liqapi
released: 2018-10-24
updated: 2019-01-25
version: "1.1.1"
stars: 4.8
ratings: 6
reviews: 3
size: 7.7M
website: 
repository: 
issue: 
icon: net.benoitbasset.liqapi.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "ECOMI Secure Wallet"
altTitle: 
authors:

users: 10000
appId: com.ecomi
released: 2018-08-15
updated: 2020-03-05
version: "2.9.2"
stars: 3.9
ratings: 111
reviews: 52
size: 33M
website: 
repository: 
issue: 
icon: com.ecomi.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "2.9.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



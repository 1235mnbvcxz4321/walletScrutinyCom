---
wsId: 
title: "Coin4sales Wallet - Buy Bitcoin, Send, Spend, More"
altTitle: 
authors:

users: 50
appId: com.coin4sales.coin4sales
released: 2020-02-11
updated: 2020-03-06
version: "1.2.4"
stars: 0.0
ratings: 
reviews: 
size: 7.5M
website: 
repository: 
issue: 
icon: com.coin4sales.coin4sales.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.2.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



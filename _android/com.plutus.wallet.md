---
wsId: goabra
title: "Abra Bitcoin Wallet Buy Trade Earn Interest Borrow"
altTitle: 
authors:
- leo
users: 1000000
appId: com.plutus.wallet
released: 2015-03-04
updated: 2021-07-30
version: "Varies with device"
stars: 4.4
ratings: 24315
reviews: 7755
size: Varies with device
website: https://www.abra.com
repository: 
issue: 
icon: com.plutus.wallet.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: AbraGlobal
providerLinkedIn: abra
providerFacebook: GoAbraGlobal
providerReddit: 

redirect_from:
  - /com.plutus.wallet/
  - /posts/com.plutus.wallet/
---


The Google Play description certainly sounds like a custodial wallet:

> Abra is the world’s first global investment app that enables you to invest in
hundreds of cryptocurrencies* like Bitcoin, Ethereum, XRP, Litecoin, Stellar,
Monero, and many more all in one app.

As we can't find a word on security or their source code or about the user being
in control we conclude for now this is a custodial app which gives it our
verdict: **not verifiable**.

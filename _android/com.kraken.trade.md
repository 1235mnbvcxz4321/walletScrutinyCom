---
wsId: krakent
title: "KrakenPro: Advanced Bitcoin & Crypto Trading"
altTitle: 
authors:
- leo
users: 1000000
appId: com.kraken.trade
released: 2019-10-24
updated: 2021-07-30
version: "2.0.2-11262"
stars: 4.5
ratings: 20639
reviews: 6970
size: 58M
website: https://www.kraken.com
repository: 
issue: 
icon: com.kraken.trade.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: krakenfx
providerLinkedIn: krakenfx
providerFacebook: KrakenFX
providerReddit: 

redirect_from:
  - /com.kraken.trade/
  - /posts/com.kraken.trade/
---


On their website we read:

> 95% of all deposits are kept in offline, air-gapped, geographically
  distributed cold storage. We keep full reserves so that you can always
  withdraw immediately on demand.

This app is an interface to a custodial exchange and therefore **not
verifiable**.

---
wsId: 
title: "IndieSquare Wallet"
altTitle: 
authors:

users: 10000
appId: inc.lireneosoft.counterparty
released: 2015-04-28
updated: 2019-06-14
version: "2.5.4"
stars: 3.0
ratings: 240
reviews: 138
size: 22M
website: 
repository: 
issue: 
icon: inc.lireneosoft.counterparty.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "2.5.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



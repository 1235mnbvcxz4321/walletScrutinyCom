---
wsId: buyucoin
title: "BuyUcoin : Crypto Wallet to Buy/Sell Bitcoin India"
altTitle: 
authors:
- leo
users: 100000
appId: com.buyucoinApp.buyucoin
released: 2019-02-15
updated: 2021-08-13
version: "3.19"
stars: 3.8
ratings: 2677
reviews: 1872
size: 37M
website: https://www.buyucoin.com
repository: 
issue: 
icon: com.buyucoinApp.buyucoin.png
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: buyucoin
providerLinkedIn: buyucoin
providerFacebook: BuyUcoin
providerReddit: BuyUcoin

redirect_from:
  - /com.buyucoinApp.buyucoin/
  - /posts/com.buyucoinApp.buyucoin/
---


On the App Store, 4 of 6 ratings are 1-star. On the Play Store the average
rating is 3.5 stars with many complaints in the reviews. Caution is
advised!

This app appears to be the broken interface for a broken exchange, judging by
the vast majority of user comments. It is certainly **not verifiable**.

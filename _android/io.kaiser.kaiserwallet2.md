---
wsId: 
title: "KaiserWallet2 - Cold wallet, Hardware wallet"
altTitle: 
authors:

users: 1000
appId: io.kaiser.kaiserwallet2
released: 2018-10-03
updated: 2020-02-28
version: "2.9.10"
stars: 4.3
ratings: 41
reviews: 22
size: 2.8M
website: 
repository: 
issue: 
icon: io.kaiser.kaiserwallet2.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "2.9.10"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



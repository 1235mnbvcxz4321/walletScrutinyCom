---
wsId: 
title: "BitFin - Buy And Sell Digital Currency"
altTitle: 
authors:

users: 100
appId: com.bitfin.BitFin
released: 2020-02-05
updated: 2020-02-19
version: "1.1"
stars: 0.0
ratings: 
reviews: 
size: 12M
website: 
repository: 
issue: 
icon: com.bitfin.BitFin.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



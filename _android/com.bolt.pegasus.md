---
wsId: 
title: "BOLT X"
altTitle: 
authors:
- kiwilamb
users: 5000
appId: com.bolt.pegasus
released: 2019-08-26
updated: 2021-07-14
version: "1.19.6"
stars: 4.6
ratings: 356
reviews: 292
size: 39M
website: https://bolt.global/
repository: 
issue: 
icon: com.bolt.pegasus.png
bugbounty: 
verdict: custodial
date: 2021-04-15
signer: 
reviewArchive:


providerTwitter: bolt_global
providerLinkedIn: bolt-global
providerFacebook: Global.Bolt
providerReddit: 

redirect_from:

---


Seems like a secondary project called "Pegasus" of Bolt Global where you can earn BOLT tokens into the Bolt-X wallet from using the Bolt+ interactive media companion app.

*(Besides that, we couldn't find any source code or even a claim of it being non-custodial.)*

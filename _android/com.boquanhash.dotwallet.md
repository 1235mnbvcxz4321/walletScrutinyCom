---
wsId: 
title: "DotWallet - Manage Your Crypto and Dapp Assets"
altTitle: 
authors:

users: 5000
appId: com.boquanhash.dotwallet
released: 2020-08-26
updated: 2021-08-02
version: "2.6.2"
stars: 4.2
ratings: 26
reviews: 13
size: 47M
website: 
repository: 
issue: 
icon: com.boquanhash.dotwallet.png
bugbounty: 
verdict: wip
date: 2021-03-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



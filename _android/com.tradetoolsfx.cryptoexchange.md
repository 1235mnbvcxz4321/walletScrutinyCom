---
wsId: 
title: "Crypto Exchange"
altTitle: 
authors:

users: 100
appId: com.tradetoolsfx.cryptoexchange
released: 2018-08-10
updated: 2019-07-31
version: "1.0.8"
stars: 0.0
ratings: 
reviews: 
size: 16M
website: 
repository: 
issue: 
icon: com.tradetoolsfx.cryptoexchange.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



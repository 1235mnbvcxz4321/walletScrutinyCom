---
wsId: 
title: "Simple Bitcoin Widget"
altTitle: 
authors:

users: 100000
appId: com.brentpanther.bitcoinwidget
released: 2013-03-11
updated: 2021-08-01
version: "7.4.6"
stars: 4.1
ratings: 1874
reviews: 743
size: 3.5M
website: 
repository: 
issue: 
icon: com.brentpanther.bitcoinwidget.png
bugbounty: 
verdict: nowallet
date: 2021-03-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Note: This is only a widget. You must add the widget to your launcher, it will
  not appear in your apps list.

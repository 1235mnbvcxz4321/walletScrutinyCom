---
wsId: iwallet
title: "iWallet - blockchain wallet for Bitcoin, Ethereum"
altTitle: 
authors:
- leo
users: 5000
appId: tech.insense.sensewalet
released: 2018-09-27
updated: 2019-06-21
version: "0.0068beta"
stars: 3.9
ratings: 23
reviews: 14
size: 14M
website: http://InSense.tech
repository: 
issue: 
icon: tech.insense.sensewalet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2019-12-30
  version: "0.0068beta"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /tech.insense.sensewalet/
  - /posts/tech.insense.sensewalet/
---


In their description we read:

> *You, as owner of wallet, fully control the private key:*
  * "iWallet"does not store info about users, data about private keys, or funds
    on servers.
  * Private keys are stored on your device.

so they are non-custodial.

The following is their website in its entirety:

![InSense this website is under construction](/images/insenseunderconstruction.png)

So, trusting their claim from the Play Store, not finding any further data to
verify it, we remain with the verdict: **not verifiable**.

---
wsId: 
title: "monerujo: Monero Wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.m2049r.xmrwallet
released: 2017-09-29
updated: 2021-05-21
version: "2.0.8 'Puginarug'"
stars: 3.7
ratings: 739
reviews: 442
size: Varies with device
website: https://monerujo.io
repository: 
issue: 
icon: com.m2049r.xmrwallet.png
bugbounty: 
verdict: nobtc
date: 2021-02-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app does not feature BTC wallet functionality.
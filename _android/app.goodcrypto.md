---
wsId: 
title: "Good Crypto: one trading app - 30 crypto exchanges"
altTitle: 
authors:
- leo
users: 100000
appId: app.goodcrypto
released: 2019-05-20
updated: 2021-07-19
version: "1.7.5"
stars: 4.4
ratings: 717
reviews: 300
size: 20M
website: https://goodcrypto.app
repository: 
issue: 
icon: app.goodcrypto.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-10
signer: 
reviewArchive:


providerTwitter: GoodCryptoApp
providerLinkedIn: goodcrypto
providerFacebook: GoodCryptoApp
providerReddit: GoodCrypto

redirect_from:
  - /app.goodcrypto/
---


This app allows you to connect to accounts on trading platforms and does not
work as a wallet as such. We assume that you cannot receive Bitcoins to and send
them from this app.

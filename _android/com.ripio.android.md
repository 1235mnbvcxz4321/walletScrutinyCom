---
wsId: 
title: "Ripio Bitcoin Wallet: the new digital economy"
altTitle: 
authors:
- leo
users: 1000000
appId: com.ripio.android
released: 2015-06-01
updated: 2021-07-23
version: "4.16.7"
stars: 3.9
ratings: 21975
reviews: 10423
size: 88M
website: https://www.ripio.com/ar/wallet
repository: 
issue: 
icon: com.ripio.android.png
bugbounty: 
verdict: custodial
date: 2020-03-28
signer: 
reviewArchive:


providerTwitter: ripioapp
providerLinkedIn: ripio
providerFacebook: RipioApp
providerReddit: 

redirect_from:
  - /ripio/
  - /com.ripio.android/
  - /posts/2019/11/ripio/
  - /posts/com.ripio.android/
---


Ripio Bitcoin Wallet: the new digital economy
does not claim to be non-custodial and looks much like an interface for an
exchange. Neither can we find any source code.

Therefore: This wallet is **not verifiable**.

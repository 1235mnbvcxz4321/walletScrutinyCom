---
wsId: 
title: "BitGlobal (formerly Bithumb Global)"
altTitle: 
authors:

users: 100000
appId: global.bithumb.android
released: 2019-07-05
updated: 2021-08-05
version: "2.6.24"
stars: 2.6
ratings: 2798
reviews: 2018
size: 22M
website: 
repository: 
issue: 
icon: global.bithumb.android.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



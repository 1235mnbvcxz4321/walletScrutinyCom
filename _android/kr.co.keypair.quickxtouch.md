---
wsId: krcokeypair
title: "QuickX Touch"
altTitle: 
authors:

users: 500
appId: kr.co.keypair.quickxtouch
released: 2018-10-09
updated: 2019-07-17
version: "1.0.0.57"
stars: 3.5
ratings: 15
reviews: 10
size: 10M
website: https://www.quickx.io
repository: 
issue: 
icon: kr.co.keypair.quickxtouch.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-07
  version: "1.0.0.57"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: quickxprotocol
providerLinkedIn: quickx
providerFacebook: quickxprotocol
providerReddit: QuickX

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.

---
wsId: 
title: "Global btc trader"
altTitle: 
authors:

users: 1000
appId: com.tech.crypto.globalbtc
released: 2017-12-26
updated: 2018-07-03
version: "1.9"
stars: 4.0
ratings: 20
reviews: 8
size: 7.1M
website: 
repository: 
issue: 
icon: com.tech.crypto.globalbtc.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.9"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "elegro Retail PoS App"
altTitle: 
authors:

users: 100
appId: dashboard.elegro.io
released: 2018-08-03
updated: 2020-06-16
version: "3.0.3"
stars: 4.6
ratings: 11
reviews: 4
size: 605k
website: 
repository: 
issue: 
icon: dashboard.elegro.io.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "3.0.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: Coinpal
title: "Coinpal"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: me.coinpal.app
released: 2017-12-22
updated: 2018-06-27
version: "4.1.4"
stars: 4.4
ratings: 111
reviews: 51
size: 20M
website: https://coinpal.me/
repository: 
issue: 
icon: me.coinpal.app.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-06-04
  version: "4.1.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: coinpalapp
providerLinkedIn: coinpal
providerFacebook: coinpal.me
providerReddit: 

redirect_from:

---


The providers [Play store description](https://play.google.com/store/apps/details?id=me.coinpal.app) states the customer is in control of the private keys.

> Device-based security: all private keys are stored locally, not in the cloud

This is a non-cusdodial wallet however

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

---
wsId: 
title: "AIRWALLET - Secure Cryptocurrency Multi-Wallet"
altTitle: 
authors:

users: 1000
appId: com.airwalletofficial.air_wallet
released: 2019-10-19
updated: 2019-10-29
version: "1.9.2"
stars: 4.2
ratings: 29
reviews: 25
size: 16M
website: 
repository: 
issue: 
icon: com.airwalletofficial.air_wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.9.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



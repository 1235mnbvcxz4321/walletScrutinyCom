---
wsId: 
title: "TRASTRA: Crypto Wallet & VISA Card"
altTitle: 
authors:

users: 50000
appId: com.trastra.mobile
released: 2019-01-19
updated: 2021-07-22
version: "2.2.4"
stars: 4.4
ratings: 414
reviews: 258
size: 52M
website: 
repository: 
issue: 
icon: com.trastra.mobile.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



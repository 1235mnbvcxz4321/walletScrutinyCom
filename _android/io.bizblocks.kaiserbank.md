---
wsId: 
title: "Kaiser PayBanC 2.0–Cold Wallet, Hardware wallet"
altTitle: 
authors:

users: 1000
appId: io.bizblocks.kaiserbank
released: 2019-01-04
updated: 2020-11-22
version: "0.30.16"
stars: 4.7
ratings: 20
reviews: 13
size: 7.5M
website: 
repository: 
issue: 
icon: io.bizblocks.kaiserbank.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



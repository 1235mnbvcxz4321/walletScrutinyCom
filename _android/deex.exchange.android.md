---
wsId: 
title: "Deex Exchange"
altTitle: 
authors:

users: 1000
appId: deex.exchange.android
released: 2019-11-29
updated: 2020-04-21
version: "0.3.9"
stars: 4.3
ratings: 244
reviews: 196
size: 5.7M
website: 
repository: 
issue: 
icon: deex.exchange.android.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "0.3.9"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "BCD Pay"
altTitle: 
authors:

users: 5000
appId: com.chimpion.bcdpay
released: 2019-05-21
updated: 2020-03-16
version: "7.1.63"
stars: 4.7
ratings: 178
reviews: 151
size: 28M
website: 
repository: 
issue: 
icon: com.chimpion.bcdpay.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "7.1.63"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



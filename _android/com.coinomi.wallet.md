---
wsId: coinomi
title: "Coinomi Wallet :: Bitcoin Ethereum Altcoins Tokens"
altTitle: 
authors:
- leo
users: 1000000
appId: com.coinomi.wallet
released: 2014-01-31
updated: 2021-08-14
version: "Varies with device"
stars: 4.4
ratings: 37290
reviews: 21196
size: Varies with device
website: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.png
bugbounty: 
verdict: nosource
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: CoinomiWallet
providerLinkedIn: coinomi
providerFacebook: coinomi
providerReddit: COINOMI

redirect_from:
  - /coinomi/
  - /com.coinomi.wallet/
  - /posts/2019/11/coinomi/
  - /posts/com.coinomi.wallet/
---


This wallet claims to be non-custodial but
[went closed-source](https://github.com/bitcoin-dot-org/bitcoin.org/issues/1622)
in early 2016
[according to them](https://twitter.com/CoinomiWallet/status/945048682927394817)

> **coinomi**<br>
  @CoinomiWallet<br>
  No, we moved to closed source to protect the users from getting ripped off by
  scammers. Our website is open source and there are at least 10 clones at the
  time of the writing stealing users' funds (by stealing their private keys).<br>
  6:48 PM · Dec 24, 2017

Our verdict: This app is **not verifiable**.

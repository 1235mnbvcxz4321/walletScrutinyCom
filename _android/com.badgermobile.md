---
wsId: 
title: "Badger Wallet"
altTitle: 
authors:

users: 10000
appId: com.badgermobile
released: 2019-06-10
updated: 2021-08-02
version: "1.13.5"
stars: 3.7
ratings: 152
reviews: 79
size: 33M
website: https://badger.bitcoin.com
repository: 
issue: 
icon: com.badgermobile.png
bugbounty: 
verdict: nobtc
date: 2019-12-28
signer: 
reviewArchive:


providerTwitter: badgerwallet
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: 

redirect_from:
  - /com.badgermobile/
  - /posts/com.badgermobile/
---



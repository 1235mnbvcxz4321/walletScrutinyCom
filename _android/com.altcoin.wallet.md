---
wsId: 
title: "Altcoin Wallet. Your coins protected"
altTitle: 
authors:

users: 10000
appId: com.altcoin.wallet
released: 2018-03-19
updated: 2020-01-21
version: "v1.6"
stars: 3.0
ratings: 101
reviews: 83
size: 3.7M
website: 
repository: 
issue: 
icon: com.altcoin.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "v1.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



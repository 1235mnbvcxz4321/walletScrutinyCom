---
wsId: 
title: "fox.exchange Cryptocurrency Express Exchange"
altTitle: 
authors:

users: 500
appId: exchange.fox.android
released: 2019-06-11
updated: 2020-04-20
version: "1.5.1"
stars: 3.3
ratings: 12
reviews: 8
size: 5.2M
website: 
repository: 
issue: 
icon: exchange.fox.android.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.5.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



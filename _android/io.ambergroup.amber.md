---
wsId: 
title: "Amber App - Swap & Earn Crypto"
altTitle: 
authors:

users: 100000
appId: io.ambergroup.amber
released: 2020-09-21
updated: 2021-08-02
version: "v1.6.0"
stars: 3.7
ratings: 348
reviews: 179
size: 80M
website: 
repository: 
issue: 
icon: io.ambergroup.amber.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: kucoin
title: "KuCoin: Bitcoin Exchange & Crypto Wallet"
altTitle: 
authors:
- leo
users: 1000000
appId: com.kubi.kucoin
released: 2018-05-03
updated: 2021-08-02
version: "3.38.1"
stars: 3.9
ratings: 7736
reviews: 4227
size: 46M
website: 
repository: 
issue: 
icon: com.kubi.kucoin.png
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: KuCoinCom
providerLinkedIn: kucoin
providerFacebook: KuCoinOfficial
providerReddit: kucoin

redirect_from:

---


> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.

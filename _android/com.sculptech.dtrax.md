---
wsId: 
title: "DTrax - Crypto wallet, Payment, Crypto Exchange"
altTitle: 
authors:

users: 10000
appId: com.sculptech.dtrax
released: 2019-01-24
updated: 2020-05-24
version: "1.2.2"
stars: 3.2
ratings: 365
reviews: 278
size: 12M
website: 
repository: 
issue: 
icon: com.sculptech.dtrax.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.2.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "PlusBit Wallet"
altTitle: 
authors:

users: 500
appId: com.plusbit
released: 2020-04-07
updated: 2020-07-12
version: "1.0"
stars: 4.9
ratings: 95
reviews: 89
size: 24M
website: 
repository: 
issue: 
icon: com.plusbit.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Tap - Buy & Sell Bitcoin Securely"
altTitle: 
authors:

users: 50000
appId: com.tapngo.tap
released: 2019-12-19
updated: 2021-08-05
version: "2.2.0"
stars: 4.4
ratings: 1144
reviews: 502
size: 127M
website: 
repository: 
issue: 
icon: com.tapngo.tap.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "CryptoTrader™ - Built by traders, for traders"
altTitle: 
authors:

users: 10000
appId: com.tradingducoin.appli
released: 2019-03-10
updated: 2021-08-09
version: "5.4.5"
stars: 3.8
ratings: 175
reviews: 87
size: 32M
website: 
repository: 
issue: 
icon: com.tradingducoin.appli.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



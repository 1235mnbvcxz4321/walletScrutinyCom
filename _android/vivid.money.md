---
wsId: 
title: "Vivid: Investments & Banking & Crypto"
altTitle: 
authors:
- danny
users: 500000
appId: vivid.money
released: 2020-10-09
updated: 2021-08-18
version: "1.54.2"
stars: 4.3
ratings: 14313
reviews: 3835
size: 135M
website: https://vivid.money/
repository: 
issue: 
icon: vivid.money.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: vivid_en
providerLinkedIn: vividmoney
providerFacebook: vivid.money.en
providerReddit: 

redirect_from:

---


> Vivid is a brand new mobile banking and investing app. Online banking and commission-free investing belong together and should be easy.

From their website's 'help' section in response to the question: "Do I own real cryptocurrencies?"

> No, you don’t. With Vivid Invest we offer trading via Fractional Сoins. Fractional Coins are investment products; one-to-one bilateral agreements (so-called OTC derivatives) with specific cryptocurrencies as underlying. The price of your Fractional Coins reflects a real coin’s price movements 1:1. There is no leverage.

We conclude that this app cannot send our receive actual Bitcoins.


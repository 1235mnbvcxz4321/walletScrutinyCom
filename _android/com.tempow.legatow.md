---
wsId: 
title: "Legatow"
altTitle: 
authors:

users: 1000
appId: com.tempow.legatow
released: 2018-07-04
updated: 2019-01-30
version: "1.0.3"
stars: 5.0
ratings: 346
reviews: 210
size: 9.5M
website: 
repository: 
issue: 
icon: com.tempow.legatow.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



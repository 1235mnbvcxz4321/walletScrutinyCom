---
wsId: 
title: "bit.com"
altTitle: 
authors:

users: 1000
appId: com.matrixport.bit
released: 2021-03-15
updated: 2021-08-03
version: "0.1.8"
stars: 5.0
ratings: 227
reviews: 216
size: 52M
website: 
repository: 
issue: 
icon: com.matrixport.bit.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



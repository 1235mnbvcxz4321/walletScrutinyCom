---
wsId: 
title: "Coinsafe - Never Lose Your Crypto. Mainnet Beta"
altTitle: 
authors:

users: 100
appId: app.getcoinsafe.android
released: 2019-02-15
updated: 2019-05-16
version: "0.1.6"
stars: 4.9
ratings: 29
reviews: 4
size: 14M
website: 
repository: 
issue: 
icon: app.getcoinsafe.android.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "0.1.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



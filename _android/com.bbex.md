---
wsId: 
title: "Bluebelt Instant Crypto Bank"
altTitle: 
authors:

users: 500
appId: com.bbex
released: 2019-07-12
updated: 2019-11-06
version: "1.0.5"
stars: 0.0
ratings: 
reviews: 
size: 3.1M
website: 
repository: 
issue: 
icon: com.bbex.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



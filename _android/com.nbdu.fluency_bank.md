---
wsId: 
title: "Fluency - The Global Cryptobank of the Future."
altTitle: 
authors:

users: 1000
appId: com.nbdu.fluency_bank
released: 2020-02-27
updated: 2020-04-29
version: "1.0.0"
stars: 4.5
ratings: 408
reviews: 105
size: 14M
website: 
repository: 
issue: 
icon: com.nbdu.fluency_bank.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



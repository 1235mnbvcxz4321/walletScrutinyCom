---
wsId: 
title: "Moon Faucet - BTC, LTC, XDG, BCH"
altTitle: 
authors:

users: 10000
appId: g3shanappz.moonfaucet
released: 2020-06-29
updated: 2020-06-29
version: "9.8"
stars: 3.9
ratings: 609
reviews: 292
size: 8.5M
website: 
repository: 
issue: 
icon: g3shanappz.moonfaucet.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "9.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



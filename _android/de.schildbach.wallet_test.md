---
wsId: 
title: "Testnet Wallet"
altTitle: 
authors:

users: 10000
appId: de.schildbach.wallet_test
released: 2011-07-10
updated: 2021-08-02
version: "8.16"
stars: 4.5
ratings: 359
reviews: 73
size: 2.9M
website: 
repository: 
issue: 
icon: de.schildbach.wallet_test.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



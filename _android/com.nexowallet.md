---
wsId: nexo
title: "Nexo - Crypto Account"
altTitle: 
authors:
- leo
users: 1000000
appId: com.nexowallet
released: 2019-06-28
updated: 2021-08-10
version: "2.0.5"
stars: 4.2
ratings: 13046
reviews: 6039
size: 65M
website: https://nexo.io
repository: 
issue: 
icon: com.nexowallet.png
bugbounty: 
verdict: custodial
date: 2020-11-17
signer: 
reviewArchive:


providerTwitter: NexoFinance
providerLinkedIn: 
providerFacebook: nexofinance
providerReddit: Nexo

redirect_from:
  - /com.nexowallet/
---


In the description on Google Play we read:

> • 100% Secured by Leading Audited Custodian BitGo

which makes it a custodial app. The custodian is claimed to be "BitGo" so as a
user you have to trust BitGo to not lose the coins and Nexo to actually not hold
all or part of the coins. In any case this app is **not verifiable**.

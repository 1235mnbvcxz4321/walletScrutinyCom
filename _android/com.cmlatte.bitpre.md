---
wsId: 
title: "비트프리 - 비트코인, 이더리움, 리플 프리미엄 정보"
altTitle: 
authors:

users: 1000
appId: com.cmlatte.bitpre
released: 2017-06-12
updated: 2018-08-10
version: "1.1.1"
stars: 4.9
ratings: 17
reviews: 9
size: 9.4M
website: 
repository: 
issue: 
icon: com.cmlatte.bitpre.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



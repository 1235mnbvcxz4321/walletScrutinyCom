---
wsId: bitmart
title: "BitMart - Cryptocurrency Exchange"
altTitle: 
authors:
- leo
users: 500000
appId: com.bitmart.bitmarket
released: 2018-05-09
updated: 2021-08-18
version: "2.6.4"
stars: 4.3
ratings: 22693
reviews: 9157
size: 30M
website: https://www.bitmart.com
repository: 
issue: 
icon: com.bitmart.bitmarket.png
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: BitMartExchange
providerLinkedIn: bitmart
providerFacebook: bitmartexchange
providerReddit: BitMartExchange

redirect_from:
  - /com.bitmart.bitmarket/
  - /posts/com.bitmart.bitmarket/
---


On their website we read:

> **Secure**<br>
  Advanced risk control system in the market. Hybrid hot/cold wallet systems and
  multi-signature technologies. 100% secure for trading and digital asset
  management

A "hot" wallet is online, a "cold" wallet is offline. Your phone is certainly
not "cold", so it's them who hold the keys. As a custodial service the app is
**not verifiable**.

---
wsId: krcokeypair
title: "AFIN Touch"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.afintouch
released: 2018-10-04
updated: 2019-12-27
version: "1.0.0.60"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: https://www.afincoin.io
repository: 
issue: 
icon: kr.co.keypair.afintouch.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-07
  version: "1.0.0.60"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: AfinCoin
providerLinkedIn: 
providerFacebook: asianfintech
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.


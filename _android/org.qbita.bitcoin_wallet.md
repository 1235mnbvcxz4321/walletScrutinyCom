---
wsId: 
title: "QBita - Monedero Bitcoin"
altTitle: 
authors:

users: 5000
appId: org.qbita.bitcoin_wallet
released: 2019-08-28
updated: 2019-08-28
version: "1.0"
stars: 4.1
ratings: 73
reviews: 45
size: 1.6M
website: 
repository: 
issue: 
icon: org.qbita.bitcoin_wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-18
signer: 
reviewArchive:
- date: 2021-08-17
  version: "1.0"
  appHash: 
  gitRevision: 1a943e37e3aeeaf622f3300ce898ae29df2c5112
  verdict: stale
- date: 2021-08-01
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



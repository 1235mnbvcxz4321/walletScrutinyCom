---
wsId: 
title: "Bitcoin Testnet Wallet"
altTitle: 
authors:

users: 1000
appId: com.bitcoin.wallet.btc.testnet
released: 2019-07-04
updated: 2019-07-05
version: "1.0"
stars: 4.3
ratings: 23
reviews: 4
size: 8.8M
website: 
repository: 
issue: 
icon: com.bitcoin.wallet.btc.testnet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



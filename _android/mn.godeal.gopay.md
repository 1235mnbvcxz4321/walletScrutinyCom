---
wsId: 
title: "GoPay Bitcoin HD Wallet"
altTitle: 
authors:

users: 1000
appId: mn.godeal.gopay
released: 2018-02-10
updated: 2019-01-20
version: "2.0.0"
stars: 4.6
ratings: 26
reviews: 8
size: 13M
website: 
repository: 
issue: 
icon: mn.godeal.gopay.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



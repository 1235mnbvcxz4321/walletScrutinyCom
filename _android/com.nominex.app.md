---
wsId: 
title: "Nominex: Cryptocurrency trading without commission"
altTitle: 
authors:

users: 5000
appId: com.nominex.app
released: 2021-06-01
updated: 2021-08-17
version: "1.2.0"
stars: 4.3
ratings: 104
reviews: 72
size: 58M
website: 
repository: 
issue: 
icon: com.nominex.app.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: SwapWallet
title: "Swap Wallet - BTC, Bitcoin wallet"
altTitle: 

users: 50000
appId: com.swapwalletltd.swap
released: 
updated: 2020-11-14
version: "Varies with device"
stars: 4.9
ratings: 2282
reviews: 2065
size: Varies with device
website: http://swapwallet.com
repository: 
issue: 
icon: com.swapwalletltd.swap.png
bugbounty: 
verdict: defunct
date: 2020-12-28
signer: 
reviewArchive:
- date: 2020-12-01
  version: 
  appHash: 
  gitRevision: eceaf4f532a049d544a5f7ce8eda0f29b30e6fcf
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.swapwalletltd.swap/
---


**Update:** We can't find the app on Google Play anymore and assume it stopped
to exist. With 50k downloads there might be many users wondering how to recover
their coins. Please let us know if you know more!

## Old review

We can find no information on security. Also their links to Knowledge Base and
FAQ are broken. Like this we have to assume it's a custodial wallet and thus
**not verifiable**.

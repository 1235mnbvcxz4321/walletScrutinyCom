---
wsId: ChangeNOW
title: "ChangeNOW: Buy Bitcoin Dogecoin & Crypto Exchange"
altTitle: 
authors:
- leo
users: 10000
appId: io.changenow.changenow
released: 2018-09-07
updated: 2021-07-30
version: "1.117"
stars: 4.8
ratings: 1209
reviews: 618
size: 6.1M
website: http://changenow.io
repository: 
issue: 
icon: io.changenow.changenow.png
bugbounty: 
verdict: nosource
date: 2020-11-16
signer: 
reviewArchive:


providerTwitter: ChangeNOW_io
providerLinkedIn: 
providerFacebook: ChangeNOW.io
providerReddit: ChangeNOW_io

redirect_from:
  - /io.changenow.changenow/
---


> We focus on simplicity and safety — the service is registration-free and non-custodial.

> With ChangeNOW, you remain in full control over your digital assets.

That's a claim. Let's see if it is verifiable ...

There is no claim of public source anywhere and
[neither does GitHub know](https://github.com/search?q=%22io.changenow.changenow%22)
this app, so it's at best closed source and thus **not verifiable**.

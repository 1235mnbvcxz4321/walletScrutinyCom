---
wsId: InfinitoWallet
title: "Infinito Wallet - Crypto Wallet & DApp Browser"
altTitle: 
authors:
- leo
users: 100000
appId: io.infinito.wallet
released: 2017-11-15
updated: 2021-07-22
version: "2.35.5"
stars: 3.1
ratings: 2148
reviews: 1029
size: 64M
website: https://www.infinitowallet.io
repository: 
issue: 
icon: io.infinito.wallet.png
bugbounty: 
verdict: nosource
date: 2020-03-30
signer: 
reviewArchive:


providerTwitter: Infinito_Ltd
providerLinkedIn: infinitoservices
providerFacebook: InfinitoWallet
providerReddit: 

redirect_from:
  - /io.infinito.wallet/
  - /posts/io.infinito.wallet/
---


Right on the Playstore description we find:

> Truly decentralized: You control private key, passphrases and passwords

So it is not a custodial app. How about source code?

Searching for its appId on GitHub
[does not yield promising results](https://github.com/search?q=%22io.infinito.wallet%22&type=Code)
neither.

Our verdict: This app is **not verifiable**.

---
wsId: 
title: "Delta Investment Portfolio Tracker"
altTitle: 
authors:

users: 1000000
appId: io.getdelta.android
released: 2017-09-23
updated: 2021-08-18
version: "4.4.0"
stars: 4.3
ratings: 23466
reviews: 8044
size: 92M
website: 
repository: 
issue: 
icon: io.getdelta.android.png
bugbounty: 
verdict: nowallet
date: 2020-12-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.getdelta.android/
---


This appears to be only a portfolio tracker. If it asks for your credentials for
exchanges, it might still get into a position of pulling your funds from there.

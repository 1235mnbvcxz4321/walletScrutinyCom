---
wsId: 
title: "Crypto Blockchain Wallet to Buy & Sell Bitcoin"
altTitle: 
authors:

users: 1000
appId: crypto.stars.wallet
released: 2018-10-11
updated: 2018-10-11
version: "0.42.6"
stars: 4.1
ratings: 9
reviews: 6
size: 6.6M
website: 
repository: 
issue: 
icon: crypto.stars.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "0.42.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



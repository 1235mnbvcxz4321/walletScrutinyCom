---
wsId: 
title: "CREX"
altTitle: 
authors:

users: 100
appId: com.crex.trade
released: 2020-05-20
updated: 2020-05-26
version: "1.0.1"
stars: 0.0
ratings: 
reviews: 
size: 12M
website: 
repository: 
issue: 
icon: com.crex.trade.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



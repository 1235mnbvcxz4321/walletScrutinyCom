---
wsId: 
title: "Blockchain Dashboard"
altTitle: 
authors:
- leo
users: 10000
appId: com.blockchain.explorer
released: 2019-12-30
updated: 2021-06-08
version: "1.2.0.1"
stars: 4.4
ratings: 297
reviews: 80
size: 24M
website: 
repository: 
issue: 
icon: com.blockchain.explorer.png
bugbounty: 
verdict: nowallet
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This only promises to be a block explorer although people claim to somehow have
lost money with it.

---
wsId: 
title: "유니익스체인지, UNIEXCHANGE"
altTitle: 
authors:

users: 1000
appId: com.uni.uniexchange
released: 2018-06-06
updated: 2019-02-14
version: "5.3.5"
stars: 3.9
ratings: 11
reviews: 7
size: 5.7M
website: 
repository: 
issue: 
icon: com.uni.uniexchange.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "5.3.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



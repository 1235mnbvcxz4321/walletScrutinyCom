---
wsId: 
title: "CoolBitX Crypto"
altTitle: 
authors:

users: 10000
appId: com.coolbitx.cwsapp
released: 2017-12-01
updated: 2021-08-11
version: "2.21.0"
stars: 4.6
ratings: 818
reviews: 341
size: 51M
website: http://coolwallet.io
repository: 
issue: 
icon: com.coolbitx.cwsapp.png
bugbounty: 
verdict: nowallet
date: 2020-12-06
signer: 
reviewArchive:


providerTwitter: coolwallet
providerLinkedIn: 
providerFacebook: coolwallet
providerReddit: 

redirect_from:
  - /com.coolbitx.cwsapp/
---


> This app (made for the CoolWallet S -- the second generation card) is the most convenient and secure way to store, receive, and send your cryptocurrency assets.

This app is the companion app of a hardware wallet. If it doesn't manage private
keys itself, it's not a wallet. Let's see if it can be used without the
"CoolWallet S" hardware wallet ...

On Google Play there is no word about private keys.

At first sight, the website only talks about their hardware wallet.

As the app cannot be setup without Bluetooth being activated we assume it can't
be used as a wallet without the hardware wallet. It might still be able to have
hardware-less accounts which would make it a wallet again but as we can't test
that and most likely no significant funds would end up in such an account, we
file this app as a companion app of a hardware wallet, only.

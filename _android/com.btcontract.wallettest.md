---
wsId: 
title: "Simple Bitcoin Wallet TESTNET"
altTitle: 
authors:

users: 100
appId: com.btcontract.wallettest
released: 2021-06-28
updated: 2021-07-30
version: "2.0"
stars: 0.0
ratings: 
reviews: 
size: 18M
website: 
repository: 
issue: 
icon: com.btcontract.wallettest.png
bugbounty: 
verdict: nobtc
date: 2021-08-09
signer: 
reviewArchive:
- date: 2021-08-02
  version: ""
  appHash: 
  gitRevision: e2e703e641028d5466e8e09931b9c89d19790759
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is for testnet only.
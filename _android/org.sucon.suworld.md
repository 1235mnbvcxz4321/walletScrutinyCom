---
wsId: 
title: "SUWORLD-Blockchain, SUCON, CoinMarket, SNS, Wallet"
altTitle: 
authors:

users: 1000
appId: org.sucon.suworld
released: 2019-04-21
updated: 2019-09-16
version: "1.0.0.6"
stars: 4.7
ratings: 54
reviews: 44
size: 4.9M
website: 
repository: 
issue: 
icon: org.sucon.suworld.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.0.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



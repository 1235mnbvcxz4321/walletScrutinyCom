---
title: "TronWallet"
altTitle: 

users: 1000000
appId: com.tronwallet2
released: 
updated: 2020-07-23
version: "3.4.5"
stars: 3.9
ratings: 8703
reviews: 4748
size: Varies with device
website: https://www.tronwallet.me
repository: 
issue: 
icon: com.tronwallet2.png
bugbounty: 
verdict: defunct
date: 2021-01-15
signer: 
reviewArchive:
- date: 2020-11-17
  version: "3.4.5"
  appHash: 
  gitRevision: 0b695b1e5f991a88b8b576bb43c69b8cac285fe8
  verdict: nosource


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.tronwallet2/
---


The wallet is not listed on Google Play anymore and as their website forwards
the visitor to the
{% include walletLink.html wallet='android/cash.klever.blockchain.wallet' %}
since long, one appears to be the successor of the other.

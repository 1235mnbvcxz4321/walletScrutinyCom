---
wsId: 
title: "Coinlend"
altTitle: 
authors:

users: 5000
appId: org.coinlend
released: 2018-07-19
updated: 2020-06-30
version: "0.0.36"
stars: 4.4
ratings: 79
reviews: 43
size: 49M
website: 
repository: 
issue: 
icon: org.coinlend.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "0.0.36"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



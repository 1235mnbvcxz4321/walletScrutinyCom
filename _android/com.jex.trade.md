---
wsId: 
title: "Binance JEX - Bitcoin Futures&Options Exchange"
altTitle: 
authors:

users: 500000
appId: com.jex.trade
released: 2019-04-25
updated: 2021-05-10
version: "2.8.1"
stars: 3.7
ratings: 860
reviews: 436
size: 10M
website: 
repository: 
issue: 
icon: com.jex.trade.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



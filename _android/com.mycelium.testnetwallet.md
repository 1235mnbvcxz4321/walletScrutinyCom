---
wsId: 
title: "Mycelium Testnet Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.mycelium.testnetwallet
released: 2013-10-04
updated: 2021-08-13
version: "3.11.0.4-TESTNET"
stars: 3.7
ratings: 135
reviews: 70
size: 24M
website: 
repository: 
issue: 
icon: com.mycelium.testnetwallet.png
bugbounty: 
verdict: nobtc
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is the testnet version of {% include walletLink.html wallet='android/com.mycelium.wallet' verdict='true' %}
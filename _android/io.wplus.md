---
wsId: 
title: "Wplus Crypto Wallet"
altTitle: 
authors:

users: 1000
appId: io.wplus
released: 2018-11-27
updated: 2019-05-24
version: "0.1.1"
stars: 4.1
ratings: 41
reviews: 23
size: 7.7M
website: 
repository: 
issue: 
icon: io.wplus.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "0.1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



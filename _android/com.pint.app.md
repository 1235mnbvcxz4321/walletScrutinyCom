---
wsId: 
title: "PINT Wallet & P2P Marketplace for Bitcoin Ethereum"
altTitle: 
authors:

users: 50000
appId: com.pint.app
released: 2018-04-25
updated: 2021-03-06
version: "2.0.32"
stars: 4.2
ratings: 218
reviews: 142
size: 23M
website: 
repository: 
issue: 
icon: com.pint.app.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



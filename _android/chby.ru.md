---
wsId: 
title: "chby.ru"
altTitle: 
authors:

users: 100
appId: chby.ru
released: 2018-12-19
updated: 2020-01-13
version: "1.0.5"
stars: 1.4
ratings: 8
reviews: 7
size: 5.7M
website: 
repository: 
issue: 
icon: chby.ru.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: pointpay
title: "PointPay Bank: Cryptocurrency Wallet & Exchange"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.pointpay.bank
released: 2020-07-21
updated: 2021-08-04
version: "v5.4.9"
stars: 4.5
ratings: 3339
reviews: 1718
size: 76M
website: https://wallet.pointpay.io
repository: 
issue: 
icon: com.pointpay.bank.png
bugbounty: 
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: PointPay1
providerLinkedIn: pointpay
providerFacebook: PointPayLtd
providerReddit: PointPay

redirect_from:

---


The PointPay website has very little information about how they manage private keys of the user.
The only basic statement is...

> We use strong military-grade encryption to store private keys

we will have to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.


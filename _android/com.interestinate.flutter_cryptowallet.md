---
wsId: 
title: "Flutter Cryptowallet"
altTitle: 
authors:

users: 100
appId: com.interestinate.flutter_cryptowallet
released: 2020-01-06
updated: 2020-01-06
version: "1.0.0"
stars: 0.0
ratings: 
reviews: 
size: 6.4M
website: 
repository: 
issue: 
icon: com.interestinate.flutter_cryptowallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "ONTO - Decentralized Cross-chain Crypto Wallet"
altTitle: 
authors:

users: 50000
appId: com.github.ontio.onto
released: 2018-06-27
updated: 2021-08-10
version: "3.9.7"
stars: 3.2
ratings: 821
reviews: 539
size: 79M
website: 
repository: 
issue: 
icon: com.github.ontio.onto.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



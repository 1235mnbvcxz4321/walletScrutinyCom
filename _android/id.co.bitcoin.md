---
wsId: indodax
title: "Indodax"
altTitle: 
authors:
- leo
users: 1000000
appId: id.co.bitcoin
released: 2014-12-15
updated: 2021-08-15
version: "4.1.8"
stars: 4.3
ratings: 64371
reviews: 33902
size: 29M
website: https://indodax.com
repository: 
issue: 
icon: id.co.bitcoin.png
bugbounty: 
verdict: custodial
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: indodax
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is the interface to

> Indodax is Indonesia’s largest crypto asset marketplace

and on their website they claim:

> **Security**<br>
  Every transaction is protected with Multi-factor Authentication, combining
  email verification and Google Authenticator SMS to guarantee that your
  transaction is truly signed and validated only by you.

With no further explanation and as this is an exchange, we assume the app is a
custodial offering and thus **not verifiable**.
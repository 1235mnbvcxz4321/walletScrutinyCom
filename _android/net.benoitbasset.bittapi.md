---
wsId: 
title: "BittAPI"
altTitle: 
authors:

users: 1000
appId: net.benoitbasset.bittapi
released: 2017-12-09
updated: 2018-11-13
version: "1.2.1"
stars: 4.7
ratings: 99
reviews: 42
size: 5.0M
website: 
repository: 
issue: 
icon: net.benoitbasset.bittapi.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.2.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



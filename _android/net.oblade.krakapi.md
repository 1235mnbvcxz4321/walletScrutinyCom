---
wsId: 
title: "KrakAPI"
altTitle: 
authors:

users: 5000
appId: net.oblade.krakapi
released: 2019-08-09
updated: 2021-08-08
version: "2.4.3"
stars: 4.4
ratings: 488
reviews: 219
size: 7.5M
website: 
repository: 
issue: 
icon: net.oblade.krakapi.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



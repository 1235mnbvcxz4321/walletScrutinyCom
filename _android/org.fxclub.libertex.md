---
wsId: libertex
title: "Libertex Online Trading app"
altTitle: 
authors:
- danny
users: 10000000
appId: org.fxclub.libertex
released: 2015-05-22
updated: 2021-05-02
version: "2.27.2"
stars: 4.1
ratings: 69809
reviews: 28160
size: 37M
website: libertex
repository: 
issue: 
icon: org.fxclub.libertex.png
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: libertex_latam
providerLinkedIn: 
providerFacebook: libertex.ph
providerReddit: 

redirect_from:

---


> Libertex is a powerful online trading app designed to boost your trading experience.

Libertex Online Trading app advertises itself as a "powerful online trading app" and not as a Bitcoin wallet. It does have a "wallet" that can store Bitcoins but users aren't provided with the private key. With a third party in control of the keys and no source code in sight it's safe to assume this app is **custodial** and as such **not verifiable.** 

**Additional Observations:**
Interestingly enough, there's [an app](https://play.google.com/store/apps/details?id=com.libertex.mobile) and [website](https://libertex.com) with the same name but with the ".com" domain rather than ".org." The Twitter account seems to clarify this by calling itself "Libertex Europe" while Libertex.org's Twitter calls itself "Libertex LATAM" which is a bit confusing. Adding to that, Libertex.com seems to be unable to accept new clients and has a 

> temporary license suspension from CySEC

We've tried contacting the support for some information on this and could not get an immediate response.

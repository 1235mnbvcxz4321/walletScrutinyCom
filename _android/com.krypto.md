---
wsId: 
title: "Krypto - Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.krypto
released: 2020-04-29
updated: 2021-07-10
version: "8.5"
stars: 3.7
ratings: 1451
reviews: 1012
size: 9.2M
website: https://letskrypto.com
repository: 
issue: 
icon: com.krypto.png
bugbounty: 
verdict: custodial
date: 2021-04-25
signer: 
reviewArchive:


providerTwitter: letskrypto
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The Krypto wallet has no statements on [their website](https://letskrypto.com) regarding the management of private keys.
this leads us to conclude the wallet funds are likely under the control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

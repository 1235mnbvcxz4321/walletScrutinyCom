---
wsId: 
title: "Genesis Vision"
altTitle: 
authors:

users: 10000
appId: vision.genesis.clientapp.investor
released: 2018-02-15
updated: 2021-06-03
version: "2.6.3"
stars: 4.7
ratings: 364
reviews: 119
size: 41M
website: 
repository: 
issue: 
icon: vision.genesis.clientapp.investor.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Skywallet"
altTitle: 
authors:

users: 500
appId: skywallet.net
released: 2020-05-11
updated: 2020-05-11
version: "1.0.3"
stars: 0.0
ratings: 
reviews: 
size: 5.7M
website: 
repository: 
issue: 
icon: skywallet.net.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



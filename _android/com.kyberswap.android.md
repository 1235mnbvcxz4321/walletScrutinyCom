---
wsId: 
title: "KyberSwap: Buy, Trade, Transfer Cryptocurrencies"
altTitle: 
authors:

users: 50000
appId: com.kyberswap.android
released: 2019-08-14
updated: 2021-07-21
version: "1.1.37"
stars: 4.2
ratings: 1810
reviews: 871
size: 19M
website: https://kyberswap.com
repository: 
issue: 
icon: com.kyberswap.android.png
bugbounty: 
verdict: nobtc
date: 2020-11-16
signer: 
reviewArchive:


providerTwitter: KyberSwap
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.kyberswap.android/
---


> KyberSwap as an Ethereum DEX (decentralised exchange), does not hold custody of your assets or digital currencies, so you are always in full control.

is an implied claim of being non-custodial but "KyberSwap as an Ethereum DEX"
refers to an ETH smart contract and not to this Android wallet of the same name.

> Exchange ETH and over 70+ tokens including DAI, USDC, WBTC, MKR, BAT, LINK.

as it turns out, this is not a BTC wallet. Only ETH and ETH tokens.

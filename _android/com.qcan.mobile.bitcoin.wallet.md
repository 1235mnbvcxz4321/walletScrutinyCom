---
wsId: Qcan
title: "Mobile Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.qcan.mobile.bitcoin.wallet
released: 2017-08-06
updated: 2021-06-03
version: "0.8.851"
stars: 4.3
ratings: 159
reviews: 90
size: 29M
website: https://qcan.com
repository: 
issue: 
icon: com.qcan.mobile.bitcoin.wallet.png
bugbounty: 
verdict: nosource
date: 2020-12-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.qcan.mobile.bitcoin.wallet/
---


> **Complete Control**<br>
  Your Bitcoin, 100% Under Your Control. You hold the key. No intermediary.

That is a clear claim to be non-custodial but neither on Google Play nor the
website can we find a link to source code. This app is thus **not verifiable**.

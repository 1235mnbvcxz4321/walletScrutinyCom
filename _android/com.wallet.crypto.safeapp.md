---
wsId: 
title: "Safe Wallet"
altTitle: 
authors:

users: 100
appId: com.wallet.crypto.safeapp
released: 2020-05-23
updated: 2020-05-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 62M
website: 
repository: 
issue: 
icon: com.wallet.crypto.safeapp.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "LATOKEN: Bitcoin Wallet, Crypto Exchange"
altTitle: 
authors:
- danny
users: 500000
appId: com.latoken.wallet
released: 2020-03-17
updated: 2021-08-18
version: "1.88.00"
stars: 4.4
ratings: 22710
reviews: 11138
size: 25M
website: 
repository: 
issue: 
icon: com.latoken.wallet.png
bugbounty: https://hackenproof.com/latoken/latoken-exchange
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: latokens
providerLinkedIn: latoken
providerFacebook: LiquidAssetToken
providerReddit: 

redirect_from:

---


> Trade, send, sell, or buy Ethereum (ETH), Bitcoin (BTC), Litecoin (LTC), and any other 200+ digital assets.

> For  the  purposes  of  anti-money  laundering  prevention  and  combating  terrorist  activities, LATOKEN reserves the right  to request  any additional information and documents from you about you and/or your Transaction, and suspend your access to your Wallet anytime in case the documents or information provided by you are unsatisfactory or insufficient, as LATOKEN decide in its sole discretion.

This states that the provider can suspend access to your wallet, making it custodial and thus not verifiable.




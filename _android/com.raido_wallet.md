---
wsId: 
title: "Multifunctional crypto wallet - Raido"
altTitle: 
authors:

users: 10000
appId: com.raido_wallet
released: 2020-07-27
updated: 2021-06-29
version: "1.2.0"
stars: 4.2
ratings: 43
reviews: 34
size: 39M
website: 
repository: 
issue: 
icon: com.raido_wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: Luno
title: "Luno: Buy Bitcoin, Ethereum and Cryptocurrency"
altTitle: 
authors:
- leo
users: 5000000
appId: co.bitx.android.wallet
released: 2014-11-01
updated: 2021-08-11
version: "7.20.0"
stars: 4.1
ratings: 84093
reviews: 44577
size: 70M
website: https://www.luno.com
repository: 
issue: 
icon: co.bitx.android.wallet.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:
- date: 2019-11-14
  version: "6.8.0"
  appHash: 
  gitRevision: 372c9c03c6422faed457f1a9975d7cab8f13d01f
  verdict: nosource

providerTwitter: LunoGlobal
providerLinkedIn: lunoglobal
providerFacebook: luno
providerReddit: 

redirect_from:
  - /luno/
  - /co.bitx.android.wallet/
  - /posts/2019/11/luno/
  - /posts/co.bitx.android.wallet/
---


Luno: Buy Bitcoin, Ethereum and Cryptocurrency
advertises on the Playstore:

> **Keeping your crypto safe**<br>
> The majority of customer Bitcoin funds are kept in what we call “deep freeze” storage. These are multi-signature wallets, with private keys stored in different bank vaults. No single person ever has access to more than one key. We maintain a multi-signature hot wallet to facilitate instant Bitcoin withdrawals.

This tells us we are dealing with a custodial wallet here. Our verdict: **not
verifiable**.

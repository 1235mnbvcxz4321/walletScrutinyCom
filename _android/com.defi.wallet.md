---
wsId: 
title: "Crypto.com l DeFi Wallet"
altTitle: 
authors:
- leo
users: 1000000
appId: com.defi.wallet
released: 2020-05-11
updated: 2021-08-13
version: "1.15.0"
stars: 4.2
ratings: 6164
reviews: 1974
size: 56M
website: https://crypto.com/en/defi/
repository: 
issue: 
icon: com.defi.wallet.png
bugbounty: 
verdict: nosource
date: 2021-01-10
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:
  - /com.defi.wallet/
  - /posts/com.defi.wallet/
---


This app's description is promising:

> Decentralized:
> - Gain full control of your crypto and private keys [...]

On their website though we cannot find any links to source code.

Searching their `appId` on GitHub,
[yields nothing](https://github.com/search?q=%22com.defi.wallet%22) neither.

This brings us to the verdict: **not verifiable**.

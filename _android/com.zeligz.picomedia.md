---
wsId: 
title: "pmgc: Bitcoin and crypto currency wallet"
altTitle: 
authors:

users: 100
appId: com.zeligz.picomedia
released: 2019-09-03
updated: 2020-01-27
version: "1.6"
stars: 4.7
ratings: 14
reviews: 10
size: 28M
website: 
repository: 
issue: 
icon: com.zeligz.picomedia.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Freeda Wallet - Buy, Sell, Hold Bitcoin"
altTitle: 
authors:

users: 1000
appId: com.freeda.freedaapp
released: 2021-04-13
updated: 2021-05-14
version: "1.2.0"
stars: 5.0
ratings: 46
reviews: 31
size: 24M
website: 
repository: 
issue: 
icon: com.freeda.freedaapp.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "DX.Exchange - Buy&Sell Bitcoin"
altTitle: 
authors:

users: 5000
appId: antroid.cxtech.com.cxandroidproject
released: 2019-02-25
updated: 2019-10-02
version: "1.3.12"
stars: 3.7
ratings: 45
reviews: 24
size: 10M
website: 
repository: 
issue: 
icon: antroid.cxtech.com.cxandroidproject.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.3.12"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Hebe Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: com.hebeblock.hebewallet
released: 2019-03-04
updated: 2021-08-09
version: "1.3.52"
stars: 2.5
ratings: 22
reviews: 15
size: 24M
website: https://hebe.cc/
repository: 
issue: 
icon: com.hebeblock.hebewallet.png
bugbounty: 
verdict: nosource
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: BlockHebe
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /posts/com.hebeblock.hebewallet/
---


In the description the provider claims:

> Hebe Wallet is a decentralized wallet that supports local transaction
  signatures, so your mnemonics will never be sent over the internet.

so it's not custodial but we can't find any source code. This app is
**not verifiable**.

---
wsId: 
title: "Amir Wallet"
altTitle: 
authors:

users: 10000
appId: capital.amir.wallet
released: 2021-08-03
updated: 2021-08-09
version: "3.8.2"
stars: 4.1
ratings: 250
reviews: 147
size: 35M
website: 
repository: 
issue: 
icon: capital.amir.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



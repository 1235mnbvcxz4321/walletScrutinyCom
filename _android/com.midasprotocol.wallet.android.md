---
wsId: midas
title: "Midas Crypto Wallet: Bitcoin, Ethereum, XRP, EOS"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.midasprotocol.wallet.android
released: 2018-11-02
updated: 2021-07-29
version: "2.0.6.1"
stars: 4.1
ratings: 442
reviews: 271
size: 44M
website: https://midasprotocol.io/
repository: 
issue: 
icon: com.midasprotocol.wallet.android.png
bugbounty: 
verdict: custodial
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: MidasProtocol
providerLinkedIn: 
providerFacebook: midasprotocol.io
providerReddit: 

redirect_from:

---


No statements regarding private key managment can be found on the [providers website](https://midasprotocol.io/) or [Support section](https://support.midasprotocol.io/hc/en-us).
It would be prudent to assume the private keys are under the control of the provider.


Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

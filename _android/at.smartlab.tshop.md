---
wsId: 
title: "TabShop POS - Point of Sale"
altTitle: 
authors:
- danny
users: 500000
appId: at.smartlab.tshop
released: 2012-12-22
updated: 2021-08-18
version: "196"
stars: 4.2
ratings: 3710
reviews: 1615
size: 8.0M
website: 
repository: 
issue: 
icon: at.smartlab.tshop.png
bugbounty: 
verdict: nowallet
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


. TabShop Point of Sale (POS) app is the perfect companion app for your retail store, cafe, bar, restaurant, pizzeria, bakery, coffee shop, food truck, grocery store, beauty salon, car wash and more.

This does not sound like a BTC wallet.

> TabShop manages several tables, to generate gift cards, checkout with Bitcoin, PayPal and Stripe, and to directly scan product codes with the built in camera.

We assume it is possible to send/receive BTC payments with this app, but it is not actually meant to be used as a wallet. Therefore we mark this as **not a wallet.**


---
wsId: 
title: "Proton Wallet"
altTitle: 
authors:

users: 5000
appId: com.metallicus.protonwallet
released: 2021-02-22
updated: 2021-06-17
version: "1.5.10"
stars: 4.4
ratings: 114
reviews: 74
size: 44M
website: 
repository: 
issue: 
icon: com.metallicus.protonwallet.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



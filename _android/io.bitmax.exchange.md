---
wsId: 
title: "AscendEX(BitMax)"
altTitle: 
authors:
- kiwilamb
- leo
users: 100000
appId: io.bitmax.exchange
released: 
updated: 2021-04-25
version: "2.4.7"
stars: 4.2
ratings: 2788
reviews: 947
size: 22M
website: https://ascendex.com
repository: 
issue: 
icon: io.bitmax.exchange.png
bugbounty: 
verdict: defunct
date: 2021-05-11
signer: 
reviewArchive:
- date: 2021-04-20
  version: "2.4.7"
  appHash: 
  gitRevision: 6849790cf3f18653fbe1116b54693fec1419d0ca
  verdict: custodial


providerTwitter: AscendEX_Global
providerLinkedIn: 
providerFacebook: AscendEXOfficial
providerReddit: AscendEX_Official

redirect_from:

---


This app apparently was removed in favor of {% include walletLink.html wallet='android/com.ascendex.exchange' %}.
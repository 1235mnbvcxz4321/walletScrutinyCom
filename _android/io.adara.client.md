---
wsId: 
title: "Adara Bitcoin & Crypto Trading"
altTitle: 
authors:

users: 1000
appId: io.adara.client
released: 2019-11-25
updated: 2020-02-05
version: "1.3"
stars: 4.4
ratings: 13
reviews: 6
size: 27M
website: 
repository: 
issue: 
icon: io.adara.client.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: Unocoin
title: "Unocoin"
altTitle: 
authors:
- leo
users: 1000000
appId: com.unocoin.unocoinwallet
released: 2016-11-30
updated: 2021-08-20
version: "4.0.2"
stars: 3.8
ratings: 16167
reviews: 8882
size: 9.7M
website: https://www.unocoin.com
repository: 
issue: 
icon: com.unocoin.unocoinwallet.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: Unocoin
providerLinkedIn: unocoin
providerFacebook: unocoin
providerReddit: 

redirect_from:
  - /com.unocoin.unocoinwallet/
  - /posts/com.unocoin.unocoinwallet/
---


This app appears to be the interface to a trading platform. The description on
Google Play does not talk about where the keys are stored but it links to their
website and there we read

> AES-256 Encryption<br>
  The address-private key pairs obtained are encrypted using AES-256, sealed in
  envelopes and stored in multiple safe deposit lockers.

which clearly means they have the keys and you don't. As a custodial service,
this app is **not verifiable**.

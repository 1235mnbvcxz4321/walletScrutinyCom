---
wsId: valr
title: "VALR - Crypto Exchange & Payments"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.valr.app
released: 2019-09-13
updated: 2021-07-18
version: "1.0.28"
stars: 4.4
ratings: 748
reviews: 405
size: 99M
website: https://www.valr.com
repository: 
issue: 
icon: com.valr.app.png
bugbounty: 
verdict: custodial
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: valrdotcom
providerLinkedIn: valr
providerFacebook: VALRdotcom
providerReddit: 

redirect_from:

---


I need not go further into researching this wallet as the statement on the Google Play description screams custodial.

> We hold your cryptocurrencies in both “cold storage” and “hot wallets”.

This is an exchange trading wallet that holds the customers funds in the providers control.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.


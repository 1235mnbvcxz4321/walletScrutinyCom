---
wsId: 
title: "XREX Exchange - Trusted Crypto Trading Community"
altTitle: 
authors:

users: 1000
appId: com.xrex.mobile
released: 2020-02-20
updated: 2021-08-16
version: "1.5.216"
stars: 4.2
ratings: 23
reviews: 7
size: 38M
website: 
repository: 
issue: 
icon: com.xrex.mobile.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



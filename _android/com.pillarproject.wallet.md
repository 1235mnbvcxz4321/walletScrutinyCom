---
wsId: 
title: "Pillar. 1 wallet for all chains, dapps, DeFi & NFT"
altTitle: 
authors:

users: 50000
appId: com.pillarproject.wallet
released: 2018-12-13
updated: 2021-08-04
version: "3.4.3"
stars: 3.4
ratings: 432
reviews: 232
size: 51M
website: https://pillarproject.io
repository: https://github.com/pillarwallet/pillarwallet
issue: 
icon: com.pillarproject.wallet.png
bugbounty: 
verdict: nobtc
date: 2020-02-20
signer: 
reviewArchive:


providerTwitter: PillarWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.pillarproject.wallet/
  - /posts/com.pillarproject.wallet/
---


This app is not a Bitcoin wallet. Neither the description nor the website claim
support of BTC and when installing it, you can find tokens with "Bitcoin" in
their name that can be managed with this app but none of them actually is Bitcoin.

---
wsId: 
title: "Bitcoin Investment Service"
altTitle: 
authors:

users: 500
appId: com.bitfxservice.bitfxservice
released: 2019-10-08
updated: 2019-10-08
version: "1.0"
stars: 3.0
ratings: 5
reviews: 4
size: 54k
website: 
repository: 
issue: 
icon: com.bitfxservice.bitfxservice.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "VelcomEx - The Leading P2P Cryptocurrency Exchange"
altTitle: 
authors:

users: 100
appId: com.application.blocdesk
released: 2020-02-05
updated: 2020-03-03
version: "1.1.5"
stars: 0.0
ratings: 
reviews: 
size: 6.0M
website: 
repository: 
issue: 
icon: com.application.blocdesk.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



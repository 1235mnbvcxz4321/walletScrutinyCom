---
wsId: 
title: "SmartWallet"
altTitle: 
authors:

users: 10000
appId: br.com.coinbr.smartwallet
released: 2017-11-03
updated: 2021-07-27
version: "1.9.16"
stars: 2.9
ratings: 281
reviews: 215
size: 6.0M
website: 
repository: 
issue: 
icon: br.com.coinbr.smartwallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Bitcoin Wallet. A Secure Crypto Wallet - HiWallet"
altTitle: 
authors:

users: 1000
appId: org.topnetwork.hiwallet
released: 2020-04-08
updated: 2021-08-13
version: "V2.3.2"
stars: 4.0
ratings: 25
reviews: 15
size: 34M
website: 
repository: 
issue: 
icon: org.topnetwork.hiwallet.jpg
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



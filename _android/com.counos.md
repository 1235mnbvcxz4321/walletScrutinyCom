---
wsId: counos
title: "Counos Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.counos
released: 2018-07-03
updated: 2021-04-16
version: "1.9.50"
stars: 4.0
ratings: 511
reviews: 350
size: 38M
website: https://www.counos.io
repository: 
issue: 
icon: com.counos.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: counoscoin
providerLinkedIn: counosplatform
providerFacebook: CounosPlatform
providerReddit: 

redirect_from:

---


Nothing in the description hints at this app being non-custodial.

On the website
[the section about the mobile wallet](https://www.counos.io/counos-mobile-wallet-tutorial) explains in cumbersome ways how to migrate between security features
but doesn't answer this question neither.

For now we assume this app is custodial and thus **not verifiable**.

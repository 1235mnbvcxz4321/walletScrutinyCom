---
wsId: 
title: "Ledger Live"
altTitle: 
authors:

users: 500000
appId: com.ledger.live
released: 2019-01-27
updated: 2021-08-10
version: "2.32.2"
stars: 4.1
ratings: 4755
reviews: 2686
size: Varies with device
website: 
repository: 
issue: 
icon: com.ledger.live.png
bugbounty: 
verdict: nowallet
date: 2020-11-17
signer: 
reviewArchive:


providerTwitter: Ledger
providerLinkedIn: ledgerhq
providerFacebook: Ledger
providerReddit: 

redirect_from:
  - /com.ledger.live/
---


This is the companion app for the Ledger hardware wallets. As we fail to start
the app without connecting a hardware wallet, it is very likely this app is not
designed to store private keys or in other words it is probably impossible to
spend with this app without confirming each and every transaction on the
hardware wallet itself and is thus **not a wallet** itself.

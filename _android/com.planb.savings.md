---
wsId: 
title: "PlanB - Bitcoin Savings Planner"
altTitle: 
authors:

users: 10
appId: com.planb.savings
released: 2020-07-08
updated: 2020-07-13
version: "0.0.2"
stars: 0.0
ratings: 
reviews: 
size: 5.3M
website: 
repository: 
issue: 
icon: com.planb.savings.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "0.0.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



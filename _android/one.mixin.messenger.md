---
wsId: 
title: "Mixin - Crypto Wallet & Private Messenger"
altTitle: 
authors:

users: 10000
appId: one.mixin.messenger
released: 2018-05-24
updated: 2021-08-15
version: "0.32.1"
stars: 4.5
ratings: 1215
reviews: 952
size: 27M
website: 
repository: 
issue: 
icon: one.mixin.messenger.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



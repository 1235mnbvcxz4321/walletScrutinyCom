---
wsId: 
title: "Bit7Pay Bitcoin and Other Cryptocurrency Wallet"
altTitle: 
authors:

users: 5000
appId: bit7pay.com.bit7pay
released: 
updated: 2020-04-08
version: "1.0"
stars: 4.2
ratings: 169
reviews: 91
size: 16M
website: 
repository: 
issue: 
icon: bit7pay.com.bit7pay.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "FLASH Wallet"
altTitle: 
authors:

users: 5000
appId: com.coinapps.flash.wallet.android
released: 2019-08-08
updated: 2020-07-23
version: "2.4.1"
stars: 4.0
ratings: 45
reviews: 24
size: 10.0M
website: 
repository: 
issue: 
icon: com.coinapps.flash.wallet.android.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "2.4.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



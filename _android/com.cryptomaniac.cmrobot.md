---
wsId: 
title: "CM Robot - Autotrader Bitcoin App"
altTitle: 
authors:

users: 1000
appId: com.cryptomaniac.cmrobot
released: 2019-08-21
updated: 2019-09-11
version: "2.2.1"
stars: 2.8
ratings: 24
reviews: 19
size: 7.9M
website: 
repository: 
issue: 
icon: com.cryptomaniac.cmrobot.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "2.2.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



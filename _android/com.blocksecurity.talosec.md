---
wsId: 
title: "Talosec"
altTitle: 
authors:

users: 5
appId: com.blocksecurity.talosec
released: 2020-05-28
updated: 2020-06-14
version: "2.0.0"
stars: 0.0
ratings: 
reviews: 
size: 15M
website: 
repository: 
issue: 
icon: com.blocksecurity.talosec.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



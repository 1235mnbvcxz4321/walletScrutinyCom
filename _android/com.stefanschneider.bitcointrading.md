---
wsId: 
title: "Bitcoin Trading Global"
altTitle: 
authors:

users: 1000
appId: com.stefanschneider.bitcointrading
released: 2019-07-01
updated: 2019-10-12
version: "2.0.0"
stars: 4.3
ratings: 15
reviews: 7
size: 29M
website: 
repository: 
issue: 
icon: com.stefanschneider.bitcointrading.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



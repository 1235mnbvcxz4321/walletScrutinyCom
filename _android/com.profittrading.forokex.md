---
wsId: 
title: "ProfitTradingApp for OKEx - Trade much faster"
altTitle: 
authors:

users: 1000
appId: com.profittrading.forokex
released: 2020-06-11
updated: 2020-07-23
version: "1.2.0"
stars: 4.5
ratings: 13
reviews: 5
size: 19M
website: 
repository: 
issue: 
icon: com.profittrading.forokex.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.2.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



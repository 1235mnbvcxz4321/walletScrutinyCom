---
wsId: 
title: "Quick Coins Wallet"
altTitle: 
authors:

users: 1
appId: com.blockchainacuity.wallet
released: 2018-08-01
updated: 2018-08-01
version: "5.37"
stars: 0.0
ratings: 
reviews: 
size: 35M
website: 
repository: 
issue: 
icon: com.blockchainacuity.wallet.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "5.37"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



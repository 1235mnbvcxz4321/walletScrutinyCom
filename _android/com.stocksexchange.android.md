---
wsId: 
title: "STEX - Crypto Exchange. Buy & Sell Bitcoin."
altTitle: 
authors:

users: 100000
appId: com.stocksexchange.android
released: 2018-05-15
updated: 2021-08-11
version: "2.0.5"
stars: 4.5
ratings: 1283
reviews: 520
size: 9.8M
website: 
repository: 
issue: 
icon: com.stocksexchange.android.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



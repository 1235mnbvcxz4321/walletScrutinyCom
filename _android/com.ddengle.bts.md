---
wsId: 
title: "비트코인 거래소&지갑 유빗(Youbit)"
altTitle: 
authors:

users: 10000
appId: com.ddengle.bts
released: 2014-09-23
updated: 2017-12-08
version: "1.1.9"
stars: 3.5
ratings: 71
reviews: 53
size: 973k
website: 
repository: 
issue: 
icon: com.ddengle.bts.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.1.9"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



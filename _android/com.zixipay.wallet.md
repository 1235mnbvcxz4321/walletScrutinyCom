---
wsId: 
title: "ZixiPay - Tether Wallet, Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: com.zixipay.wallet
released: 2019-12-18
updated: 2021-07-08
version: "1.74"
stars: 4.4
ratings: 20
reviews: 7
size: 14M
website: 
repository: 
issue: 
icon: com.zixipay.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



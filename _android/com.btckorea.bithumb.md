---
wsId: bithumbko
title: "Bithumb - No.1 Digital Asset Platform"
altTitle: 
authors:
- leo
users: 1000000
appId: com.btckorea.bithumb
released: 2017-09-26
updated: 2021-08-18
version: "2.2.5"
stars: 3.2
ratings: 20557
reviews: 8810
size: 40M
website: https://www.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.png
bugbounty: 
verdict: custodial
date: 2021-02-19
signer: 
reviewArchive:


providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: bithumb
providerReddit: 

redirect_from:

---


This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.

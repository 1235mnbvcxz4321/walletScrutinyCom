---
wsId: 
title: "OKEx - Bitcoin/Crypto Trading Platform"
altTitle: 
authors:
- leo
users: 1000000
appId: com.okinc.okex.gp
released: 2019-10-29
updated: 2021-08-12
version: "5.2.0"
stars: 4.9
ratings: 123604
reviews: 38611
size: 154M
website: https://www.okex.com
repository: 
issue: 
icon: com.okinc.okex.gp.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: OKEx
providerLinkedIn: 
providerFacebook: okexofficial
providerReddit: OKEx

redirect_from:
  - /com.okinc.okex.gp/
  - /posts/com.okinc.okex.gp/
---


This app gives you access to a trading platform which sounds fully custodial and
therefore **not verifiable**.

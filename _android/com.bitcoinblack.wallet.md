---
wsId: bitcoinblack
title: "Bitcoin Black Wallet"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.bitcoinblack.wallet
released: 2020-06-11
updated: 2020-12-25
version: "1.0.6"
stars: 3.3
ratings: 1617
reviews: 968
size: 25M
website: https://bitcoin.black/
repository: 
issue: 
icon: com.bitcoinblack.wallet.png
bugbounty: 
verdict: nobtc
date: 2021-04-29
signer: 
reviewArchive:


providerTwitter: BCB_Official1
providerLinkedIn: 
providerFacebook: bitcoinblackofficial
providerReddit: AllAboardBitcoinBlack

redirect_from:

---


This is a wallet for an alt coin "Bitcoin Black"

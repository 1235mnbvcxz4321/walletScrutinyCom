---
wsId: 
title: "Kinesis Money - Exchange Gold, Bitcoin & Currency"
altTitle: 
authors:

users: 10000
appId: com.kinesis.kinesisapp
released: 2020-02-28
updated: 2021-08-09
version: "1.2.56"
stars: 3.4
ratings: 334
reviews: 157
size: 35M
website: 
repository: 
issue: 
icon: com.kinesis.kinesisapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "BlackFort Wallet & Exchange"
altTitle: 
authors:

users: 1000
appId: exchange.blackfort.wallet
released: 2020-08-31
updated: 2021-06-24
version: "1.2.4"
stars: 4.2
ratings: 61
reviews: 35
size: 17M
website: 
repository: 
issue: 
icon: exchange.blackfort.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



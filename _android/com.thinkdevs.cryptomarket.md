---
wsId: 
title: "Crypto Wallet 2020"
altTitle: 
authors:

users: 10000
appId: com.thinkdevs.cryptomarket
released: 2018-03-29
updated: 2021-01-19
version: "0.0.10"
stars: 3.7
ratings: 128
reviews: 72
size: 3.0M
website: 
repository: 
issue: 
icon: com.thinkdevs.cryptomarket.png
bugbounty: 
verdict: nowallet
date: 2020-12-14
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.thinkdevs.cryptomarket/
---


This is not a wallet:

> Crypto Wallet gives you quick and easy access to cryptocurrency prices, details.

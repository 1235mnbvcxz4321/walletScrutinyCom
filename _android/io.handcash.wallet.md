---
wsId: 
title: "HandCash"
altTitle: 
authors:

users: 50000
appId: io.handcash.wallet
released: 2019-09-10
updated: 2021-07-23
version: "2.6.6"
stars: 4.1
ratings: 459
reviews: 304
size: 36M
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
verdict: nobtc
date: 2019-12-28
signer: 
reviewArchive:


providerTwitter: handcashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.handcash.wallet/
  - /posts/io.handcash.wallet/
---


A BSV wallet.

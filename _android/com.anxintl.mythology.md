---
wsId: 
title: "DICE Wallet"
altTitle: 
authors:

users: 10000
appId: com.anxintl.mythology
released: 2016-04-18
updated: 2016-10-29
version: "1.3.3"
stars: 3.1
ratings: 45
reviews: 19
size: 4.2M
website: 
repository: 
issue: 
icon: com.anxintl.mythology.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.3.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



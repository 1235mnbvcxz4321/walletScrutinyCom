---
wsId: 
title: "Embit - Bitcoin blockchain wallet & data insertion"
altTitle: 
authors:

users: 100
appId: app.codecellar.embitwallet
released: 2019-12-01
updated: 2021-08-01
version: "4.56"
stars: 0.0
ratings: 
reviews: 
size: 7.8M
website: 
repository: 
issue: 
icon: app.codecellar.embitwallet.png
bugbounty: 
verdict: fewusers
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



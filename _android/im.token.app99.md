---
wsId: 
title: "AllToken : Bitcoin,Ethereum,Blockchain Wallet"
altTitle: 
authors:

users: 1000
appId: im.token.app99
released: 2020-05-09
updated: 2021-05-07
version: "2.9.6"
stars: 3.5
ratings: 6
reviews: 3
size: 45M
website: 
repository: 
issue: 
icon: im.token.app99.png
bugbounty: 
verdict: wip
date: 2021-06-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app probably was related to
{% include walletLink.html wallet='android/im.token.app' %} as it has almost
the same `applicationId` but we did not get to look closer into it.

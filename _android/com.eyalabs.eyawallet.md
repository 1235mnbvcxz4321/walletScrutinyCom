---
wsId: 
title: "이야페이, 비트코인 지갑"
altTitle: 
authors:

users: 10000
appId: com.eyalabs.eyawallet
released: 2015-12-03
updated: 2019-02-20
version: "5.3.2"
stars: 3.2
ratings: 90
reviews: 51
size: 5.7M
website: 
repository: 
issue: 
icon: com.eyalabs.eyawallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "5.3.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Ecoin Wallet"
altTitle: 
authors:

users: 100000
appId: fio.ecoin.wallet
released: 2020-10-08
updated: 2021-07-19
version: "5.7.8"
stars: 3.9
ratings: 2261
reviews: 1013
size: 84M
website: 
repository: 
issue: 
icon: fio.ecoin.wallet.jpg
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



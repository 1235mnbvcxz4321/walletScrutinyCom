---
wsId: 
title: "BitoEX幣託-虛擬通貨錢包"
altTitle: 
authors:

users: 10000
appId: com.bitoex.bitoexapp
released: 2019-11-18
updated: 2019-11-19
version: "1.0.0"
stars: 3.1
ratings: 138
reviews: 100
size: 8.2M
website: 
repository: 
issue: 
icon: com.bitoex.bitoexapp.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Crypto POS"
altTitle: 
authors:

users: 100
appId: com.gobaba.cryptopos
released: 2018-11-05
updated: 2018-11-05
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 6.6M
website: 
repository: 
issue: 
icon: com.gobaba.cryptopos.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "CryptoTab Lite — Get Bitcoin in your wallet"
altTitle: 
authors:
- danny
users: 500000
appId: lite.cryptotab.android
released: 2021-06-16
updated: 2021-08-19
version: "6.0.12"
stars: 4.2
ratings: 4086
reviews: 2093
size: Varies with device
website: 
repository: 
issue: 
icon: lite.cryptotab.android.png
bugbounty: 
verdict: nowallet
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is a lite version of {% include walletLink.html wallet='android/pro.cryptotab.android' verdict='true' %}.
It's safe to assume they have the same verdict.
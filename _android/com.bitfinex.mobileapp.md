---
wsId: Bitfinex
title: "Bitfinex: Trade Bitcoin & Digital Assets"
altTitle: 
authors:
- leo
users: 100000
appId: com.bitfinex.mobileapp
released: 2019-02-11
updated: 2021-08-11
version: "4.9.0"
stars: 3.8
ratings: 1432
reviews: 622
size: 59M
website: https://www.bitfinex.com
repository: 
issue: 
icon: com.bitfinex.mobileapp.png
bugbounty: 
verdict: custodial
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: bitfinex
providerLinkedIn: bitfinex
providerFacebook: bitfinex
providerReddit: bitfinex

redirect_from:

---


From a security standpoint, this app appears to be doing something right: The
user does not provide his Bifinex exchange credentials but an API key which he
has to create on Bitfinex and there he can limit the permissions. For example
forbid sending coins off the exchange if he wants to use the app only for
trading but not as a wallet. The app can be used as a wallet, too though. It
remains a remote control for your Bitfinex exchange account and that is
custodial.

As a custodial app it is **not verifiable**.

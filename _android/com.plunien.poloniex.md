---
wsId: Poloniex
title: "Poloniex Crypto Exchange"
altTitle: 
authors:
- leo
users: 100000
appId: com.plunien.poloniex
released: 2017-06-19
updated: 2021-08-18
version: "1.29.1"
stars: 4.3
ratings: 5701
reviews: 2973
size: 27M
website: https://support.poloniex.com
repository: 
issue: 
icon: com.plunien.poloniex.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: Poloniex
providerLinkedIn: 
providerFacebook: poloniex
providerReddit: 

redirect_from:
  - /com.plunien.poloniex/
  - /posts/com.plunien.poloniex/
---


This app is not primarily advertised as a wallet. It is an interface to a crypto
exchange but on the Google Play description we read:

> Manage your balance and trades on the go so you never miss a market move.
  Deposit and withdraw from your crypto wallet, monitor account balances and
  orders, view real-time ticker updates across all markets, and choose your
  favorite cryptocurrencies and create price alerts for them.

So it has a wallet integrated which is custodial by the sound of it and
therefore **not verifiable**.

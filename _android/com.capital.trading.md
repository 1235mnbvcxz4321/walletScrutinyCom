---
wsId: 
title: "Trading app by Capital.com"
altTitle: 
authors:
- danny
users: 1000000
appId: com.capital.trading
released: 2017-10-18
updated: 2021-08-16
version: "1.31"
stars: 4.4
ratings: 20294
reviews: 6803
size: Varies with device
website: 
repository: https://capital.com/
issue: 
icon: com.capital.trading.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: capitalcom
providerLinkedIn: capital.com
providerFacebook: capitalcom
providerReddit: 

redirect_from:

---


> Capital.com is here to change your entire trading experience. We provide an award-winning* free online trading app** and easy-to-use platform, designed to make trading smarter, simpler and more intuitive.

This sounds like an exchange. We assume this product is custodial and therefore **not verifiable**.

As an additional note, some clients in the reviews report having problems with withdrawal.

---
wsId: 
title: "LVL - The Money App"
altTitle: 
authors:

users: 10000
appId: co.lvl.firstapp
released: 2020-06-30
updated: 2021-08-11
version: "1.0.4"
stars: 4.0
ratings: 130
reviews: 58
size: 66M
website: 
repository: 
issue: 
icon: co.lvl.firstapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Coinsbit - Cryptocurrency Exchange: BTC, ETH, USDT"
altTitle: 
authors:

users: 100000
appId: com.coinsbit.coinsbit
released: 2019-10-28
updated: 2019-11-27
version: "1.0"
stars: 2.7
ratings: 1536
reviews: 1108
size: 16M
website: 
repository: 
issue: 
icon: com.coinsbit.coinsbit.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



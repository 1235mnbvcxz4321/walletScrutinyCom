---
wsId: 
title: "Aladdin Pro"
altTitle: 
authors:

users: 50000
appId: com.abbc.aladdin.pro
released: 2019-11-15
updated: 2021-06-09
version: "1.4.3"
stars: 3.1
ratings: 1835
reviews: 1363
size: 23M
website: 
repository: 
issue: 
icon: com.abbc.aladdin.pro.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: metamask
title: "MetaMask - Buy, Send and Swap Crypto"
altTitle: 
authors:
- leo
users: 1000000
appId: io.metamask
released: 2020-09-01
updated: 2021-08-17
version: "3.0.1"
stars: 3.3
ratings: 10064
reviews: 5229
size: 29M
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is an ETH-only app and thus not a Bitcoin wallet.

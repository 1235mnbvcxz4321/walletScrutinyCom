---
wsId: 
title: "Vita Wallet"
altTitle: 
authors:

users: 10000
appId: com.vita_wallet
released: 2019-10-16
updated: 2021-08-11
version: "3.5.2"
stars: 4.3
ratings: 257
reviews: 180
size: 45M
website: 
repository: 
issue: 
icon: com.vita_wallet.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



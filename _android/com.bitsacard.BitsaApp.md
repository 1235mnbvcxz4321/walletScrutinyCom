---
wsId: 
title: "Bitsa"
altTitle: 
authors:

users: 10000
appId: com.bitsacard.BitsaApp
released: 2019-04-09
updated: 2021-08-13
version: "2.6.3"
stars: 2.2
ratings: 732
reviews: 497
size: 79M
website: 
repository: 
issue: 
icon: com.bitsacard.BitsaApp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



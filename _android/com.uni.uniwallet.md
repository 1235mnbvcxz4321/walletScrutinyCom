---
wsId: 
title: "UNI&PAY"
altTitle: 
authors:

users: 1000
appId: com.uni.uniwallet
released: 2018-06-05
updated: 2019-02-19
version: "7.0.4"
stars: 4.0
ratings: 44
reviews: 23
size: 4.2M
website: 
repository: 
issue: 
icon: com.uni.uniwallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "7.0.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Ionia, Blockchain Financial Platform"
altTitle: 
authors:

users: 5000
appId: tech.ionia.app
released: 2018-05-11
updated: 2019-02-27
version: "1.1.3"
stars: 4.5
ratings: 40
reviews: 24
size: 11M
website: 
repository: 
issue: 
icon: tech.ionia.app.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



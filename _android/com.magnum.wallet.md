---
wsId: 
title: "Magnum Wallet – Bitcoin, Ethereum, Crypto Exchange"
altTitle: 
authors:
- leo
users: 10000
appId: com.magnum.wallet
released: 2019-04-23
updated: 2019-08-29
version: "1.0.12"
stars: 3.4
ratings: 222
reviews: 156
size: 3.0M
website: https://magnumwallet.co
repository: 
issue: 
icon: com.magnum.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-19
signer: 
reviewArchive:
- date: 2021-08-17
  version: "1.0.12"
  appHash: 
  gitRevision: 79e92f0e1174136cdf05180253c87cacb589f002
  verdict: stale
- date: 2020-04-07
  version: "1.0.12"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: magnum_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.magnum.wallet/
  - /posts/com.magnum.wallet/
---


In the app's description we read:

> **Simple & Secure Interface**
> Stay on top of your portfolio with intuitive navigation anytime, anywhere.
> With security & anonymity as our top priorities, we provide a fully
> non-custodial service, meaning that users have full control of their private
> keys and other personal information.

So this wallet claims to be non-custodial but can we find its source code?
Neither on Google Play nor on their website do we find a word about this product's
source code. Its properties thus are **not verifiable**.

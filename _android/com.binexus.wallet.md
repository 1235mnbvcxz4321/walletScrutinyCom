---
wsId: 
title: "Binexus  Wallet"
altTitle: 
authors:

users: 50
appId: com.binexus.wallet
released: 2018-03-28
updated: 2018-03-29
version: "v5.0.0"
stars: 0.0
ratings: 
reviews: 
size: 4.7M
website: 
repository: 
issue: 
icon: com.binexus.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "v5.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



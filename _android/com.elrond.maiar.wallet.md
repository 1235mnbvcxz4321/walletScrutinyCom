---
wsId: 
title: "Maiar: Crypto & eGold Wallet - Buy, Earn & Pay"
altTitle: 
authors:

users: 100000
appId: com.elrond.maiar.wallet
released: 2021-01-15
updated: 2021-08-19
version: "1.3.46"
stars: 4.5
ratings: 3734
reviews: 1644
size: 70M
website: 
repository: 
issue: 
icon: com.elrond.maiar.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: cex
title: "CEX.IO Cryptocurrency Exchange - Buy Bitcoin (BTC)"
altTitle: 
authors:
- kiwilamb
users: 1000000
appId: io.cex.app.prod
released: 2015-12-22
updated: 2021-08-10
version: "Varies with device"
stars: 4.1
ratings: 12502
reviews: 7910
size: Varies with device
website: https://cex.io
repository: 
issue: 
icon: io.cex.app.prod.png
bugbounty: 
verdict: custodial
date: 2021-04-16
signer: 
reviewArchive:


providerTwitter: cex_io
providerLinkedIn: cex-io
providerFacebook: CEX.IO
providerReddit: 

redirect_from:

---


The CEX.io mobile app claims on the website to manage bitcoins...

> Stay in control of your funds anywhere. Deposit and withdraw crypto and fiat, add your debit or credit card in a few clicks, and store your funds securely.

however their is no evidence of the wallet being non-custodial, with no source code repository listed or found...

Our verdict: This 'wallet' is probably custodial but does not provide public source and therefore is **not verifiable**.

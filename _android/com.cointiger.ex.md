---
wsId: 
title: "CoinTiger-Crypto Exchange"
altTitle: 
authors:

users: 100000
appId: com.cointiger.ex
released: 2020-07-23
updated: 2021-08-03
version: "5.0.0.2"
stars: 5.0
ratings: 8555
reviews: 895
size: 42M
website: 
repository: 
issue: 
icon: com.cointiger.ex.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



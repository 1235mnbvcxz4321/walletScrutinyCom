---
wsId: OKEx
title: "اوکی اکسچنج، خرید بیت کوین و ارز دیجیتال"
altTitle: 
authors:
- leo
users: 100000
appId: co.okex.app
released: 2019-09-11
updated: 2021-08-07
version: "5.5.0"
stars: 4.4
ratings: 4257
reviews: 2060
size: 7.2M
website: https://ok-ex.co
repository: 
issue: 
icon: co.okex.app.png
bugbounty: 
verdict: wip
date: 2021-06-18
signer: 
reviewArchive:
- date: 2020-11-16
  version: ""
  appHash: 
  gitRevision: bcb5dbfd724ca531c1965cce7ef0d38f023e4c0c
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.okex.app/
---


**Update:** This app appears to have disappeared from Google Play or maybe only
from English Google Play, as it apparently was in Arab, only? If we should
re-add it, please create an issue on our GitLab.

> The okex app is a digital currency trading platform

as such, this is probably a custodial offering.

As the website is broken, we can't find any contrary claims and conclude, this
app is **not verifiable**.

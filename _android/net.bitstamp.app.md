---
wsId: Bitstamp
title: "Bitstamp – Buy & Sell Bitcoin at Crypto Exchange"
altTitle: 
authors:
- leo
users: 100000
appId: net.bitstamp.app
released: 2019-01-29
updated: 2021-07-29
version: "2.3.2"
stars: 4.2
ratings: 10170
reviews: 2837
size: 137M
website: https://www.bitstamp.net
repository: 
issue: 
icon: net.bitstamp.app.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:

---


On the Google Play description we read:

> Convenient, but secure
>
> ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.

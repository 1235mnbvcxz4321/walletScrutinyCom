---
wsId: 
title: "SONICEX"
altTitle: 
authors:

users: 100
appId: com.sonicex
released: 2019-08-05
updated: 2020-04-15
version: "1.6"
stars: 3.9
ratings: 7
reviews: 3
size: 11M
website: 
repository: 
issue: 
icon: com.sonicex.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Coinom Wallet : Bitcoin Ethereum Altcoins Tokens"
altTitle: 
authors:
- emanuel
- leo
users: 10
appId: com.apptrack.coinom
released: 2021-05-25
updated: 2021-05-25
version: "1.0.12"
stars: 0.0
ratings: 
reviews: 
size: 41M
website: 
repository: 
issue: 
icon: com.apptrack.coinom.png
bugbounty: 
verdict: defunct
date: 2021-07-30
signer: 
reviewArchive:
- date: 2021-07-24
  version: "1.0.12" 
  appHash: 
  gitRevision: 39dc0c413233485694e168a9ebb31778ad09074c
  verdict: fake

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-07-30**: This app is not on Play Store anymore.

This app "{{ page.title }}" clearly tries to imitate
{% include walletLink.html wallet='android/com.coinomi.wallet' verdict='true' %}.

---
wsId: ShapeShift
title: "ShapeShift Buy & Trade Bitcoin & Top Crypto Assets"
altTitle: 
authors:
- leo
users: 100000
appId: com.shapeshift.droid_shapeshift
released: 2015-10-26
updated: 2021-07-16
version: "2.19.0"
stars: 3.0
ratings: 2791
reviews: 1751
size: 52M
website: https://shapeshift.com
repository: 
issue: 
icon: com.shapeshift.droid_shapeshift.png
bugbounty: 
verdict: nosource
date: 2021-05-23
signer: 
reviewArchive:


providerTwitter: ShapeShift_io
providerLinkedIn: 
providerFacebook: ShapeShiftPlatform
providerReddit: 

redirect_from:
  - /com.shapeshift.droid_shapeshift/
---


ShapeShift is best known for their non-custodial exchange but this app appears
to be a wallet:

> **STORE YOUR CRYPTO IN A SECURE WALLET**<br>
  Setup a ShapeShift multi-chain wallet in seconds to store your crypto.

... and non-custodial:

> ShapeShift makes self-custody easy, never holding your coins, so you have
  complete control over your assets.

but is their code public? 

On the [referenced website](https://shapeshift.com/) there is no link back to
the app on App Store or Play Store but there is
[this site](https://shapeshift.com/invite) where they suggest having an
invite-only mobile app. When you provide them with your email (Seriously?) they ...
forward you to [this site](https://shapeshift.com/download) where there are
actually download links for both mobile apps.

As we couldn't find any source code we assume the app is closed source and
therefore **not verifiable**.

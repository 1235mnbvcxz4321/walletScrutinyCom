---
wsId: 
title: "Kraken Futures: Bitcoin & Crypto Futures Trading"
altTitle: 
authors:
- leo
users: 10000
appId: com.krakenfutures
released: 2019-10-07
updated: 2021-03-24
version: "5.24.0"
stars: 3.2
ratings: 102
reviews: 46
size: 12M
website: https://futures.kraken.com
repository: 
issue: 
icon: com.krakenfutures.png
bugbounty: 
verdict: custodial
date: 2020-06-20
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.krakenfutures/
  - /posts/com.krakenfutures/
---


This is the interface for an exchange and nothing in the description hints at
non-custodial parts to it.

---
wsId: HuobiWallet
title: "HuobiWallet"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.huobionchainwallet.gp
released: 2019-10-17
updated: 2021-08-16
version: "2.09.00.032"
stars: 4.9
ratings: 17192
reviews: 1246
size: 66M
website: https://huobiwallet.com
repository: 
issue: 
icon: com.huobionchainwallet.gp.png
bugbounty: 
verdict: nosource
date: 2021-04-20
signer: 
reviewArchive:


providerTwitter: HuobiWallet
providerLinkedIn: 
providerFacebook: HuobiWallet
providerReddit: 

redirect_from:

---


From the description of the play store app the wallet provider clearly states the private keys are in control of the user....

> Huobi Wallet users have sole control over their own private keys and thus have full control over their assets. There are no third parties involved in management of private keys.

However the non-custodial claims of the provider cannot be verified as no source code is available.

Our verdict: This 'wallet' is possibly non-custodial but does not provide public source and therefore is **not verifiable**.



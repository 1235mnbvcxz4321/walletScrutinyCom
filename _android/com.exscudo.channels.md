---
wsId: exscudo
title: "Channels. Secure wallet + Exchange + Private chat"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.exscudo.channels
released: 2018-05-17
updated: 2021-08-06
version: "2.18.854"
stars: 4.5
ratings: 547
reviews: 232
size: 61M
website: https://exscudo.com
repository: 
issue: 
icon: com.exscudo.channels.png
bugbounty: 
verdict: nosource
date: 2021-04-22
signer: 
reviewArchive:


providerTwitter: ex_scudo
providerLinkedIn: 
providerFacebook: exscudo
providerReddit: EXSCUDO

redirect_from:

---


The Play Store description states...

> All funds are fully protected and no one except the owner can access them.

the provider is claiming the wallet is non-custodial.
However no source code can be found in a public repository for validation.

Our verdict: This ‘wallet’ claims to be non-custodial, however with no source code this is **not verifiable**.


---
wsId: 
title: "Bian：Bitcoin Wallet-Virtual currency"
altTitle: 
authors:
- emanuel
- leo
users: 10
appId: com.xmzys.binance73
released: 2021-07-03
updated: 2021-07-03
version: "2.0"
stars: 0.0
ratings: 
reviews: 
size: 3.0M
website: 
repository: 
issue: 
icon: com.xmzys.binance73.png
bugbounty: 
verdict: fake
date: 2021-07-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app "{{ page.title }}" clearly tries to imitate
{% include walletLink.html wallet='android/com.binance.us' verdict='true' %} or
{% include walletLink.html wallet='android/com.binance.dev' verdict='true' %}.

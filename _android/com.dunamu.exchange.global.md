---
wsId: 
title: "Upbit (Global), the Digital-Asset Exchange"
altTitle: 
authors:

users: 100000
appId: com.dunamu.exchange.global
released: 2018-11-06
updated: 2021-08-19
version: "1.5.13p3"
stars: 4.2
ratings: 2384
reviews: 1925
size: 28M
website: 
repository: 
issue: 
icon: com.dunamu.exchange.global.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



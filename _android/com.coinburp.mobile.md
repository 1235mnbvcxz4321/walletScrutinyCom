---
wsId: CoinBurp
title: "CoinBurp - Buy and Sell Bitcoin. Crypto Wallet."
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.coinburp.mobile
released: 2020-06-17
updated: 2021-06-03
version: "1.0.14"
stars: 3.9
ratings: 102
reviews: 37
size: 43M
website: https://www.coinburp.com/
repository: 
issue: 
icon: com.coinburp.mobile.png
bugbounty: 
verdict: custodial
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: coinburp
providerLinkedIn: coinburp
providerFacebook: thecoinburp
providerReddit: 

redirect_from:

---


On the provider's website we find how private keys are managed, under [help section "Account Security"](https://help.coinburp.com/hc/en-gb/articles/360017544100-Are-Balances-Stored-on-CoinBurp-Insured-) we find their custodial provider is Bitpay.
This is the typical setup of exchange based apps, they hold a % of coins in hot wallets for daily trade management and store larger % in cold "offline" storage for security purposes.

Our Verdict: This "wallet" is custodial and therefor **not verifiable**

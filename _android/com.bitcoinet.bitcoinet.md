---
wsId: 
title: "Bitcoinet"
altTitle: 
authors:

users: 100
appId: com.bitcoinet.bitcoinet
released: 2020-06-25
updated: 2020-06-20
version: "5.0"
stars: 0.0
ratings: 
reviews: 
size: 2.8M
website: 
repository: 
issue: 
icon: com.bitcoinet.bitcoinet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "5.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: bingbon
title: "Bingbon Bitcoin & Cryptocurrency Platform"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: pro.bingbon.app
released: 2019-05-18
updated: 2021-08-14
version: "2.36.0"
stars: 4.4
ratings: 1259
reviews: 718
size: 28M
website: https://bingbon.com
repository: 
issue: 
icon: pro.bingbon.app.png
bugbounty: 
verdict: custodial
date: 2021-04-21
signer: 
reviewArchive:


providerTwitter: BingbonOfficial
providerLinkedIn: bingbon
providerFacebook: BingbonOfficial
providerReddit: Bingbon

redirect_from:

---


We cannot find any claims as to the custody of private keys found from Bingbon.
We must assume the wallet app is custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

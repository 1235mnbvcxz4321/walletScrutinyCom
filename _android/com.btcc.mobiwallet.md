---
wsId: 
title: "Mobi - Crypto Asset Wallet"
altTitle: 
authors:

users: 1000
appId: com.btcc.mobiwallet
released: 2019-03-28
updated: 2021-05-20
version: "2.32.2"
stars: 3.4
ratings: 24
reviews: 16
size: 14M
website: 
repository: 
issue: 
icon: com.btcc.mobiwallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "BitMeet Peer to Peer Crypto Trading"
altTitle: 
authors:

users: 100
appId: com.goodbarber.bitmeat
released: 2018-01-09
updated: 2018-01-10
version: "1.0"
stars: 2.4
ratings: 5
reviews: 3
size: 13M
website: 
repository: 
issue: 
icon: com.goodbarber.bitmeat.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



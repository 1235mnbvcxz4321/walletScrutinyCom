---
wsId: 
title: "Crypto Exchange Currency.com"
altTitle: 
authors:
- leo
users: 500000
appId: com.currency.exchange.prod2
released: 2019-04-15
updated: 2021-08-05
version: "1.14.4"
stars: 4.2
ratings: 4253
reviews: 1615
size: 18M
website: https://currency.com
repository: 
issue: 
icon: com.currency.exchange.prod2.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: currencycom
providerLinkedIn: currencycom/
providerFacebook: currencycom/
providerReddit: 

redirect_from:
  - /com.currency.exchange.prod2/
  - /posts/com.currency.exchange.prod2/
---


This is an interface for a custodial trading platform and thus **not
verifiable**.

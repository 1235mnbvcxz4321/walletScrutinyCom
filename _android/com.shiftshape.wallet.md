---
wsId: 
title: "Portis - Multi Blockchain Wallet"
altTitle: 
authors:

users: 1000
appId: com.shiftshape.wallet
released: 2021-07-16
updated: 2021-07-16
version: "1.0.0"
stars: 4.7
ratings: 71
reviews: 18
size: 5.6M
website: 
repository: 
issue: 
icon: com.shiftshape.wallet.png
bugbounty: 
verdict: defunct
date: 2021-08-11
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.0"
  appHash: 
  gitRevision: 3d4e0de7554c723d80c48c1d30caa7bad0af40aa
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-03**: This app is not on Play Store anymore.

---
wsId: 
title: "Bitplicity Bitcoin Wallet - Buy & Sell Crypto"
altTitle: 
authors:

users: 100
appId: com.Bitplicity.Bitplicityapp
released: 2020-06-06
updated: 2020-06-19
version: "1.4"
stars: 0.0
ratings: 
reviews: 
size: 1.6M
website: 
repository: 
issue: 
icon: com.Bitplicity.Bitplicityapp.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



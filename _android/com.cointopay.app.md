---
wsId: 
title: "Cointopay wallet"
altTitle: 
authors:

users: 1000
appId: com.cointopay.app
released: 2018-05-29
updated: 2021-03-31
version: "2.1.6"
stars: 3.4
ratings: 17
reviews: 10
size: 6.5M
website: 
repository: 
issue: 
icon: com.cointopay.app.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



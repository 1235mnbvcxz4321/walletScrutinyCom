---
wsId: klever
title: "Klever Wallet: Bitcoin, Ethereum, Tron, & more"
altTitle: 
authors:
- leo
users: 500000
appId: cash.klever.blockchain.wallet
released: 2020-08-27
updated: 2021-08-19
version: "4.4.1"
stars: 3.6
ratings: 9242
reviews: 5222
size: Varies with device
website: https://www.klever.io
repository: 
issue: 
icon: cash.klever.blockchain.wallet.png
bugbounty: 
verdict: nosource
date: 2021-05-22
signer: 
reviewArchive:


providerTwitter: klever_io
providerLinkedIn: 
providerFacebook: klever.io
providerReddit: 

redirect_from:
  - /cash.klever.blockchain.wallet/
---


On their website we read:

> **Peer-to-Peer**<br>
  Klever is a decentralized p2p and self-custody wallet network. Your Keys, your
  crypto.

so they claim the app is self-custodial but we cannot find any source code which
makes the app **not verifiable**.

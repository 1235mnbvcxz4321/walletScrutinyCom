---
wsId: 
title: "BtcTurk | PRO - Buy-Sell Bitcoin"
altTitle: 
authors:
- danny
users: 1000000
appId: com.btcturk.pro
released: 2020-09-10
updated: 2021-08-17
version: "1.32.2"
stars: 4.5
ratings: 136063
reviews: 40991
size: 27M
website: https://pro.btcturk.com/
repository: 
issue: 
icon: com.btcturk.pro.png
bugbounty: https://pro.btcturk.com/en/bug-bounty
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: btcturkpro
providerLinkedIn: btcturk
providerFacebook: btcturk
providerReddit: 

redirect_from:

---


> We offer mandatory two-step verification and an optional withdrawal PIN Code for your security. We store at least 97% of assets in secure, offline cold wallets.

This confirms it as custodial.

**Additional Notes:**

On the website's footer there is a [link to the API](https://docs.btcturk.com).

They also feature a [Bug Bounty](https://pro.btcturk.com/en/bug-bounty).

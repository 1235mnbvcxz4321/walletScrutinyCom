---
wsId: geminiwallet
title: "Gemini: Buy Bitcoin Instantly"
altTitle: 
authors:
- leo
users: 1000000
appId: com.gemini.android.app
released: 2018-12-10
updated: 2021-08-18
version: "3.29.0"
stars: 4.5
ratings: 21038
reviews: 4528
size: Varies with device
website: https://gemini.com
repository: 
issue: 
icon: com.gemini.android.app.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: gemini
providerLinkedIn: geminitrust
providerFacebook: GeminiTrust
providerReddit: 

redirect_from:
  - /com.gemini.android.app/
  - /posts/com.gemini.android.app/
---


This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.

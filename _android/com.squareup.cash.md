---
wsId: CashApp
title: "Cash App"
altTitle: 
authors:
- leo
users: 50000000
appId: com.squareup.cash
released: 
updated: 2021-08-16
version: "3.46.0"
stars: 4.3
ratings: 430374
reviews: 151955
size: 29M
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.png
bugbounty: 
verdict: custodial
date: 2020-08-06
signer: 
reviewArchive:


providerTwitter: cashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.squareup.cash/
---


This app is primarily a banking app:

> Cash App is the easiest way to send, spend, save, and invest your money. It’s
  the SAFE, FAST, and FREE mobile banking app.

but you can also receive and send Bitcoins with it:

> **BUY, SELL, DEPOSIT, AND WITHDRAW BITCOIN**
> 
> Cash App is the easiest way to buy, sell, deposit, and withdraw Bitcoin. Track
  the BTC price in realtime in your app and get started by buying as little as
  $1 of Bitcoin. Your BTC arrives in your app instantly. You can then decide to
  keep it safe in Cash App or withdraw it to a different wallet.

To little surprise for a banking app, we can't find any claims about the Bitcoin
Wallet being non-custodial. As a custodial offering, this app is **not
verifiable**.

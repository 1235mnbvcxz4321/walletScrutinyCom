---
wsId: 
title: "BTC Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: btcmine.btcwallet
released: 2017-08-13
updated: 2017-08-13
version: "1.1"
stars: 3.9
ratings: 16
reviews: 7
size: 2.3M
website: 
repository: 
issue: 
icon: btcmine.btcwallet.png
bugbounty: 
verdict: defunct
date: 2021-07-09
signer: 
reviewArchive:
- date: 2020-12-14
  version: "1.1"
  appHash: 
  gitRevision: 9159da8fa35082b76a2f1ea1029a90b22af4e042
  verdict: custodial


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /btcmine.btcwallet/
---


**Update 2021-07-09**: This app is no more. Given the shady findings below we
assume it was removed by Google and not coming back.

This app features no website or claims about the custody of coins. Its
description starts with:

> Download share and win. If your friends download app and send btc to their own
  wallet, you and your friends will win 0,001 BTC!!

and continues with more not so meaningful words. This app is probably a scam and
absent claims of being non-custodial we file it as **not verifiable**.

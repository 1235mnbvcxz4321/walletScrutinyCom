---
wsId: 
title: "Coinbase Airdrop - Free YFI, DOT & Aave"
altTitle: 
authors:

users: 500
appId: com.coinbase.airdrop
released: 2021-05-31
updated: 2021-05-31
version: "1.0"
stars: 3.3
ratings: 7
reviews: 6
size: 5.8M
website: 
repository: 
issue: 
icon: com.coinbase.airdrop.png
bugbounty: 
verdict: fewusers
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



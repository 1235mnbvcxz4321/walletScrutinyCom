---
wsId: krcokeypair
title: "NiXPAY"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.nixtouch
released: 2018-10-19
updated: 2019-12-27
version: "1.0.0.65"
stars: 5.0
ratings: 5
reviews: 2
size: 11M
website: https://www.nixblock.com
repository: 
issue: 
icon: kr.co.keypair.nixtouch.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-07
  version: "1.0.0.65"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.

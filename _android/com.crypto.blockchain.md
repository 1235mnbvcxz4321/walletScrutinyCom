---
wsId: 
title: "Blockchain India"
altTitle: 
authors:
- emanuel
- leo
users: 500
appId: com.crypto.blockchain
released: 2021-06-22
updated: 2021-08-17
version: "1.1.0"
stars: 0.0
ratings: 
reviews: 
size: 8.0M
website: 
repository: 
issue: 
icon: com.crypto.blockchain.jpg
bugbounty: 
verdict: fake
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This looks like a fake {% include walletLink.html wallet='android/piuk.blockchain.android' verdict='true' %}.
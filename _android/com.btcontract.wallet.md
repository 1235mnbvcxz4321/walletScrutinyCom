---
wsId: 
title: "Simple Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.btcontract.wallet
released: 2015-07-15
updated: 2021-08-20
version: "2.0.3"
stars: 3.5
ratings: 1004
reviews: 536
size: 22M
website: https://lightning-wallet.com
repository: https://github.com/btcontract/wallet/
issue: 
icon: com.btcontract.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: SimpleBtcWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The provider
[stated here](https://github.com/btcontract/lnwallet/issues/20#issuecomment-902663980)
that {% include walletLink.html wallet='android/com.lightning.walletapp' verdict='true' %}
was discontinued and its successor is {% include walletLink.html wallet='android/com.btcontract.wallet' verdict='true' %}.

This app, according to their website:

> Simple Bitcoin Wallet (aka SBW) is an open-source, non-custodial, autonomous
  wallet for Android devices which can store, send and receive bitcoins.

This sounds good. What doesn't sound good are the many scam accusations and bug
complaints in the reviews on Play Store. The link to f-droid.org also is broken.

This app needs a more in depth review.

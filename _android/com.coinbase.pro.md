---
wsId: coinbasepro
title: "Coinbase Pro – Bitcoin & Crypto Trading"
altTitle: 
authors:
- leo
users: 1000000
appId: com.coinbase.pro
released: 2020-01-06
updated: 2021-07-26
version: "1.0.79"
stars: 4.3
ratings: 14372
reviews: 4498
size: 56M
website: https://pro.coinbase.com
repository: 
issue: 
icon: com.coinbase.pro.jpg
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: CoinbasePro
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:
  - /com.coinbase.pro/
  - /posts/com.coinbase.pro/
---


This is the interface for a trading platform aka exchange. The funds are stored
with the provider. As a custodial service it is **not verifiable**.

---
wsId: 
title: "BITCOIN 2020 - New Improved Crypto - Wallet App"
altTitle: 
authors:

users: 100
appId: bitcoin.twenty.twenty
released: 2019-06-24
updated: 2019-07-18
version: "2.0"
stars: 0.0
ratings: 
reviews: 
size: 6.6M
website: 
repository: 
issue: 
icon: bitcoin.twenty.twenty.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "2.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Coinstec - Buy & sell Bitcoin, Cryptocurrencies TP"
altTitle: 
authors:

users: 500
appId: com.pandats.coinstec
released: 2017-12-24
updated: 2018-01-04
version: "10.0.2"
stars: 0.0
ratings: 
reviews: 
size: 43M
website: 
repository: 
issue: 
icon: com.pandats.coinstec.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "10.0.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



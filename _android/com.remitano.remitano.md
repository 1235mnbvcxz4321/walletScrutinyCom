---
wsId: Remitano
title: "Remitano - Buy & Sell Bitcoin Fast & Securely"
altTitle: 
authors:
- leo
users: 500000
appId: com.remitano.remitano
released: 2016-07-24
updated: 2021-08-10
version: "5.57.0"
stars: 4.4
ratings: 14859
reviews: 6622
size: 93M
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:
  - /posts/com.remitano.remitano/
---


This app is an interface to an exchange which holds your coins. On Google Play
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.

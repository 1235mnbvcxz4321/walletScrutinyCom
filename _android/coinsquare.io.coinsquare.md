---
wsId: 
title: "Coinsquare"
altTitle: 
authors:
- leo
users: 50000
appId: coinsquare.io.coinsquare
released: 2017-08-30
updated: 2021-07-30
version: "2.13.11"
stars: 2.2
ratings: 754
reviews: 512
size: 7.3M
website: https://coinsquare.com
repository: 
issue: 
icon: coinsquare.io.coinsquare.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: coinsquare
providerLinkedIn: 
providerFacebook: coinsquare.io
providerReddit: 

redirect_from:
  - /coinsquare.io.coinsquare/
  - /posts/coinsquare.io.coinsquare/
---


This is the interface for an exchange. In the description we read:

> We are SSL and 2FA enabled, with a 95% cold storage policy on all digital
  currency, and run multiple encrypted and distributed backups every day.

which means this is a custodial service and thus **not verifiable**.

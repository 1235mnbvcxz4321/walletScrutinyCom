---
wsId: 
title: "Trademonk Cryptocurrency Exchange"
altTitle: 
authors:

users: 10000
appId: com.ionicframework.myapp474083
released: 2017-06-16
updated: 2018-10-22
version: "2.0.13"
stars: 2.9
ratings: 278
reviews: 174
size: 8.6M
website: 
repository: 
issue: 
icon: com.ionicframework.myapp474083.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "2.0.13"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



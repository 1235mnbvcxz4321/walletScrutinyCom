---
wsId: 
title: "Lisk"
altTitle: 
authors:

users: 5000
appId: io.lisk.mobile
released: 2018-10-02
updated: 2021-03-25
version: "1.4.1"
stars: 4.2
ratings: 189
reviews: 92
size: 18M
website: 
repository: 
issue: 
icon: io.lisk.mobile.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "EXODUS CRYPTO BTC ETH WALLET TRX USDT"
altTitle: 
authors:
- emanuel
- leo
users: 100
appId: wall.exodus.btcandcrypto
released: 2021-07-16
updated: 2021-07-16
version: "6.045"
stars: 0.0
ratings: 
reviews: 
size: 10M
website: 
repository: 
issue: 
icon: wall.exodus.btcandcrypto.png
bugbounty: 
verdict: defunct
date: 2021-08-06
signer: 
reviewArchive:
- date: 2021-08-02
  version: "6.045"
  appHash: 
  gitRevision: 954e8c3a9aa0a5fdc7a523f841189f1b997fab8e
  verdict: fake

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-04**: This app disappeared from the Play Store 2 days after our
"fake" verdict.

By name and logo this tries to fool users into believing it's
{% include walletLink.html wallet='android/exodusmovement.exodus' verdict='true' %}.

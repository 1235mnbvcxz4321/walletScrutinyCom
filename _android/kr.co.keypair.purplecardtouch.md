---
wsId: krcokeypair
title: "Purple Touch"
altTitle: 
authors:

users: 100
appId: kr.co.keypair.purplecardtouch
released: 2018-09-11
updated: 2019-07-17
version: "1.0.0.45"
stars: 0.0
ratings: 
reviews: 
size: 9.6M
website: http://www.banco.id
repository: 
issue: 
icon: kr.co.keypair.purplecardtouch.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-07
  version: "1.0.0.45"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.

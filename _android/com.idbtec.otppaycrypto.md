---
wsId: 
title: "OTPPAY - Crypto Exchange & Merchant Payments"
altTitle: 
authors:

users: 1000
appId: com.idbtec.otppaycrypto
released: 2018-08-13
updated: 2020-04-01
version: "1.21"
stars: 4.6
ratings: 92
reviews: 59
size: 6.4M
website: 
repository: 
issue: 
icon: com.idbtec.otppaycrypto.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.21"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



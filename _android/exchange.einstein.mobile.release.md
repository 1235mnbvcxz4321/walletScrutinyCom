---
wsId: 
title: "Einstein Convert - Safe and Secure Bitcoin Wallet"
altTitle: 
authors:

users: 50000
appId: exchange.einstein.mobile.release
released: 2019-02-01
updated: 2019-10-17
version: "1.2.2"
stars: 1.5
ratings: 951
reviews: 769
size: 32M
website: 
repository: 
issue: 
icon: exchange.einstein.mobile.release.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.2.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



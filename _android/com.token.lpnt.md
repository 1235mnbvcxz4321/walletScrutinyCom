---
wsId: 
title: "LPNT - Crypto Wallet Beta version"
altTitle: 
authors:

users: 10000
appId: com.token.lpnt
released: 2021-05-03
updated: 2021-07-24
version: "1.6"
stars: 3.4
ratings: 630
reviews: 356
size: 5.7M
website: 
repository: 
issue: 
icon: com.token.lpnt.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "STICPAY"
altTitle: 
authors:

users: 50000
appId: com.sticpay.app.sticpay
released: 2017-08-31
updated: 2021-08-06
version: "3.45"
stars: 3.3
ratings: 279
reviews: 179
size: 6.4M
website: 
repository: 
issue: 
icon: com.sticpay.app.sticpay.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Bisq Notifications"
altTitle: 
authors:

users: 10000
appId: com.joachimneumann.bisq
released: 2018-09-06
updated: 2018-12-04
version: "1.1.0"
stars: 4.1
ratings: 59
reviews: 26
size: 3.6M
website: 
repository: 
issue: 
icon: com.joachimneumann.bisq.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: busha
title: "Busha: Buy & Sell Bitcoin, Ethereum. Crypto Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: co.busha.android
released: 2019-01-21
updated: 2021-06-02
version: "2.6.13"
stars: 4.1
ratings: 2301
reviews: 1763
size: 17M
website: https://busha.co
repository: 
issue: 
icon: co.busha.android.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: getbusha
providerLinkedIn: 
providerFacebook: getbusha
providerReddit: 

redirect_from:
  - /co.busha.android/
  - /posts/co.busha.android/
---


The description

> Won’t you rather trade and store your crypto assets on a platform you can
  trust? Busha is a Nigerian based crypto exchange that offers you all these and
  more.

sounds like it's an app to access an account on a custodial platform.

On their website they are more explicit:

> **Safe & Secure**<br>
  Our 24/7 monitoring systems, cold storage and industry-standard multi-sig
  wallets ensure that your assets are the safest they can be.

which is a list of features only relevant in a custodial context.

Our verdict: **not verifiable**.

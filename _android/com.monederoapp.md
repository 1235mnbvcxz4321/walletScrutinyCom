---
wsId: 
title: "Monedero: Wallet Bitcoin, Ethereum, Dash"
altTitle: 
authors:
- leo
users: 1000
appId: com.monederoapp
released: 2020-03-13
updated: 2021-06-25
version: "4.1.0"
stars: 4.0
ratings: 20
reviews: 9
size: 28M
website: 
repository: 
issue: 
icon: com.monederoapp.png
bugbounty: 
verdict: wip
date: 2020-05-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.monederoapp/
  - /posts/com.monederoapp/
---


This page was created by a script from the **appId** "com.monederoapp" and public
information found
[here](https://play.google.com/store/apps/details?id=com.monederoapp).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.

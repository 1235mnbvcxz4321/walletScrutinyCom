---
wsId: roqqu
title: "Roqqu: Buy & Sell Bitcoin and Cryptocurrency Fast"
altTitle: 
authors:
- kiwilamb
- leo
users: 500000
appId: com.roqqu.app
released: 2019-09-30
updated: 2021-08-17
version: "1.4.59"
stars: 3.3
ratings: 14699
reviews: 10678
size: 28M
website: https://roqqu.com
repository: 
issue: 
icon: com.roqqu.app.png
bugbounty: 
verdict: custodial
date: 2021-04-16
signer: 
reviewArchive:


providerTwitter: roqqupay
providerLinkedIn: 
providerFacebook: roqqupay
providerReddit: 

redirect_from:

---


The provider claims:

> SAFETY FIRST<br>
  Over 98% of cryptocurrency is stored securely offline and the rest is
  protected by industry-leading online security.

which means you do not control the keys to your Bitcoins.

It is somewhat obscure weather this wallet can even store bitcoins, but under
their FAQ section on their website it contains an article on how to
[send and receive bitcoins](https://roqqu.com/knowledge/articles/send/how-to-send-and-receive-btc)
with addresses and QR codes displayed.

Our verdict: This "wallet" is custodial and therefore **not verifiable**.

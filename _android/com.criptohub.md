---
wsId: 
title: "CRIPTOHUB - CHBR"
altTitle: 
authors:

users: 1000
appId: com.criptohub
released: 2019-07-30
updated: 2019-10-10
version: "1.0.6"
stars: 4.5
ratings: 33
reviews: 21
size: 11M
website: 
repository: 
issue: 
icon: com.criptohub.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



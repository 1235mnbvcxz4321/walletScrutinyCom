---
wsId: 
title: "BitOasis - Buy, Sell and Trade Digital Assets"
altTitle: 
authors:

users: 50000
appId: com.bitoasis
released: 2020-08-20
updated: 2021-08-05
version: "1.3.13"
stars: 4.1
ratings: 796
reviews: 198
size: 67M
website: 
repository: 
issue: 
icon: com.bitoasis.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



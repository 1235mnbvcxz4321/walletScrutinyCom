---
wsId: 
title: "Altcoin Bitcoin Trade"
altTitle: 
authors:
- leo
users: 10000
appId: com.mk.cryptofolio
released: 2018-05-02
updated: 2020-08-19
version: "1.0.51"
stars: 4.4
ratings: 24
reviews: 13
size: 8.0M
website: http://www.thecryptofolioapp.com
repository: 
issue: 
icon: com.mk.cryptofolio.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2020-06-20
  version: "1.0.51"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.mk.cryptofolio/
  - /posts/com.mk.cryptofolio/
---


This app is an interface for multiple exchanges:

> The CryptoFolio App™ lets you trade instantly on suported exchanges without
  signing in to them every time.

meaning you don't own your keys (The keys to the exchanges maybe but not to your
bitcoins). The app security is therefore **not verifiable**.

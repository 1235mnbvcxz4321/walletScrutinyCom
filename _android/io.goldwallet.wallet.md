---
wsId: 
title: "GoldWallet - Bitcoin Vault Wallet"
altTitle: 
authors:

users: 10000
appId: io.goldwallet.wallet
released: 2020-02-18
updated: 2021-07-28
version: "6.4.0"
stars: 4.0
ratings: 467
reviews: 264
size: 38M
website: https://bitcoinvault.global
repository: 
issue: 
icon: io.goldwallet.wallet.png
bugbounty: 
verdict: nobtc
date: 2020-12-14
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.goldwallet.wallet/
---


This app appears to not be a vault for Bitcoin but something for Bitcoin Vault.

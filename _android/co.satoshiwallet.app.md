---
wsId: 
title: "SatoshiWallet for Bitcoin, Ethereum, Monero & more"
altTitle: 
authors:

users: 1000
appId: co.satoshiwallet.app
released: 2019-08-16
updated: 2020-01-17
version: "2.1.3"
stars: 3.4
ratings: 16
reviews: 13
size: Varies with device
website: https://satoshipoint.io
repository: https://github.com/SatoshiWallet/ui
issue: 
icon: co.satoshiwallet.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-06-21
  version: "2.1.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


So this app was found by Emanuel, apparently for being open source. It is a fork
of the {% include walletLink.html wallet='android/co.edgesecure.app' %}.

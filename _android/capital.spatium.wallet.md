---
wsId: 
title: "Spatium Keyless Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: capital.spatium.wallet
released: 2018-08-31
updated: 2021-06-17
version: "2.3.40"
stars: 3.8
ratings: 27
reviews: 13
size: 127M
website: 
repository: 
issue: 
icon: capital.spatium.wallet.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



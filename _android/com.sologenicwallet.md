---
wsId: 
title: "SOLO Wallet"
altTitle: 
authors:

users: 5000
appId: com.sologenicwallet
released: 2020-02-04
updated: 2021-08-11
version: "2.0.9"
stars: 3.8
ratings: 122
reviews: 58
size: 48M
website: 
repository: 
issue: 
icon: com.sologenicwallet.png
bugbounty: 
verdict: nobtc
date: 2020-06-20
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.sologenicwallet/
  - /posts/com.sologenicwallet/
---


This wallet does not support BTC.

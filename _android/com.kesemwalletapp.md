---
wsId: 
title: "Kesem - Secured Crypto Wallet for Bitcoin"
altTitle: 
authors:

users: 100
appId: com.kesemwalletapp
released: 2019-01-09
updated: 2019-05-15
version: "2.17"
stars: 4.2
ratings: 5
reviews: 3
size: 31M
website: 
repository: 
issue: 
icon: com.kesemwalletapp.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "2.17"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



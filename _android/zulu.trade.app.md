---
wsId: 
title: "ZuluTrade - Copy Trading Platform"
altTitle: 
authors:

users: 100000
appId: zulu.trade.app
released: 2011-03-04
updated: 2021-08-09
version: "4.23.5"
stars: 4.4
ratings: 1761
reviews: 583
size: 34M
website: 
repository: 
issue: 
icon: zulu.trade.app.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



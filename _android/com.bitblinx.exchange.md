---
wsId: 
title: "BITBLINX - Bitcoin Exchange & Crypto Trading"
altTitle: 
authors:

users: 5000
appId: com.bitblinx.exchange
released: 2019-11-16
updated: 2020-05-01
version: "1.8"
stars: 4.6
ratings: 214
reviews: 184
size: 4.1M
website: 
repository: 
issue: 
icon: com.bitblinx.exchange.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



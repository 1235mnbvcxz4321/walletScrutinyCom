---
wsId: 
title: "Currency.com: Investing"
altTitle: 
authors:

users: 100000
appId: com.currency.exchange.investsmart
released: 2020-12-10
updated: 2021-07-20
version: "1.14.0"
stars: 4.4
ratings: 182
reviews: 58
size: Varies with device
website: 
repository: 
issue: 
icon: com.currency.exchange.investsmart.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



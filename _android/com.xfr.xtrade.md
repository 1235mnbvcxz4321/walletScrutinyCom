---
wsId: 
title: "Xtrade - Online Trading"
altTitle: 
authors:

users: 1000000
appId: com.xfr.xtrade
released: 2014-10-02
updated: 2021-06-17
version: "6.0.60"
stars: 4.0
ratings: 36534
reviews: 11459
size: 28M
website: http://www.xtrade.com/
repository: 
issue: 
icon: com.xfr.xtrade.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: XTrade
providerLinkedIn: xtrade
providerFacebook: XTrade
providerReddit: 

redirect_from:

---


> Enjoy the ultimate mobile trading experience when you access your Xtrade trading account from your phone

Like most trading platforms it seems to have a **custodial** wallet integrated. Therefore it is **not verifiable.**

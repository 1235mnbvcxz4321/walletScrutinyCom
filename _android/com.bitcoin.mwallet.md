---
wsId: mwallet
title: "Bitcoin Wallet: buy BTC, BCH & ETH"
altTitle: "Bitcoin Wallet by Bitcoin.com"
authors:
- leo
users: 1000000
appId: com.bitcoin.mwallet
released: 2017-06-19
updated: 2021-08-19
version: "7.1.3"
stars: 4.4
ratings: 30114
reviews: 9346
size: 56M
website: https://www.bitcoin.com
repository: https://github.com/Bitcoin-com/Wallet
issue: https://github.com/Bitcoin-com/Wallet/issues/39
icon: com.bitcoin.mwallet.png
bugbounty: 
verdict: nosource
date: 2021-05-20
signer: 
reviewArchive:
- date: 2019-12-20
  version: "5.13.3"
  appHash: 
  gitRevision: fc6cffcbb02fa7d441528eec517e5d61050cf26c
  verdict: nonverifiable

providerTwitter: bitcoincom
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: btc

redirect_from:
  - /com.bitcoin.mwallet/
  - /posts/com.bitcoin.mwallet/
---


According to
[the words of its owner on 2020-04-12](https://www.reddit.com/r/btc/comments/g04ece/bitcoincom_wallet_app_is_still_closed_source/fn7rlvy/)
this wallet is closed source until further notice. There was no indication of a
change by 2020-05-20. We assume it is still
supposed to be non-custodial but without source code, this is **not verifiable**.

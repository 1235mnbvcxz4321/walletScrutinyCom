---
wsId: WalletsAfrica
title: "Wallets Africa - Seamless Digital Transactions"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: ng.wallet.app
released: 2017-06-22
updated: 2021-08-09
version: "2.479"
stars: 3.6
ratings: 1683
reviews: 1350
size: 17M
website: 
repository: 
issue: 
icon: ng.wallet.app.png
bugbounty: 
verdict: custodial
date: 2021-04-23
signer: 
reviewArchive:


providerTwitter: walletsafrica
providerLinkedIn: 
providerFacebook: walletsafrica
providerReddit: 

redirect_from:

---


Wallets Africa is quite a broad product, the lack of source code makes it
impossible to verify this app and there are no statements on their website as to
management of private keys.

Our verdict: This “wallet” is probably custodial and therefore is
**not verifiable**.

---
wsId: 
title: "Dharma — Your Ethereum Wallet"
altTitle: 
authors:

users: 10000
appId: io.dharma.Dharma
released: 2020-01-15
updated: 2021-06-17
version: "1.0.23"
stars: 2.8
ratings: 270
reviews: 178
size: 60M
website: https://www.dharma.io
repository: 
issue: 
icon: io.dharma.Dharma.png
bugbounty: 
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: bitplaza
title: "Bitplaza - Shopping With Bitcoin"
altTitle: 
authors:

users: 10000
appId: com.Bitplaza.android
released: 2018-04-21
updated: 2018-11-14
version: "4.0"
stars: 4.4
ratings: 111
reviews: 56
size: 1.7M
website: https://www.bitplazashopping.com
repository: 
issue: 
icon: com.Bitplaza.android.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-10
  version: "4.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nowallet

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is a market place with no integrated wallet.

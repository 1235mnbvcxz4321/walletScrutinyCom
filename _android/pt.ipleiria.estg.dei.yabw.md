---
wsId: 
title: "Yet Another Bitcoin Wallet"
altTitle: 
authors:

users: 500
appId: pt.ipleiria.estg.dei.yabw
released: 2018-05-02
updated: 2018-06-25
version: "1.2.4"
stars: 4.4
ratings: 5
reviews: 2
size: 3.0M
website: 
repository: 
issue: 
icon: pt.ipleiria.estg.dei.yabw.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.2.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



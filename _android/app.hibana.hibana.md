---
wsId: 
title: "Hibana Wallet"
altTitle: 
authors:
- kiwilamb
users: 50
appId: app.hibana.hibana
released: 2018-12-11
updated: 2018-12-11
version: "0.1"
stars: 0.0
ratings: 
reviews: 
size: 6.5M
website: 
repository: 
issue: 
icon: app.hibana.hibana.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-15
  version: "0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is a lightning wallet.

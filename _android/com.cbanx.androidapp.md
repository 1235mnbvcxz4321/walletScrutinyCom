---
wsId: 
title: "CBANX - Cryptocurrency Exchange"
altTitle: 
authors:

users: 100
appId: com.cbanx.androidapp
released: 2018-08-25
updated: 2019-06-25
version: "2.1.20"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: 
repository: 
issue: 
icon: com.cbanx.androidapp.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "2.1.20"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Lndroid.Messenger Testnet"
altTitle: 
authors:

users: 10
appId: org.lndroid.messenger.testnet
released: 2020-03-23
updated: 2020-03-23
version: "0.1.3"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: org.lndroid.messenger.testnet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "0.1.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



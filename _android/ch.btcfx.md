---
wsId: 
title: "BTCfx - Bitcoin Trading Client"
altTitle: 
authors:

users: 10000
appId: ch.btcfx
released: 2013-07-13
updated: 2019-12-20
version: "1.3.0.1"
stars: 3.5
ratings: 562
reviews: 304
size: 5.7M
website: 
repository: 
issue: 
icon: ch.btcfx.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.3.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



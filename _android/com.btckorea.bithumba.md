---
wsId: 
title: "빗썸 트레이더"
altTitle: 
authors:
- leo
users: 100000
appId: com.btckorea.bithumba
released: 2020-03-31
updated: 2021-04-20
version: "1.1.1"
stars: 3.2
ratings: 598
reviews: 311
size: 19M
website: https://www.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumba.png
bugbounty: 
verdict: defunct
date: 2021-08-04
signer: 
reviewArchive:
- date: 2021-02-19
  version: "1.1.1"
  appHash: 
  gitRevision: 8770b9bdf24645be6c837baa253b07d13a2b2ce8
  verdict: custodial

providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-04**: Since 2021-07-22 the app is not on Play Store anymore.
We don't know any details but also noticed that
**"Bithumb Global-Bitcoin, crypto exchange  & wallet"** renamed to
{% include walletLink.html wallet='android/global.bithumb.android' verdict='true' %}
which might or might not have to do with {{ page.title }} which has the appId
`com.btckorea.bithumba`.

# Old Analysis

This app appears to be the korean version of
{% include walletLink.html wallet='android/com.btckorea.bithumb' %}. Google Translate doesn't reveal any
substantial difference. We conclude it is a custodial offering and **not verifiable**.

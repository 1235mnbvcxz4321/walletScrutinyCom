---
wsId: 
title: "Zloadr - Free Airdrops, NFT Drops & NFT Calendar"
altTitle: 
authors:

users: 10000
appId: com.phonegap.Zloadr
released: 2019-06-17
updated: 2019-07-15
version: "1.1.0"
stars: 4.4
ratings: 991
reviews: 907
size: 12M
website: 
repository: 
issue: 
icon: com.phonegap.Zloadr.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "BitWay Bitcoin Crypto Wallet"
altTitle: 
authors:

users: 100
appId: com.bit.btcwallet
released: 2019-11-17
updated: 2019-11-25
version: "0.2"
stars: 0.0
ratings: 
reviews: 
size: 29M
website: 
repository: 
issue: 
icon: com.bit.btcwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "0.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



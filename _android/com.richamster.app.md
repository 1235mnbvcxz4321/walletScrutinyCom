---
wsId: 
title: "Richamster - Cryptocurrency trading mobile app"
altTitle: 
authors:

users: 5000
appId: com.richamster.app
released: 2018-11-24
updated: 2020-08-08
version: "2.3"
stars: 4.3
ratings: 81
reviews: 47
size: 15M
website: 
repository: 
issue: 
icon: com.richamster.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "2.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



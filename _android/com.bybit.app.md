---
wsId: bybit
title: "Bybit: Crypto Trading Platform"
altTitle: 
authors:
- leo
users: 1000000
appId: com.bybit.app
released: 2019-10-31
updated: 2021-08-11
version: "3.1.3"
stars: 4.9
ratings: 35584
reviews: 2804
size: 51M
website: https://www.bybit.com
repository: 
issue: 
icon: com.bybit.app.png
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: Bybit_Official
providerLinkedIn: bybitexchange
providerFacebook: Bybit
providerReddit: Bybit

redirect_from:

---


> "Bybit is the World's fastest-growing and one of the largest crypto
  derivatives exchanges to trade Bitcoin and crypto.

and as such, funds are in cold storage with them:

> YOUR SAFETY IS OUR PRIORITY<br>
  We safeguard your cryptocurrencies with a multi-signature cold-wallet
  solution. Your funds are 100% protected from the prying eyes. All traders'
  deposited assets are segregated from Bybit's operating budget to increase our
  financial accountability and transparency.

As a custodial app it is **not verifiable**.

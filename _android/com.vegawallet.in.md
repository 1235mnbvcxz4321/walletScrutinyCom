---
wsId: 
title: "VegaWallet"
altTitle: 
authors:

users: 1000
appId: com.vegawallet.in
released: 2018-12-19
updated: 2020-07-02
version: "3.2.3"
stars: 4.7
ratings: 23
reviews: 19
size: 41M
website: 
repository: 
issue: 
icon: com.vegawallet.in.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "3.2.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Paymint - Secure Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100
appId: com.paymintlabs.paymint
released: 2020-06-29
updated: 2020-10-05
version: "1.2.2"
stars: 4.1
ratings: 11
reviews: 7
size: 25M
website: 
repository: https://github.com/Paymint-Labs/Paymint
issue: 
icon: com.paymintlabs.paymint.png
bugbounty: 
verdict: fewusers
date: 2020-10-03
signer: 
reviewArchive:


providerTwitter: paymint_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.paymintlabs.paymint/
---



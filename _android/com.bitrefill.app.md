---
wsId: bitrefill
title: "Bitrefill - Use Bitcoin to buy Gift Cards & Topups"
altTitle: 
authors:
- leo
users: 100000
appId: com.bitrefill.app
released: 2018-04-10
updated: 2019-09-28
version: "1.28.9"
stars: 3.9
ratings: 655
reviews: 388
size: 5.2M
website: https://www.bitrefill.com
repository: 
issue: 
icon: com.bitrefill.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2019-12-25
  version: "1.28.9"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: bitrefill
providerLinkedIn: 
providerFacebook: bitrefill
providerReddit: Bitrefill

redirect_from:
  - /com.bitrefill.app/
  - /posts/com.bitrefill.app/
---


While the primary purpose of this app is to buy stuff with Bitcoin and it appears
to be possible to use the app without putting money into it, the app also can
hold a balance, so it appears to be a wallet. At least we take that from the
screenshots.

As the description has no claims to the contrary and we can't find anything about
the app on their website except for a link to the Playstore, we have to assume
it is a custodial service.

Our verdict: The app is **not verifiable**.

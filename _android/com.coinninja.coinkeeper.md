---
wsId: 
title: "DropBit: Bitcoin Wallet"
altTitle: 
authors:

users: 10000
appId: com.coinninja.coinkeeper
released: 2018-08-01
updated: 2020-01-28
version: "3.2.7"
stars: 2.3
ratings: 205
reviews: 149
size: 33M
website: https://dropbit.app
repository: https://github.com/coinninjadev/dropbit-android
issue: https://github.com/coinninjadev/dropbit-android/issues/2
icon: com.coinninja.coinkeeper.png
bugbounty:
verdict: defunct
date: 2021-07-21
signer: 
reviewArchive:
- date: 2021-06-18
  version: "3.2.7"
  appHash: 
  gitRevision: 
  verdict: wip
- date: 2021-02-28
  version: "3.2.7"
  appHash: 
  gitRevision: 
  verdict: defunct
- date: 2019-11-24
  version: "3.2.7"
  appHash:  
  gitRevision: a920a50eb4b0f8638e7cedb013a135f9c0a7b0fc
  verdict: nonverifiable

providerTwitter: dropbitapp
providerLinkedIn: 
providerFacebook: DropBit-2094204254174419
providerReddit: DropBit

redirect_from:
  - /dropbit/
  - /com.coinninja.coinkeeper/
  - /posts/2019/11/dropbit/
  - /posts/com.coinninja.coinkeeper/
---


**Update 2021-07-21**: Our issues on the repository got no reply, the server's
certificate expired last year and there is no update from the provider in years.
Although available on the Play Store, this app is no more and probably for good.
People have been loosing money and we strongly recommend to not use this app.

{{ page.title }} was a custodial lightning wallet and appears to still be a non-custodial
on-chain bitcoin wallet. As all funds on the lightning side are locked up with
no comment from the provider
([who apparently is in jail](https://www.reddit.com/r/DropBit/comments/fmgoad/sats_stuck_in_dropbit_lightning_side/)),
we mark this wallet as defunct for now.

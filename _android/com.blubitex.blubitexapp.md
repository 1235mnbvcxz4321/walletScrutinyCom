---
wsId: 
title: "Blubitex"
altTitle: 
authors:

users: 50000
appId: com.blubitex.blubitexapp
released: 2020-10-03
updated: 2021-08-02
version: "2.1.335"
stars: 3.5
ratings: 2663
reviews: 1484
size: 80M
website: 
repository: 
issue: 
icon: com.blubitex.blubitexapp.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



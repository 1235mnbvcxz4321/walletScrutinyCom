---
wsId: 
title: "LUNA - Blockchain Wallet"
altTitle: 
authors:

users: 5000
appId: com.btcwallet
released: 2019-08-29
updated: 2019-10-23
version: "8.0.8"
stars: 3.8
ratings: 32
reviews: 21
size: 4.7M
website: 
repository: 
issue: 
icon: com.btcwallet.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "8.0.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Crypto Wallet: Store and Trade Coins"
altTitle: 
authors:

users: 500
appId: com.xufagroup.cryptowallet
released: 2018-07-05
updated: 2020-06-03
version: "1.2"
stars: 1.8
ratings: 5
reviews: 4
size: Varies with device
website: 
repository: 
issue: 
icon: com.xufagroup.cryptowallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "DigiCafe - Mobile DigiByte Point of Sale"
altTitle: 
authors:

users: 1000
appId: com.dgbCafe.dgbCafe
released: 2018-11-16
updated: 2021-05-28
version: "2.1"
stars: 4.8
ratings: 28
reviews: 16
size: 19M
website: 
repository: 
issue: 
icon: com.dgbCafe.dgbCafe.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



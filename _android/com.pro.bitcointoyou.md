---
wsId: bitcointoyou
title: "Bitcointoyou Pro"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.pro.bitcointoyou
released: 2019-10-31
updated: 2021-08-01
version: "0.0.183"
stars: 3.8
ratings: 1122
reviews: 689
size: 56M
website: https://www.bitcointoyou.com
repository: 
issue: 
icon: com.pro.bitcointoyou.jpg
bugbounty: 
verdict: custodial
date: 2021-04-27
signer: 
reviewArchive:


providerTwitter: bitcointoyou
providerLinkedIn: bitcointoyou
providerFacebook: Bitcointoyou
providerReddit: 

redirect_from:

---


The [Bitcointoyou website](https://www.bitcointoyou.com) has no statement regarding the management of private keys.
However being an exchange, it is highly likely that this is a custodial service with funds being in control of the provider.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.



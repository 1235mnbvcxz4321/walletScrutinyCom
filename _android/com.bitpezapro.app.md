---
wsId: 
title: "Bekonta - Fast & Easy Crypto Exchange"
altTitle: 
authors:

users: 10000
appId: com.bitpezapro.app
released: 2020-02-09
updated: 2020-07-09
version: "1.0.1"
stars: 4.4
ratings: 101
reviews: 77
size: 2.3M
website: 
repository: 
issue: 
icon: com.bitpezapro.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Foxbit Exchange"
altTitle: 
authors:

users: 100000
appId: br.com.foxbit.foxbitandroid
released: 2019-01-28
updated: 2021-08-18
version: "2.3.0"
stars: 2.9
ratings: 4245
reviews: 3114
size: 13M
website: 
repository: 
issue: 
icon: br.com.foxbit.foxbitandroid.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



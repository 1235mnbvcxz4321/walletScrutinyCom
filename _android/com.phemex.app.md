---
wsId: phemex
title: "Phemex: Top Bitcoin Exchange App, Crypto & 0 Fees"
altTitle: 
authors:
- kiwilamb
users: 500000
appId: com.phemex.app
released: 2020-02-19
updated: 2021-07-28
version: "1.4.1"
stars: 4.8
ratings: 10672
reviews: 2236
size: 47M
website: https://phemex.com
repository: 
issue: 
icon: com.phemex.app.png
bugbounty: 
verdict: custodial
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: phemex_official
providerLinkedIn: phemex
providerFacebook: Phemex.official
providerReddit: 

redirect_from:

---


The Phemex mobile app claims to hold funds in cold storage...

> All assets are 100% stored in cold wallets. Each withdrawal is thoroughly monitored and requires two-person approval with offline signatures.

leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

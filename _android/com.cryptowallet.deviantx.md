---
wsId: 
title: "DeviantX Wallet : Bitcoin Ethereum Altcoins Tokens"
altTitle: 
authors:

users: 1000
appId: com.cryptowallet.deviantx
released: 2018-11-12
updated: 2019-08-01
version: "2.6"
stars: 3.6
ratings: 29
reviews: 15
size: 11M
website: 
repository: 
issue: 
icon: com.cryptowallet.deviantx.jpg
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "2.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



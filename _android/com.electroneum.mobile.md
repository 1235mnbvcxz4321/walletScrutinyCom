---
wsId: 
title: "Electroneum"
altTitle: 
authors:

users: 1000000
appId: com.electroneum.mobile
released: 2017-12-13
updated: 2021-07-30
version: "5.1.2"
stars: 2.7
ratings: 58217
reviews: 37337
size: 19M
website: 
repository: 
issue: 
icon: com.electroneum.mobile.png
bugbounty: 
verdict: nobtc
date: 2020-06-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.electroneum.mobile/
  - /posts/com.electroneum.mobile/
---


This app does not support storing BTC.

*(Besides that, we couldn't find any source code or even a claim of it being
non-custodial.)*

---
wsId: 
title: "WhiteBIT – buy & sell bitcoin. Crypto wallet"
altTitle: 
authors:

users: 100000
appId: com.whitebit.android
released: 2019-06-07
updated: 2021-08-12
version: "2.1.8"
stars: 4.4
ratings: 1635
reviews: 908
size: 13M
website: 
repository: 
issue: 
icon: com.whitebit.android.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



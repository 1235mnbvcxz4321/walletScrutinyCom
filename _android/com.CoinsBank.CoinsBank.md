---
wsId: 
title: "CoinsBank Mobile Wallet"
altTitle: 
authors:

users: 50000
appId: com.CoinsBank.CoinsBank
released: 2016-04-03
updated: 2020-12-30
version: "3.5.4"
stars: 3.6
ratings: 258
reviews: 135
size: 18M
website: 
repository: 
issue: 
icon: com.CoinsBank.CoinsBank.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



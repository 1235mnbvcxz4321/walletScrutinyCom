---
wsId: 
title: "Hawala—Blockchain Wallet (BTC, ETH, LTC, BCH...)"
altTitle: 
authors:

users: 500
appId: com.cyberblock.cyberblockmobile
released: 2018-10-22
updated: 2018-10-22
version: "2.0"
stars: 1.8
ratings: 5
reviews: 2
size: 17M
website: 
repository: 
issue: 
icon: com.cyberblock.cyberblockmobile.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "2.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



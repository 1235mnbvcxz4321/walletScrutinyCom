---
wsId: enjin
title: "Enjin: Bitcoin, Ethereum, NFT Crypto Wallet"
altTitle: 
authors:
- leo
users: 500000
appId: com.enjin.mobile.wallet
released: 2018-01-01
updated: 2021-08-04
version: "1.15.1-r"
stars: 4.6
ratings: 9277
reviews: 5323
size: 32M
website: https://enjin.io/products/wallet
repository: 
issue: 
icon: com.enjin.mobile.wallet.png
bugbounty: 
verdict: nosource
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: enjin
providerLinkedIn: enjin
providerFacebook: enjinsocial
providerReddit: EnjinCoin

redirect_from:

---


Enjin: Blockchain & Crypto Wallet
description starts promising:

> "Your private keys are your own"

They advertise advanced securing techniques among which are:

> An extensive independent security audit and penetration test found no security
  issues.

(You can read the report
[here](https://cdn.enjin.io/files/pdfs/enjin-wallet-security-audit.pdf))

But source code isn't available on [their website](https://github.com/enjin).
So the user is left with only one choice: trust.

Our verdict: **not verifiable**.


Other observations
------------------

> in-app browsing:
> "ENJOY SEAMLESS BROWSING
> Interact with any DApp with the single click of a button—without leaving the
> safety of your crypto wallet."

looks very advanced, the list of features is tremendous. also an old player:

> ABOUT ENJIN<br>
  Founded in 2009 and based in Singapore, Enjin offers an ecosystem of
  integrated, user-first blockchain products that enable anyone to easily
  manage, explore, distribute, and integrate blockchain-based assets.

on their main page, they advertise advanced securing techniques amongst which are:

> * Custom ARM instructions ensure that sensitive data is instantly deleted from
    your phone's memory.
> * Enjin Keyboard. Built from scratch to protect you from any form of data
    sniffing or keyloggers.

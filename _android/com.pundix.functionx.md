---
wsId: 
title: "f(x)Wallet by Pundi X Labs"
altTitle: 
authors:

users: 10000
appId: com.pundix.functionx
released: 2021-03-25
updated: 2021-08-09
version: "1.7.2"
stars: 4.0
ratings: 207
reviews: 105
size: 19M
website: 
repository: 
issue: 
icon: com.pundix.functionx.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



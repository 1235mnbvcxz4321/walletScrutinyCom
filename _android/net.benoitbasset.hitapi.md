---
wsId: 
title: "HitAPI"
altTitle: 
authors:

users: 10000
appId: net.benoitbasset.hitapi
released: 2018-02-07
updated: 2019-05-07
version: "1.3.0"
stars: 4.0
ratings: 142
reviews: 60
size: 4.7M
website: 
repository: 
issue: 
icon: net.benoitbasset.hitapi.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.3.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



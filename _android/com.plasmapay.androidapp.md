---
wsId: 
title: "PlasmaPay - cryptocurrency wallet and DeFi app"
altTitle: 
authors:

users: 1000
appId: com.plasmapay.androidapp
released: 2019-07-31
updated: 2021-07-14
version: "2.0.2"
stars: 3.0
ratings: 28
reviews: 16
size: 151M
website: 
repository: 
issue: 
icon: com.plasmapay.androidapp.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



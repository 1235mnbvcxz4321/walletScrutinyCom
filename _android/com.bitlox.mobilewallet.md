---
wsId: 
title: "BITLOX Crypto Wallet"
altTitle: 
authors:

users: 100
appId: com.bitlox.mobilewallet
released: 2018-12-16
updated: 2019-09-18
version: "4.0.4"
stars: 0.0
ratings: 
reviews: 
size: 8.7M
website: 
repository: 
issue: 
icon: com.bitlox.mobilewallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "4.0.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



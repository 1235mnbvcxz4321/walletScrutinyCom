---
wsId: etoro
title: "eToro - Smart Crypto Trading Made Easy"
altTitle: 
authors:
- leo
users: 10000000
appId: com.etoro.openbook
released: 2013-11-05
updated: 2021-08-20
version: "340.0.0"
stars: 4.2
ratings: 91772
reviews: 35152
size: 57M
website: https://www.etoro.com
repository: 
issue: 
icon: com.etoro.openbook.png
bugbounty: 
verdict: custodial
date: 2021-04-02
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: etoro
providerFacebook: eToro
providerReddit: 

redirect_from:

---


Etoro is used to speculate on assets more than to actually transfer them but in
the case of Bitcoin, according to
[the Help Center](https://www.etoro.com/customer-service/help/1422157482/can-i-withdraw-my-cryptocurrencies-from-the-platform/)
you can actually send Bitcoins from this app ... if you are in the right
jurisdiction ...
[further restrictions apply](https://etoro.nanorep.co/widget/widget.html?kb=156763&account=etoro#onloadquestionid=1306615492) ...

So all in all this could pass as a custodial app.

As a custodial app it is **not verifiable**.

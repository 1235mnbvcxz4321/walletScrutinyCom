---
wsId: 
title: "Alvexo: Online CFD Trading App on Forex & Stocks"
altTitle: 
authors:

users: 50000
appId: com.alvexo
released: 2018-07-13
updated: 2021-08-03
version: "3.0.46"
stars: 4.2
ratings: 583
reviews: 323
size: 17M
website: 
repository: 
issue: 
icon: com.alvexo.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: PumaPay
title: "PumaPay Blockchain wallet - Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 10000
appId: com.pumapay.pumawallet
released: 2018-05-07
updated: 2021-07-30
version: "3.9.9"
stars: 3.8
ratings: 348
reviews: 222
size: 64M
website: https://pumapay.io
repository: 
issue: 
icon: com.pumapay.pumawallet.png
bugbounty: 
verdict: nosource
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: PumaPay
providerLinkedIn: decentralized-vision
providerFacebook: PumaPayOfficial
providerReddit: 

redirect_from:
  - /com.pumapay.pumawallet/
  - /posts/com.pumapay.pumawallet/
---


This app has very little information on their website but in the Google Play
description we read:

> Please note: It is your sole responsibility to keep the 12-word seed phrase of
  the wallet in a safe place for you to be able to access it in the future. In
  this case, you will be required to restore the Private Key.

So this sounds like they claim to be non-custodial but we cannot see any source
code which makes the app **not verifiable**.

---
wsId: Vauld
title: "Vauld -  Earn, Borrow & Trade With Crypto"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.bankofhodlers.mobile
released: 2020-04-30
updated: 2021-07-24
version: "2.3.2"
stars: 3.7
ratings: 266
reviews: 138
size: 27M
website: https://www.vauld.com/
repository: 
issue: 
icon: com.bankofhodlers.mobile.png
bugbounty: 
verdict: custodial
date: 2021-05-08
signer: 
reviewArchive:


providerTwitter: Vauld_
providerLinkedIn: vauld
providerFacebook: VauldOfficial
providerReddit: BankofHodlers

redirect_from:

---


The Vauld website Help Center had an article "Security at Vauld" which covers a number of security risk.<br>
A statement of the management of the users "funds" makes it pretty clear the wallets private keys are in control of the provider.

> Our funds are managed through a multi signature system with the signatories being our co-founders.

Our verdict: This wallet is custodial and therefore **not verifiable**.


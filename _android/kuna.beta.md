---
wsId: 
title: "Kuna.io — Easy way to buy&sell BTC ETH USDT"
altTitle: 
authors:

users: 50000
appId: kuna.beta
released: 2019-07-22
updated: 2021-08-19
version: "1.3.21"
stars: 3.7
ratings: 586
reviews: 327
size: 88M
website: 
repository: 
issue: 
icon: kuna.beta.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



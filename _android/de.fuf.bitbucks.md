---
wsId: BitcoinWalletBitBucks
title: "Bitcoin Wallet BitBucks"
altTitle: 
authors:
- leo
users: 5000
appId: de.fuf.bitbucks
released: 2019-09-03
updated: 2021-03-18
version: "1.4.1"
stars: 4.1
ratings: 43
reviews: 17
size: 16M
website: https://www.bitbucks.io
repository: 
issue: 
icon: de.fuf.bitbucks.png
bugbounty: 
verdict: custodial
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: bit_bucks
providerLinkedIn: 
providerFacebook: bitbucks.io
providerReddit: 

redirect_from:
  - /de.fuf.bitbucks/
  - /posts/de.fuf.bitbucks/
---


According to their website:

> **Pay safely and securely**<br>
  Your Bitcoin is multi-signature protected and will be securely stored in the
  safest wallets. Even if you lose your mobile phone, you will not lose your
  credit.

This is a custodial app and thus **not verifiable**

---
wsId: 
title: "NEO Wallet. Send & Receive the coin－Freewallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: neo.org.freewallet.app
released: 2017-11-29
updated: 2020-04-21
version: "2.5.2"
stars: 4.1
ratings: 240
reviews: 179
size: 7.4M
website: https://freewallet.org/
repository: 
issue: 
icon: neo.org.freewallet.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-02
  version: "2.5.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nobtc

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This wallet is built for the management of an alt coin, NEO token.

Our verdict: This is not a wallet for holding bitcoins.


---
wsId: 
title: "Banko Wallet - Bitcoin, ETH, TRON, Crypto Wallet"
altTitle: 
authors:

users: 1000
appId: app.bankowallet.android
released: 2019-07-22
updated: 2020-06-29
version: "1.3.17851"
stars: 1.8
ratings: 16
reviews: 13
size: 22M
website: 
repository: 
issue: 
icon: app.bankowallet.android.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.3.17851"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "GBBIT Bitcoin Wallet India"
altTitle: 
authors:

users: 5000
appId: gbbit.app.wallet
released: 2017-12-11
updated: 2020-05-07
version: "10.61"
stars: 3.8
ratings: 92
reviews: 63
size: 5.6M
website: 
repository: 
issue: 
icon: gbbit.app.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "10.61"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



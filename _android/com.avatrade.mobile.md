---
wsId: 
title: "AvaTrade GO Trading: Stocks, Bitcoin, CFDs & Forex"
altTitle: 
authors:
- danny
users: 1000000
appId: com.avatrade.mobile
released: 2017-05-23
updated: 2021-08-10
version: "94.2.0"
stars: 4.7
ratings: 5852
reviews: 4119
size: 19M
website: 
repository: 
issue: 
icon: com.avatrade.mobile.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: AvaTrade
providerLinkedIn: AvaTrade
providerFacebook: AvaTrade
providerReddit: 

redirect_from:

---


>AvaTradeGO enables users to trade, monitor their accounts, engage in social trading and follow top traders from all over the globe.

It's a trading platform and most likely custodial.

From [their website:](https://www.avatrade.com/forex/cryptocurrencies/bitcoin)

> As a CFD brokerage firm, we do not provide crypto wallets to store your Bitcoin. We simply provide access to intuitive platforms for you to trade real-time price changes of Bitcoin.

It clearly is not a wallet. Sounds like a no send/receive. 

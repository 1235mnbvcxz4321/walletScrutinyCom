---
wsId: 
title: "Bitcoiva-Bitcoin & Cryptocurrency Exchange India"
altTitle: 
authors:
- leo
users: 10000
appId: com.application.bitcoiva
released: 2020-11-02
updated: 2021-08-12
version: "1.17"
stars: 3.6
ratings: 271
reviews: 133
size: 7.2M
website: https://bitcoiva.com
repository: 
issue: 
icon: com.application.bitcoiva.png
bugbounty: 
verdict: custodial
date: 2021-07-29
signer: 
reviewArchive:


providerTwitter: bitcoiva
providerLinkedIn: 
providerFacebook: Bitcoiva-105126591217169
providerReddit: 

redirect_from:

---


> Buy, store and sell cryptocurrencies like Bitcoin, Ethereum,

sounds like a BTC wallet.

But where are the coins stored? With the user or on their servers?

> We use industry best practices to make BITCOIVA the most secure cryptocurrency exchange and wallet. We invest in regular security audits to ensure a highly secured trading platform for our users and take all measures to keep the assets stored with us secure at all times.  

is much talk about security but nothing concrete.

On Play Store there is no link to a website, neither but their privacy policy is
on [this page](https://bitcoiva.com/) which has a Play Store logo but no link so
we are not sure this is even the official app.

As Bitcoiva is an exchange and nothing points to the contrary we assume it is
a custodial service and thus **not verifiable**.



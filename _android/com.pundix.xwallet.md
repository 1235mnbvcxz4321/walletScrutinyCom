---
wsId: xwallet
title: "XWallet"
altTitle: 
authors:
- kiwilamb
- leo
users: 100000
appId: com.pundix.xwallet
released: 2018-10-22
updated: 2020-11-23
version: "2.8.2"
stars: 3.0
ratings: 4965
reviews: 2881
size: 62M
website: https://pundix.com
repository: 
issue: 
icon: com.pundix.xwallet.png
bugbounty: 
verdict: defunct
date: 2021-06-16
signer: 
reviewArchive:
- date: 2021-04-29
  version: "2.8.2"
  appHash: 
  gitRevision: 0fcd9076800af0e458a5c75034c15ef0c6ddda58
  verdict: custodial

providerTwitter: PundiXLabs
providerLinkedIn: pundix
providerFacebook: pundixlabs
providerReddit: 

redirect_from:

---


**Update 2021-06-16**: It's been a few days this app is not on the App store. If
it should return, please open an issue on our issue tracker!

Searching the Pundix [support FAQ](https://support.pundix.com/) we find an FAQ that answers the private key management question.

> **Will I have a private key when setting up an account in XWallet app?**<br>
  No. Your email address and mobile number are required when setting up an XWallet app account.

The wallet does not provide the user access to the private keys.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.


---
wsId: 
title: "Multi Exchange Crypto Trading Terminal"
altTitle: 
authors:

users: 10
appId: org.nativescript.BTrex
released: 2018-04-26
updated: 2018-06-03
version: "1.0.6.2"
stars: 0.0
ratings: 
reviews: 
size: 21M
website: 
repository: 
issue: 
icon: org.nativescript.BTrex.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.6.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



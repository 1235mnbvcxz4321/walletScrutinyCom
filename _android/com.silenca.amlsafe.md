---
wsId: amlsafe
title: "AML Safe Wallet — Buy & Sell Bitcoin with AML"
altTitle: 
authors:
- leo
users: 1000
appId: com.silenca.amlsafe
released: 2020-10-26
updated: 2021-08-19
version: "1.30.49"
stars: 4.5
ratings: 232
reviews: 166
size: 57M
website: https://amlsafe.io
repository: 
issue: 
icon: com.silenca.amlsafe.jpg
bugbounty: 
verdict: custodial
date: 2021-04-11
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> AMLSafe is a cutting edge crypto wallet with fiat pairs and an instant
  Anti-Money Laundering (AML) checking. Our wallet gives you the ability to buy
  and sell many cryptocurrencies with fiat.

Seriously?

It is non-custodial! And also not for terrorists.

> Our non-custodial crypto wallet helps ensure that your crypto assets are not
  related to money laundering or terrorist financing.

But ... is it reall non-custodial? On the website we read:

> **Security**<br>
  Your private key is stored on-premise and secured with a multilevel security
  system.

Their [documentation](https://amlsafe.io/en/documentation/) is "under
construction".

With the conflicting claims about where the keys are stored, we have to assume
they retain access to the users' keys. Either way this app is **not verifiable**.

---
wsId: 
title: "ProBit Global: Buy & Sell Bitcoin. Crypto Exchange"
altTitle: 
authors:
- leo
users: 500000
appId: com.probit.app.android2.release.global
released: 2019-06-19
updated: 2021-08-17
version: "1.34.3"
stars: 3.3
ratings: 5059
reviews: 3090
size: 18M
website: https://www.probit.com
repository: 
issue: 
icon: com.probit.app.android2.release.global.png
bugbounty: 
verdict: custodial
date: 2020-11-28
signer: 
reviewArchive:


providerTwitter: ProBit_Exchange
providerLinkedIn: probit-exchange
providerFacebook: probitexchange
providerReddit: 

redirect_from:
  - /com.probit.app.android2.release.global/
---


Probit appears to also and mainly be an exchange and as we can't find claims to
the contrary, we assume this app is a custodial offering and thus **not verifiable**.

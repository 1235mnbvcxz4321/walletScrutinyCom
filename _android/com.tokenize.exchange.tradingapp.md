---
wsId: 
title: "Tokenize Crypto Trading App - International"
altTitle: 
authors:

users: 1000
appId: com.tokenize.exchange.tradingapp
released: 2019-12-03
updated: 2021-06-28
version: "1.15.0"
stars: 3.0
ratings: 31
reviews: 24
size: 38M
website: 
repository: 
issue: 
icon: com.tokenize.exchange.tradingapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



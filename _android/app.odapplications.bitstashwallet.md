---
wsId: 
title: "BitStash Wallet - Multi Cryptocurrency Wallet"
altTitle: 
authors:

users: 100
appId: app.odapplications.bitstashwallet
released: 2019-11-10
updated: 2019-11-24
version: "0.0.3"
stars: 0.0
ratings: 
reviews: 
size: 15M
website: 
repository: 
issue: 
icon: app.odapplications.bitstashwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-05
  version: "0.0.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



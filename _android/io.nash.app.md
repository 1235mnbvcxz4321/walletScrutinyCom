---
wsId: nash
title: "Nash – Buy crypto at the best rates"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: io.nash.app
released: 2019-08-31
updated: 2021-08-16
version: "7.11"
stars: 4.2
ratings: 630
reviews: 265
size: 124M
website: https://nash.io/
repository: 
issue: 
icon: io.nash.app.png
bugbounty: 
verdict: nosource
date: 2021-04-30
signer: 
reviewArchive:


providerTwitter: nashsocial
providerLinkedIn: nashsocial
providerFacebook: 
providerReddit: 

redirect_from:

---


This statement in the description from the [play store](https://play.google.com/store/apps/details?id=io.nash.app) below is a claim only the user has access to the private keys.

> Nash doesn’t take control of your funds – unlike Coinbase, Kraken or Binance. We’re the only fully-featured exchange where you can trade Bitcoin without giving up your private keys.

With keys in control of the user, we need to find the source code in order to check reproducibility.
However we are unable to locate a public source repository.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

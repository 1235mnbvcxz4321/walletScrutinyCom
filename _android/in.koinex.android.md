---
wsId: 
title: "Koinex - India's largest digital assets exchange"
altTitle: 
authors:

users: 100000
appId: in.koinex.android
released: 2018-02-11
updated: 2019-02-13
version: "0.1.5"
stars: 2.3
ratings: 3686
reviews: 2359
size: 15M
website: 
repository: 
issue: 
icon: in.koinex.android.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "0.1.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



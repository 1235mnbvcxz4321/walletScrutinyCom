---
wsId: 
title: "Paribu | Bitcoin-Kripto Para Alım Satım"
altTitle: 
authors:
- danny
users: 1000000
appId: com.paribu.app
released: 2019-06-14
updated: 2021-05-17
version: "3.3.7"
stars: 4.2
ratings: 46204
reviews: 26297
size: 38M
website: 
repository: 
issue: 
icon: com.paribu.app.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: paribucom
providerLinkedIn: paribu
providerFacebook: paribucom
providerReddit: 

redirect_from:

---


The app's description is written in Turkish, so all the quotes below are from Google Translate.

> In Paribu, user assets are stored in secure cold wallets. You can always perform your transactions safely with up-to-date security measures.

This sounds custodial.

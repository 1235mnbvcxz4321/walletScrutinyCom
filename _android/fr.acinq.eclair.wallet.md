---
wsId: 
title: "Eclair Mobile Testnet"
altTitle: 
authors:

users: 10000
appId: fr.acinq.eclair.wallet
released: 2017-07-20
updated: 2021-05-20
version: "0.4.16"
stars: 4.4
ratings: 218
reviews: 87
size: 29M
website: 
repository: 
issue: 
icon: fr.acinq.eclair.wallet.jpg
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



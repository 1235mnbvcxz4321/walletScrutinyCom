---
wsId: STASISStablecoinWallet
title: "STASIS Stablecoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.stasis.stasiswallet
released: 2018-06-13
updated: 2021-07-19
version: "1.9.19"
stars: 4.3
ratings: 159
reviews: 95
size: 17M
website: https://stasis.net/wallet
repository: https://github.com/stasisnet
issue: 
icon: com.stasis.stasiswallet.png
bugbounty: 
verdict: custodial
date: 2020-05-03
signer: 
reviewArchive:


providerTwitter: stasisnet
providerLinkedIn: stasisnet
providerFacebook: stasisnet
providerReddit: 

redirect_from:
  - /com.stasis.stasiswallet/
  - /posts/com.stasis.stasiswallet/
---


On Google Play and their website there is no mention of being non-custodial and
certainly there is no source code available. Until we hear opposing claims
we consider it a custodial app and therefore **not verifiable**.

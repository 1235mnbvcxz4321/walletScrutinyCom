---
wsId: 
title: "Galaxy Wallet"
altTitle: 
authors:

users: 1000
appId: com.application.galaxywallet
released: 2019-12-09
updated: 2020-05-19
version: "1.2"
stars: 2.9
ratings: 17
reviews: 12
size: 5.9M
website: 
repository: 
issue: 
icon: com.application.galaxywallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Wirex Multicurrency Crypto Wallet"
altTitle: 
authors:
- danny
users: 1000000
appId: com.wirex
released: 2016-03-31
updated: 2021-08-06
version: "3.23.5"
stars: 3.5
ratings: 21700
reviews: 11846
size: 100M
website: https://wirexapp.com/en
repository: 
issue: 
icon: com.wirex.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: wirexapp
providerLinkedIn: wirex-limited
providerFacebook: wirexapp
providerReddit: 

redirect_from:

---


From their app description:

> Download the Wirex app today and discover seamless and secure ways to store, buy, exchange and spend crypto and traditional currency at the best rates available.

From their terms of use:

> When you ask us to buy DPTs on your behalf through the Wirex Service, we will store the DPTs in pooled crypto wallets created and maintained by our third-party wallet custodian or by us.

This makes the app custodial and also not verifiable.

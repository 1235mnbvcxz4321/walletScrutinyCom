---
wsId: 
title: "Bixin - Secure Bitcoin Wallet"
altTitle: 
authors:

users: 5000
appId: com.bixin.bixin_android
released: 2017-03-22
updated: 2021-08-20
version: "4.3.1"
stars: 3.9
ratings: 81
reviews: 27
size: 174M
website: 
repository: 
issue: 
icon: com.bixin.bixin_android.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



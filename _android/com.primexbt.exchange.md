---
wsId: 
title: "PrimeXBT Trade"
altTitle: 
authors:

users: 50000
appId: com.primexbt.exchange
released: 2019-05-08
updated: 2020-07-16
version: "144.5.0-release"
stars: 2.1
ratings: 586
reviews: 444
size: 9.5M
website: 
repository: 
issue: 
icon: com.primexbt.exchange.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "144.5.0-release"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



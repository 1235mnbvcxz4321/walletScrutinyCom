---
wsId: 
title: "Official Guap Coin & Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: org.guapcoin.multiwallet
released: 2020-02-27
updated: 2020-02-27
version: "1.0.0"
stars: 4.3
ratings: 39
reviews: 24
size: 26M
website: 
repository: 
issue: 
icon: org.guapcoin.multiwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



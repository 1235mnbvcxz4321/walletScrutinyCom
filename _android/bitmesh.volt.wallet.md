---
wsId: 
title: "Volt:Bitcoin,ETH&BSV Crypto Wallet,Multisig Wallet"
altTitle: 
authors:

users: 500
appId: bitmesh.volt.wallet
released: 2020-05-15
updated: 2021-08-12
version: "2.0.9"
stars: 4.5
ratings: 41
reviews: 27
size: 23M
website: 
repository: 
issue: 
icon: bitmesh.volt.wallet.png
bugbounty: 
verdict: fewusers
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



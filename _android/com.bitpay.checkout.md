---
wsId: 
title: "BitPay Checkout"
altTitle: 
authors:

users: 50000
appId: com.bitpay.checkout
released: 2014-11-03
updated: 2020-08-11
version: "1.2.5"
stars: 3.8
ratings: 250
reviews: 95
size: 3.6M
website: 
repository: 
issue: 
icon: com.bitpay.checkout.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.2.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



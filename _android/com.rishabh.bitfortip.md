---
wsId: 
title: "Bitfortip | Now with Tezos support"
altTitle: 
authors:

users: 5000
appId: com.rishabh.bitfortip
released: 2016-12-21
updated: 2021-06-20
version: "2.6"
stars: 4.3
ratings: 73
reviews: 41
size: 3.5M
website: 
repository: 
issue: 
icon: com.rishabh.bitfortip.jpg
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



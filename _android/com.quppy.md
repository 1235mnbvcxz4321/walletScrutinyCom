---
wsId: Quppy
title: "Quppy Wallet - bitcoin, crypto and euro payments"
altTitle: 
authors:
- leo
users: 100000
appId: com.quppy
released: 2018-09-17
updated: 2021-08-18
version: "1.0.57"
stars: 4.9
ratings: 3175
reviews: 1013
size: 15M
website: https://quppy.com
repository: 
issue: 
icon: com.quppy.png
bugbounty: 
verdict: custodial
date: 2020-12-01
signer: 
reviewArchive:


providerTwitter: QuppyPay
providerLinkedIn: quppy
providerFacebook: quppyPay
providerReddit: 

redirect_from:
  - /com.quppy/
---


This provider loses no word on security or where the keys are stored. We assume
it is a custodial offering and therefore **not verifiable**.

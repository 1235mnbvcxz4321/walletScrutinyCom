---
wsId: getchange
title: "Change: Beginner-Friendly Trading & Investment App"
altTitle: 
authors:
- leo
users: 100000
appId: com.getchange.wallet.cordova
released: 2018-06-07
updated: 2021-08-17
version: "10.25.0"
stars: 4.3
ratings: 2317
reviews: 1043
size: 31M
website: https://getchange.com
repository: 
issue: 
icon: com.getchange.wallet.cordova.jpg
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: changefinance
providerLinkedIn: changeinvest
providerFacebook: changeinvest
providerReddit: 

redirect_from:
  - /com.getchange.wallet.cordova/
  - /posts/com.getchange.wallet.cordova/
---


On their Google Play description we find

> • Secure: Funds are protected in multi-signature, cold-storage cryptocurrency
  wallets

which means it is a custodial service and thus **not verifiable**.

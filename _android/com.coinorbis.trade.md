---
wsId: 
title: "Coinorbis Trade - Cryptocurrency Trading Platform"
altTitle: 
authors:

users: 100
appId: com.coinorbis.trade
released: 2018-11-05
updated: 2018-11-22
version: "2.0.0"
stars: 4.8
ratings: 6
reviews: 1
size: 5.2M
website: 
repository: 
issue: 
icon: com.coinorbis.trade.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



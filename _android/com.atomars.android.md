---
wsId: 
title: "Atomars"
altTitle: 
authors:

users: 1000
appId: com.atomars.android
released: 2019-07-01
updated: 2019-07-01
version: "1.1.1"
stars: 2.1
ratings: 36
reviews: 26
size: 960k
website: 
repository: 
issue: 
icon: com.atomars.android.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



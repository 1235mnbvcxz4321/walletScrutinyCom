---
wsId: 
title: "Medooza wallet- Best Crypto wallet for android app"
altTitle: 
authors:

users: 500
appId: io.crypto.wallet.bitcoin.ethereum.medooza.wallet
released: 2019-01-04
updated: 2019-01-04
version: "1.1"
stars: 4.1
ratings: 7
reviews: 6
size: 3.5M
website: 
repository: 
issue: 
icon: io.crypto.wallet.bitcoin.ethereum.medooza.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



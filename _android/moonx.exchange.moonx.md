---
wsId: 
title: "MoonX - Crypto Trading Platform"
altTitle: 
authors:

users: 500
appId: moonx.exchange.moonx
released: 2019-08-09
updated: 2020-07-16
version: "2.0"
stars: 4.9
ratings: 13
reviews: 13
size: 9.6M
website: 
repository: 
issue: 
icon: moonx.exchange.moonx.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "2.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



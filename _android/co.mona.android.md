---
wsId: mona
title: "Crypto.com - Buy Bitcoin Now"
altTitle: 
authors:
- leo
users: 10000000
appId: co.mona.android
released: 2017-08-30
updated: 2021-08-06
version: "3.108.0"
stars: 4.1
ratings: 149969
reviews: 52285
size: 109M
website: https://www.crypto.com
repository: 
issue: 
icon: co.mona.android.png
bugbounty: 
verdict: custodial
date: 2019-12-28
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:
  - /co.mona.android/
  - /crypto.com/
  - /posts/co.mona.android/
---


The description of this app is very much focused on the Visa Card they offer
that you can top up with crypto currencies. This and the talk about earning
interest on your holdings clearly sound like a custodial service.

On their website,

>  **You'll need:**
>
> * An email address
> * A phone number
> * One identification document

also sounds more like opening a bank account than starting to use a non-custodial
wallet.

With high certainty this is not a wallet but a custodial service.

Our verdict: **not verifiable**.

---
wsId: 
title: "Tokenize Crypto Trading App - Malaysia (Beta)"
altTitle: 
authors:

users: 10000
appId: com.tokenize.malaysia.tradingapp
released: 2020-11-26
updated: 2021-06-23
version: "1.15.2"
stars: 2.6
ratings: 129
reviews: 83
size: 45M
website: 
repository: 
issue: 
icon: com.tokenize.malaysia.tradingapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



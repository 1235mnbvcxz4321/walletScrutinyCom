---
wsId: bitrue
title: "Bitrue - Cryptocurrency Wallet & Exchange"
altTitle: 
authors:
- leo
users: 100000
appId: com.bitrue.currency.exchange
released: 2018-07-18
updated: 2021-08-12
version: "4.5.5"
stars: 3.4
ratings: 1860
reviews: 1189
size: 36M
website: https://www.bitrue.com
repository: 
issue: 
icon: com.bitrue.currency.exchange.png
bugbounty: 
verdict: custodial
date: 2020-11-17
signer: 
reviewArchive:


providerTwitter: BitrueOfficial
providerLinkedIn: 
providerFacebook: BitrueOfficial
providerReddit: 

redirect_from:
  - /com.bitrue.currency.exchange/
---


This app is heavily focused on the "exchange" part which is also in its name.
Nowhere in Google Play can we find claims about self-custody but things like

> - Applies the advanced multi-layer clustered system and the hot/cold wallet
  isolation technology to ensure system security.

only make sense for custodial apps. As a custodial app it is **not verifiable**.

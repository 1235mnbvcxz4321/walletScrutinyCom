---
wsId: 
title: "IQ Forex - Trading Binary Option on FX & Crypto"
altTitle: 
authors:

users: 50000
appId: trade.iqforex
released: 2019-09-09
updated: 2019-11-26
version: "1.3.0"
stars: 3.7
ratings: 230
reviews: 111
size: 8.0M
website: 
repository: 
issue: 
icon: trade.iqforex.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.3.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



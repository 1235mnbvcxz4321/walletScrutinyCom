---
wsId: 
title: "WowooWallet"
altTitle: 
authors:

users: 5000
appId: com.wowoonet.wallet
released: 2017-12-01
updated: 2019-03-26
version: "1.4.3"
stars: 3.7
ratings: 21
reviews: 7
size: 21M
website: 
repository: 
issue: 
icon: com.wowoonet.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.4.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



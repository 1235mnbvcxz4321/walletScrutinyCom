---
wsId: zebedee
title: "ZEBEDEE Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: io.zebedee.wallet
released: 2020-10-20
updated: 2021-05-26
version: "11.0.0"
stars: 4.4
ratings: 37
reviews: 22
size: 55M
website: https://zebedee.io
repository: 
issue: 
icon: io.zebedee.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-04-12
signer: 
reviewArchive:


providerTwitter: zebedeeio
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is very gamer focused and does no mention at all security aspects or
who's the custodian to your coins:

> The ZEBEDEE Wallet is basically everything you need to start playing games for
  Bitcoin, participating in Bitcoin-powered esports events or collecting Bitcoin
  tips on your live streams.

It is lightning network focused and apparently the counterpart for an sdk the
company is promoting for Bitcoin integration in games.

For lack of a better source I went on [their discord](https://zeb.gg/zebedeeiodiscord)
and asked, so ... according to JC on Discord, this app is custodial. As such it
is **not verifiable**.

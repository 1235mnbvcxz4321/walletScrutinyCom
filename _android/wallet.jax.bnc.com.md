---
wsId: 
title: "Jaxx Liberty:New Wallet BTC ETH"
altTitle: 
authors:
- emanuel
- leo
users: 100
appId: wallet.jax.bnc.com
released: 2021-07-14
updated: 2021-07-14
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 6.8M
website: 
repository: 
issue: 
icon: wallet.jax.bnc.com.png
bugbounty: 
verdict: fake
date: 2021-07-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app "{{ page.title }}" clearly tries to imitate
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.

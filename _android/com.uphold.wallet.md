---
wsId: UpholdbuyandsellBitcoin
title: "Uphold: buy and sell Bitcoin"
altTitle: 
authors:
- leo
users: 1000000
appId: com.uphold.wallet
released: 2015-12-29
updated: 2021-08-16
version: "4.17.6"
stars: 3.6
ratings: 16166
reviews: 10047
size: 49M
website: https://uphold.com
repository: 
issue: 
icon: com.uphold.wallet.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: UpholdInc
providerLinkedIn: upholdinc
providerFacebook: UpholdInc
providerReddit: 

redirect_from:
  - /com.uphold.wallet/
  - /posts/com.uphold.wallet/
---


This app appears to be an interface to a custodial trading platform. In the
Google Play description we read:

> **Trust Through Transparency**<br>
  Uphold is fully reserved. Unlike banks, we don’t loan out your money. To prove
  it, we publish our holdings in real time.

If they hold your money, you don't. As a custodial service this app is **not
verifiable**.

---
wsId: multi.org.freewallet.app
title: "Freewallet MultiWallet Classic"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: multi.org.freewallet.app
released: 2017-08-10
updated: 2018-10-29
version: "1.0.69"
stars: 3.8
ratings: 2010
reviews: 1028
size: 10M
website: https://freewallet.org/
repository: 
issue: 
icon: multi.org.freewallet.app.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-30
  version: "1.0.69"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: Freewallet_org

redirect_from:

---


The [Free wallet website](https://freewallet.org/) clearly states to the user how the wallet private keys are managed.

> Сustomers’ private keys are kept by Freewallet. Lost private keys or mnemonic phrases don’t mean lost funds. Your account can be retrieved via your email like on any other service.

We do not need to research any further for our verdict.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

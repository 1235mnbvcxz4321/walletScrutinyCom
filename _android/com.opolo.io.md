---
wsId: 
title: "OPOLO Hardware Wallet"
altTitle: 
authors:

users: 500
appId: com.opolo.io
released: 2020-09-01
updated: 2021-06-08
version: "1.3.1"
stars: 5.0
ratings: 6
reviews: 2
size: 18M
website: 
repository: 
issue: 
icon: com.opolo.io.png
bugbounty: 
verdict: fewusers
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



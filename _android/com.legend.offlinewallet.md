---
wsId: 
title: "HyperKey"
altTitle: 
authors:

users: 100
appId: com.legend.offlinewallet
released: 2020-05-09
updated: 2020-07-25
version: "1.0.5"
stars: 0.0
ratings: 
reviews: 
size: 40M
website: 
repository: 
issue: 
icon: com.legend.offlinewallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



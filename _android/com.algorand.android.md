---
wsId: 
title: "Algorand Wallet"
altTitle: 
authors:

users: 100000
appId: com.algorand.android
released: 2019-06-07
updated: 2021-08-20
version: "4.9.3"
stars: 4.8
ratings: 4560
reviews: 1228
size: 37M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: nobtc
date: 2020-12-06
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---



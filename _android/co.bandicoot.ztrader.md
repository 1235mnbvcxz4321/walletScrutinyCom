---
wsId: 
title: "zTrader Altcoin/Bitcoin Trader"
altTitle: 
authors:

users: 100000
appId: co.bandicoot.ztrader
released: 2014-05-04
updated: 2016-08-07
version: "1.2.2"
stars: 3.3
ratings: 1213
reviews: 619
size: 3.2M
website: 
repository: 
issue: 
icon: co.bandicoot.ztrader.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.2.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



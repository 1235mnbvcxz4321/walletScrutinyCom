---
wsId: 
title: "Crypto Wallet & Atomic swap DEX"
altTitle: 
authors:
- emanuel
- leo
users: 500
appId: com.atomicwallet.newapp.safe
released: 2021-06-14
updated: 2021-06-14
version: "1.5.2"
stars: 2.4
ratings: 7
reviews: 6
size: 6.0M
website: 
repository: 
issue: 
icon: com.atomicwallet.newapp.safe.png
bugbounty: 
verdict: defunct
date: 2021-07-30
signer: 
reviewArchive:
- date: 2021-07-24
  version: "1.5.2" 
  appHash: 
  gitRevision: 39dc0c413233485694e168a9ebb31778ad09074c
  verdict: fake

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-07-30**: This app is not on Play Store anymore.

This app "{{ page.title }}" clearly tries to imitate
{% include walletLink.html wallet='android/io.atomicwallet' verdict='true' %}.


---
wsId: 
title: "IQFinex Wallet"
altTitle: 
authors:

users: 500
appId: com.iqfinex
released: 2018-09-10
updated: 2019-01-24
version: "1.0.4"
stars: 4.8
ratings: 270
reviews: 261
size: 6.7M
website: 
repository: 
issue: 
icon: com.iqfinex.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.0.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Multicurrency crypto-wallet: Bitpapa"
altTitle: 
authors:
- leo
users: 50000
appId: com.bitpapa
released: 2020-08-18
updated: 2021-05-31
version: "1.6.7"
stars: 4.4
ratings: 3837
reviews: 3780
size: 42M
website: https://bitpapa.com
repository: 
issue: 
icon: com.bitpapa.png
bugbounty: 
verdict: custodial
date: 2021-04-11
signer: 
reviewArchive:


providerTwitter: bitpapa_com
providerLinkedIn: 
providerFacebook: bitpapacom
providerReddit: 

redirect_from:

---


This app supports Bitcoin:

> Bitpapa app allows you to create a multicurrency cryptowallet for secure
  transactions in Bitcoin, Ethereum, and USDT

You can send and receive:

> You can receive, send, and store cryptocurrencies using a convenient and
  secure cryptowallet within your account, as well as trade securely with other
  people on Bitpapa P2P marketplace

And ... it's custodial:

> Bitpapa has eliminated trading commissions, and internal transfers between
  Bitpapa users are free as well.

Free transactions works if it's updates to their database. The blockchain is not
free ever.

This app is **not verifiable**.

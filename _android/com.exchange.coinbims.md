---
wsId: 
title: "빔스코리아 - 신세대 암호화폐거래소"
altTitle: 
authors:

users: 100
appId: com.exchange.coinbims
released: 2019-08-20
updated: 2019-09-09
version: "1.3"
stars: 0.0
ratings: 
reviews: 
size: 1.9M
website: 
repository: 
issue: 
icon: com.exchange.coinbims.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.3"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



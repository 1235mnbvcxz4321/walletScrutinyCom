---
wsId: 
title: "FIG Vault"
altTitle: 
authors:

users: 1000
appId: online.figcoin.figvault
released: 2020-12-23
updated: 2021-08-10
version: "0.10.0"
stars: 4.4
ratings: 37
reviews: 25
size: 9.9M
website: 
repository: 
issue: 
icon: online.figcoin.figvault.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "ProfitTrading for Bybit - Trade much faster"
altTitle: 
authors:

users: 1000
appId: com.profittrading.forbybit
released: 2020-03-22
updated: 2020-07-24
version: "2.0.0"
stars: 3.5
ratings: 23
reviews: 10
size: 19M
website: 
repository: 
issue: 
icon: com.profittrading.forbybit.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Mahfazah | محفظة بيتكوين"
altTitle: 
authors:

users: 1000
appId: io.mahfazah
released: 2018-09-19
updated: 2018-11-22
version: "1.2.6"
stars: 4.4
ratings: 30
reviews: 14
size: 20M
website: 
repository: 
issue: 
icon: io.mahfazah.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.2.6"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: krcokeypair
title: "KeyWallet Touch - Bitcoin Ethereum Crypto Wallet"
altTitle: 
authors:

users: 10000
appId: kr.co.keypair.keywalletTouch
released: 2018-05-13
updated: 2020-12-14
version: "Varies with device"
stars: 4.3
ratings: 102
reviews: 62
size: Varies with device
website: https://keywalletpro.io
repository: 
issue: 
icon: kr.co.keypair.keywalletTouch.png
bugbounty: 
verdict: nosource
date: 2021-03-06
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The description makes rather vague claims:

> - Fully complied with HD(Hierarchical Deterministic) wallet

which is a standard that makes most sense for self-custodial wallets.

> This application manages crypto wallets for Bitcoin, Ethereum, Ripple, ERC20
  tokens and etc.

Now this might be a language barrier issue but we would hope it is a wallet and
not an application to manage wallets aka "not a wallet"?

Their website appears to be no more and we get forwarded to
[some Korean site](http://html.ugo.kr/servicestop.html) that according to Google
translate reads:

> This is to inform you that the "service period has expired" for this site .

There is another website though: [afinkeywallet.io](https://afinkeywallet.io)

So in the best of cases this is a functioning closed source self-custodial
Bitcoin wallet and thus **not verifiable**.

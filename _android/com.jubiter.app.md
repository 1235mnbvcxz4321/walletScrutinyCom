---
wsId: 
title: "JuBiter Wallet 2.0 - Secure Hardware Crypto Wallet"
altTitle: 
authors:

users: 100
appId: com.jubiter.app
released: 2020-11-19
updated: 2021-07-16
version: "2.5.0"
stars: 2.8
ratings: 5
reviews: 3
size: 40M
website: 
repository: 
issue: 
icon: com.jubiter.app.png
bugbounty: 
verdict: fewusers
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



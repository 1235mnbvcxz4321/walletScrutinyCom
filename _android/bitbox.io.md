---
wsId: 
title: "Bitbox Bitcoin Wallet & Exchange"
altTitle: 
authors:

users: 1000
appId: bitbox.io
released: 2019-05-16
updated: 2019-07-30
version: "1.0.4"
stars: 4.1
ratings: 14
reviews: 6
size: 5.6M
website: 
repository: 
issue: 
icon: bitbox.io.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



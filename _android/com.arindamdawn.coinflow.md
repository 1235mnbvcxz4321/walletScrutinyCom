---
wsId: 
title: "Coinflow - Exchange Bitcoin and Altcoins instantly"
altTitle: 
authors:

users: 100
appId: com.arindamdawn.coinflow
released: 2017-07-26
updated: 2017-07-27
version: "1.0.1"
stars: 0.0
ratings: 
reviews: 
size: 1.6M
website: 
repository: 
issue: 
icon: com.arindamdawn.coinflow.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "ShiftBuy. Cryptocurrency exchange in two clicks."
altTitle: 
authors:

users: 500
appId: com.unicorn.shiftbuy
released: 2020-01-03
updated: 2020-04-21
version: "0.2.7.2"
stars: 4.4
ratings: 14
reviews: 10
size: 7.7M
website: 
repository: 
issue: 
icon: com.unicorn.shiftbuy.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "0.2.7.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Perfect E-currency Exchange"
altTitle: 
authors:

users: 1000
appId: perfect.currency.exchange
released: 2019-07-22
updated: 2019-09-07
version: "v8.0"
stars: 4.2
ratings: 11
reviews: 4
size: 6.7M
website: 
repository: 
issue: 
icon: perfect.currency.exchange.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "v8.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



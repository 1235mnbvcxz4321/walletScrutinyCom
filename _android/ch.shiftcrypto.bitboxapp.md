---
wsId: 
title: "BitBoxApp"
altTitle: 
authors:

users: 1000
appId: ch.shiftcrypto.bitboxapp
released: 2020-07-13
updated: 2021-08-03
version: "android-4.29.0"
stars: 4.6
ratings: 53
reviews: 25
size: 56M
website: https://shiftcrypto.ch/app
repository: https://github.com/digitalbitbox/bitbox-wallet-app
issue: 
icon: ch.shiftcrypto.bitboxapp.png
bugbounty: 
verdict: nowallet
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: ShiftCryptoHQ
providerLinkedIn: shift-crypto
providerFacebook: Shiftcrypto
providerReddit: 

redirect_from:

---


The description of this app reads:

> A BitBox02 hardware wallet is required.

so we assume that this app does not manage private keys or send transactions if
not approved via the hardware wallet. It itself is **not a wallet**.

---
wsId: 
title: "뉴드림이엑스-거래소(암호화폐, 비트코인,비트코인캐시,이더리움,라이트코인)"
altTitle: 
authors:

users: 5000
appId: com.ndex.web
released: 2018-10-10
updated: 2020-04-02
version: "1.1"
stars: 4.7
ratings: 10
reviews: 3
size: 1.6M
website: 
repository: 
issue: 
icon: com.ndex.web.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



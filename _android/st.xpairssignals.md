---
wsId: 
title: "Forex indicator signals and crypto currency (Free)"
altTitle: 
authors:

users: 100000
appId: st.xpairssignals
released: 2017-10-07
updated: 2020-02-21
version: "4.4"
stars: 4.2
ratings: 822
reviews: 475
size: 3.4M
website: 
repository: 
issue: 
icon: st.xpairssignals.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "4.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Rise Wallet"
altTitle: 
authors:

users: 1000
appId: com.risewallet.riseapp
released: 2019-03-08
updated: 2021-07-14
version: "1.4.0"
stars: 4.4
ratings: 37
reviews: 23
size: 5.6M
website: 
repository: 
issue: 
icon: com.risewallet.riseapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



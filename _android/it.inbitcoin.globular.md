---
wsId: 
title: "Globular"
altTitle: 
authors:

users: 10
appId: it.inbitcoin.globular
released: 2019-06-07
updated: 2020-01-26
version: "1.2.0"
stars: 5.0
ratings: 5
reviews: 
size: 7.7M
website: 
repository: 
issue: 
icon: it.inbitcoin.globular.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.2.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



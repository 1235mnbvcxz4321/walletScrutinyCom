---
wsId: 
title: "CoinUs - Crypto wallet. Bitcoin,Ethereum,Filecoin"
altTitle: 
authors:

users: 50000
appId: com.theblockchain.coinus.wallet
released: 2018-04-23
updated: 2021-07-16
version: "2.8.0"
stars: 4.1
ratings: 358
reviews: 224
size: 67M
website: 
repository: 
issue: 
icon: com.theblockchain.coinus.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



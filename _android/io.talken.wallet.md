---
wsId: Talken
title: "Talken -   Multi-chain NFT Wallet & Marketplace"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: io.talken.wallet
released: 2019-07-31
updated: 2021-08-06
version: "1.0.26"
stars: 4.9
ratings: 3597
reviews: 2812
size: 14M
website: https://talken.io/
repository: 
issue: 
icon: io.talken.wallet.png
bugbounty: 
verdict: custodial
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: Talken_
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


With this statement below from the providers [Play store description](https://play.google.com/store/apps/details?id=io.talken.wallet), it is clear that the user is not in control of the wallets private keys.

> Easy and secure wallet
> Easy wallet service without managing private keys and mnemonics.

Our Verdict: This "wallet" is custodial and therefor **not verifiable**


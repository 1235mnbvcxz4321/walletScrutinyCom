---
wsId: 
title: "QuickX Wallet"
altTitle: 
authors:

users: 1000
appId: com.QuickX
released: 2019-10-22
updated: 2020-07-31
version: "1.16.0"
stars: 4.7
ratings: 59
reviews: 36
size: 10M
website: 
repository: 
issue: 
icon: com.QuickX.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.16.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



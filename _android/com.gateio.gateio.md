---
wsId: gate.io
title: "gate.io - Popular Bitcoin&cryptocurrency Exchange"
altTitle: 
authors:
- danny
users: 500000
appId: com.gateio.gateio
released: 2017-11-03
updated: 2021-07-26
version: "3.0.3"
stars: 3.2
ratings: 5797
reviews: 4107
size: 186M
website: https://gate.io/
repository: 
issue: 
icon: com.gateio.gateio.jpg
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: gate_io
providerLinkedIn: 
providerFacebook: gateioglobal
providerReddit: GateioExchange

redirect_from:

---


> Gate.io is a reliable and secure crypto exchange for trading and investing bitcoin(...)

It's an exchange, so probably custodial.

Adding to that, Gate.io claims to use

> cold storage of the majority of users’ digital assets

We mark this wallet as **custodial** and therefore **not verifiable.**
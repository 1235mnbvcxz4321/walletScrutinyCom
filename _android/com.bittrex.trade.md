---
wsId: bittrex
title: "Bittrex Global"
altTitle: 
authors:
- leo
users: 100000
appId: com.bittrex.trade
released: 2019-12-19
updated: 2021-08-13
version: "1.15.0"
stars: 3.3
ratings: 1822
reviews: 1203
size: 52M
website: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.png
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: BittrexGlobal
providerLinkedIn: 
providerFacebook: BittrexGlobal
providerReddit: 

redirect_from:

---


This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.

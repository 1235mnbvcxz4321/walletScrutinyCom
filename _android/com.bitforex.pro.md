---
wsId: 
title: "BitForex Pro"
altTitle: 
authors:

users: 50000
appId: com.bitforex.pro
released: 2020-07-19
updated: 2020-07-20
version: "2.0.0"
stars: 2.3
ratings: 363
reviews: 262
size: 29M
website: 
repository: 
issue: 
icon: com.bitforex.pro.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "F1EX"
altTitle: 
authors:

users: 50
appId: com.foxone.exchange
released: 2019-03-18
updated: 2019-05-20
version: "1.6.2"
stars: 0.0
ratings: 
reviews: 
size: 8.4M
website: 
repository: 
issue: 
icon: com.foxone.exchange.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.6.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



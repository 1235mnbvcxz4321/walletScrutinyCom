---
wsId: 
title: "SKG 암호화폐 지갑(비트코인, 이더리움 등) ERC20"
altTitle: 
authors:

users: 50
appId: com.wallet.crypto.skgapp
released: 2019-06-17
updated: 2019-07-22
version: "1.4.42"
stars: 0.0
ratings: 
reviews: 
size: 48M
website: 
repository: 
issue: 
icon: com.wallet.crypto.skgapp.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.4.42"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



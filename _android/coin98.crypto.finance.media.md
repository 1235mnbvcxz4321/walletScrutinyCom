---
wsId: 
title: "Coin98 Wallet - Crypto Wallet & DeFi Gateway"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: coin98.crypto.finance.media
released: 2019-12-19
updated: 2021-08-14
version: "10.1.0"
stars: 4.5
ratings: 11890
reviews: 7480
size: 62M
website: https://coin98.app/
repository: 
issue: 
icon: coin98.crypto.finance.media.png
bugbounty: 
verdict: nosource
date: 2021-04-19
signer: 
reviewArchive:


providerTwitter: coin98_wallet
providerLinkedIn: 
providerFacebook: Coin98Wallet
providerReddit: 

redirect_from:

---


The [Coin98 website](https://coin98.app/) claims to be a wallet from their description...

> Coin98 Wallet is the simplest and most secure crypto & bitcoin wallet to store, send, receive and manage your crypto assets, such as Bitcoin (BTC), Ethereum (ETH), TomoChain (TOMO), Ethereum-based ERC20 tokens. etc.

so it claims to manage BTC, it also claims users are in control of their private keys, hence the wallet claim of being non-custodial

> Taking full control of your assets with Biometric Authentication and Private Keys: managed by you, none of your personal data collected by the App.

with no source code repository listed or found...

Our verdict: This 'wallet' claims to be non-custodial but does not provide public source and therefore is **not verifiable**.

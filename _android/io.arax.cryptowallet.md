---
wsId: 
title: "COSS Wallet: Crypto One Stop Solution"
altTitle: 
authors:

users: 10000
appId: io.arax.cryptowallet
released: 2019-01-15
updated: 2019-11-25
version: "Varies with device"
stars: 3.6
ratings: 223
reviews: 157
size: Varies with device
website: 
repository: 
issue: 
icon: io.arax.cryptowallet.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "Varies with device"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Moonlet"
altTitle: 
authors:

users: 10000
appId: com.moonlet
released: 2020-04-21
updated: 2021-08-20
version: "1.5.12"
stars: 3.7
ratings: 510
reviews: 298
size: 37M
website: 
repository: 
issue: 
icon: com.moonlet.png
bugbounty: 
verdict: nobtc
date: 2020-12-14
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.moonlet/
---


This app appears to only support ETH tokens. Neither the description, nor the
website claim otherwise.

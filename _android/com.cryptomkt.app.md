---
wsId: 
title: "CryptoMarket"
altTitle: 
authors:

users: 50000
appId: com.cryptomkt.app
released: 2017-08-03
updated: 2020-07-20
version: "3.2.1"
stars: 2.8
ratings: 448
reviews: 318
size: 11M
website: 
repository: 
issue: 
icon: com.cryptomkt.app.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "3.2.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



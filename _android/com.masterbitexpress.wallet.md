---
wsId: 
title: "MasterBitExpress Bitcoin Wallet"
altTitle: 
authors:

users: 500
appId: com.masterbitexpress.wallet
released: 2018-04-21
updated: 2019-10-16
version: "1.1.1.10"
stars: 0.0
ratings: 
reviews: 
size: 14M
website: 
repository: 
issue: 
icon: com.masterbitexpress.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.1.1.10"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



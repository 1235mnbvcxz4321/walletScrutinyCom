---
wsId: TECHBANK
title: "TechBank"
altTitle: 
authors:
- leo
users: 10000
appId: com.beeone.techbank
released: 2019-07-14
updated: 2021-07-28
version: "4.9.3"
stars: 4.3
ratings: 635
reviews: 273
size: 83M
website: https://techbank.finance
repository: 
issue: 
icon: com.beeone.techbank.png
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.beeone.techbank/
---


They claim a million "members" and list big banks as their "acquirers" but there
is little to back those claims. 10k downloads on Play Store and eight ratings on
App Store don't look like a million users. The reviews on both platforms also
are abysmal.

Neither on the description nor their website do we find claims about this app being
a non-custodial wallet and as the name Tech**Bank** sounds rather custodial, we
file it as such and conclude this app is **not verifiable**.

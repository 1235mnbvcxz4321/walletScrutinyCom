---
wsId: Celsius
title: "Celsius - Safe Crypto Platform"
altTitle: 
authors:
- leo
users: 100000
appId: network.celsius.wallet
released: 2018-06-04
updated: 2021-08-09
version: "4.10.0"
stars: 4.4
ratings: 5908
reviews: 4151
size: 89M
website: https://celsius.network
repository: 
issue: 
icon: network.celsius.wallet.png
bugbounty: 
verdict: custodial
date: 2020-11-16
signer: 
reviewArchive:


providerTwitter: celsiusnetwork
providerLinkedIn: celsiusnetwork
providerFacebook: CelsiusNetwork
providerReddit: 

redirect_from:
  - /network.celsius.wallet/
---


> Use our fully functioning & secure crypto wallet & crypto lending platform to
  transfer and withdraw your Ethereum, Bitcoin, and over 30 other
  cryptocurrencies, free.

sounds like also a Bitcoin wallet.

The focus on "lending platform" doesn't make us hope for non-custodial parts to
it though ...

And sure enough, nowhere on the website can we find about this app being
non-custodial. As a custodial app, it is **not verifiable**.

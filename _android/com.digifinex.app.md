---
wsId: digifinex
title: "DigiFinex - Buy & Sell Bitcoin, Crypto Trading"
altTitle: 
authors:
- leo
users: 100000
appId: com.digifinex.app
released: 2018-06-26
updated: 2021-08-18
version: "2021.08.18"
stars: 3.9
ratings: 2698
reviews: 1599
size: 71M
website: https://www.digifinex.com
repository: 
issue: 
icon: com.digifinex.app.png
bugbounty: 
verdict: custodial
date: 2020-11-28
signer: 
reviewArchive:


providerTwitter: DigiFinex
providerLinkedIn: digifinex-global
providerFacebook: digifinex.global
providerReddit: DigiFinex

redirect_from:
  - /com.digifinex.app/
---


> DigiFinex is a world’s leading crypto finance exchange

doesn't sound like "wallet" is their primary business and as we can't find any
claims to the contrary, we have to assume this is a custodial offering and thus
**not verifiable**.

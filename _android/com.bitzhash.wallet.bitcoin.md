---
wsId: 
title: "Light Bitcoin Wallet"
altTitle: 
authors:

users: 50
appId: com.bitzhash.wallet.bitcoin
released: 2020-03-11
updated: 2020-04-28
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 8.8M
website: 
repository: 
issue: 
icon: com.bitzhash.wallet.bitcoin.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-05
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



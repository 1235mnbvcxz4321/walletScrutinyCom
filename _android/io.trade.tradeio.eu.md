---
wsId: 
title: "trade.io - Smarter crypto trading"
altTitle: 
authors:

users: 5000
appId: io.trade.tradeio.eu
released: 2019-07-22
updated: 2019-09-20
version: "1.0.0"
stars: 2.2
ratings: 25
reviews: 16
size: 3.6M
website: 
repository: 
issue: 
icon: io.trade.tradeio.eu.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "OneKey - Safe Crypto Wallet"
altTitle: 
authors:

users: 5000
appId: com.bixin.wallet.mainnet
released: 2020-09-16
updated: 2021-08-16
version: "2.7.0"
stars: 4.9
ratings: 299
reviews: 155
size: 69M
website: 
repository: 
issue: 
icon: com.bixin.wallet.mainnet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



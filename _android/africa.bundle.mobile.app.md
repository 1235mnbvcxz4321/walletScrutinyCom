---
wsId: africa.bundle
title: "Bundle Africa - Send/Get Cash, Buy & Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: africa.bundle.mobile.app
released: 2020-04-20
updated: 2021-07-15
version: "3.0.0"
stars: 4.4
ratings: 9600
reviews: 5941
size: 49M
website: https://bundle.africa/
repository: 
issue: 
icon: africa.bundle.mobile.app.png
bugbounty: 
verdict: custodial
date: 2021-04-19
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The [Bundle website](https://bundle.africa/) claims to be a wallet from their description...

> Send, receive, request naria and other ctyptocurrenices for free.

so it claims to manage BTC, however their is no evidence of the wallet being non-custodial, with no source code repository listed or found...

Our verdict: This 'wallet' is probably custodial but does not provide public source and therefore is **not verifiable**.

---
wsId: 
title: "FoxOne - BTC,ETH,EOS,Cosmos exchange & wallet"
altTitle: 
authors:

users: 1000
appId: com.fox.one
released: 2018-07-15
updated: 2021-01-08
version: "2.14.1"
stars: 4.8
ratings: 113
reviews: 89
size: 33M
website: 
repository: 
issue: 
icon: com.fox.one.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



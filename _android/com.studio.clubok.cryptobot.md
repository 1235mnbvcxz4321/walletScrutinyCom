---
wsId: 
title: "M U L T I B O T"
altTitle: 
authors:

users: 5000
appId: com.studio.clubok.cryptobot
released: 2019-02-27
updated: 2021-08-15
version: "2.8.2"
stars: 3.8
ratings: 57
reviews: 38
size: 5.0M
website: 
repository: 
issue: 
icon: com.studio.clubok.cryptobot.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: wazirx
title: "WazirX - Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:
- kiwilamb
- leo
users: 5000000
appId: com.wrx.wazirx
released: 2018-07-16
updated: 2021-08-19
version: "2.14.2"
stars: 4.2
ratings: 239700
reviews: 62505
size: 9.8M
website: https://wazirx.com
repository: 
issue: 
icon: com.wrx.wazirx.png
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:
- date: 2021-04-16
  version: "2.13.4"
  appHash: 
  gitRevision: ef345dd316ca2a0ef9a19f23680a94e1a42fd171
  verdict: nowallet

providerTwitter: WazirxIndia
providerLinkedIn: wazirx
providerFacebook: wazirx
providerReddit: 

redirect_from:

---


**Update 2021-08-09**: As this exchange allows holding your BTC in the app such
as sending and receiving them, it is usable as a wallet. A custodial wallet. As
such it is **not verifiable**.

This mobile app is an exchange based trading solution and is not a wallet.

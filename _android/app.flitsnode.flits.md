---
wsId: flits
title: "Flits: Masternode and staking wallet"
altTitle: 
authors:
- leo
users: 10000
appId: app.flitsnode.flits
released: 2019-04-05
updated: 2021-02-09
version: "4.4"
stars: 3.9
ratings: 2045
reviews: 1352
size: 14M
website: https://flitsnode.app
repository: 
issue: 
icon: app.flitsnode.flits.png
bugbounty: 
verdict: nosource
date: 2021-03-06
signer: 
reviewArchive:


providerTwitter: FlitsNode
providerLinkedIn: 
providerFacebook: flitsnode
providerReddit: 

redirect_from:

---


This app appears to support Bitcoin:

> Use Bitcoin or Etherium to get started and deploy a masternode in seconds with
  the Flits app!

and self-custodial:

> You are the only one who controls the keys! Your crypto funds are 100% safely
  stored inside your phone!

but can we verify those claims?

On their website they brag with:

> **311 BTC**<br>
  Total wallet value

so they have quite some insight into their users' financial activity. Certainly
not great if you want more privacy. Together with the other claim:

> **33706**<br>
  Users

we get to an average balance of 9mBTC or $440US.

What we can not find though is their source code, so the app is **not verifiable**.

---
wsId: 
title: "InfiPay"
altTitle: 
authors:

users: 1000
appId: io.infipay.android
released: 2020-10-28
updated: 2021-04-08
version: "1.1.2"
stars: 4.2
ratings: 50
reviews: 31
size: 49M
website: 
repository: 
issue: 
icon: io.infipay.android.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



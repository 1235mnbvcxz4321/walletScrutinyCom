---
wsId: 
title: "BitPocket"
altTitle: 
authors:
- kiwilamb
users: 100
appId: at.bitpocket.pos
released: 2016-07-08
updated: 2017-10-20
version: "1.1.8"
stars: 4.8
ratings: 5
reviews: 3
size: 4.7M
website: http://bitpocket.at/
repository: https://github.com/getbitpocket/bitpocket-mobile-app
issue: 
icon: at.bitpocket.pos.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-15
  version: "1.1.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


A Bitcoin wallet.

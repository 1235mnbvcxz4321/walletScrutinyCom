---
wsId: 
title: "ZedXe Wallet"
altTitle: 
authors:

users: 500
appId: com.zedXeWallet
released: 2020-05-07
updated: 2020-06-11
version: "1.8"
stars: 4.6
ratings: 31
reviews: 26
size: 39M
website: 
repository: 
issue: 
icon: com.zedXeWallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Chipper Cash - Send & Receive Money Across Africa"
altTitle: 
authors:
- kiwilamb
users: 1000000
appId: com.chippercash
released: 2018-07-07
updated: 2021-08-20
version: "1.9.10"
stars: 4.1
ratings: 33222
reviews: 20852
size: 85M
website: https://chippercash.com/
repository: 
issue: 
icon: com.chippercash.png
bugbounty: 
verdict: custodial
date: 2021-04-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Stated in their sites [support article](https://support.chippercash.com/en/articles/4750740-how-to-buy-sell-cryptocurrency-on-chipper-cash) 
"Currently it's not possible to send to or receive Bitcoin or Ethereum from external wallets"

Conclusion is that Chipper is a custodial wallet as funds are held by Chipper on behalf of the user.

---
wsId: 
title: "Pungo App"
altTitle: 
authors:
- leo
users: 1000
appId: cloud.peer2.pungo_wallet
released: 
updated: 2021-04-08
version: "1.33"
stars: 3.9
ratings: 11
reviews: 10
size: 8.4M
website: https://pungowallet.com
repository: 
issue: 
icon: cloud.peer2.pungo_wallet.png
bugbounty: 
verdict: defunct
date: 2021-06-02
signer: 
reviewArchive:
- date: 2020-07-29
  version: "1.33"
  appHash: 
  gitRevision: c08607be70f54ef29194d3b501f74ae09a9c9c6c
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-06-02**: Their website is gone. Their Play Store listing is gone.
We assume this wallet is no more.

This app's Google Play description reads:

> Pungo Wallet is focused on ease of use, security, and especially on
  maintaining the end user's financial independence as it’s a noncustodial
  wallet.

That's good. Let's see if this is just a claim ...

Their website `pungowallet.com` forwards to `pungo.io` which does not mention
"wallet" at all.

Their app id [can be found on
GitHub](https://github.com/search?l=XML&q=%22cloud.peer2.pungo_wallet%22&type=Code)
though: [ChainZilla/agamamobilefork](https://github.com/ChainZilla/agamamobilefork).

This repository has only two commits from two years ago, does not claim to be
associated with this app on Google Play and the app was updated on Google Play
recently. With this we conclude the search with the verdict: **not verifiable**.

---
wsId: 
title: "Bitget ：Crypto Trading & BitCoin Contract Platfom"
altTitle: 
authors:

users: 10000
appId: com.bitget.exchange
released: 2020-04-03
updated: 2021-08-08
version: "1.2.9"
stars: 4.0
ratings: 255
reviews: 215
size: 33M
website: 
repository: 
issue: 
icon: com.bitget.exchange.png
bugbounty: 
verdict: wip
date: 2021-08-11
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.2.7"
  appHash: 
  gitRevision: 3d4e0de7554c723d80c48c1d30caa7bad0af40aa
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-05**: This app is not on Play Store anymore.

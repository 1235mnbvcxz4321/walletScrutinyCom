---
wsId: 
title: "Paratica: Bitcoin Auto Trade"
altTitle: 
authors:

users: 5000
appId: com.paratica.dashboard
released: 2019-04-05
updated: 2019-04-05
version: "1.0"
stars: 4.5
ratings: 83
reviews: 33
size: 3.8M
website: 
repository: 
issue: 
icon: com.paratica.dashboard.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Bottrex - Cryptocurrency trading bot"
altTitle: 
authors:

users: 10000
appId: io.bottrex.starter
released: 2019-05-20
updated: 2021-08-20
version: "0.4.2"
stars: 3.7
ratings: 77
reviews: 45
size: 8.9M
website: 
repository: 
issue: 
icon: io.bottrex.starter.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: coinhako
title: "Coinhako: Buy Bitcoin, Crypto Wallet & Trading"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.coinhako
released: 2018-05-06
updated: 2021-08-13
version: "2.6.1"
stars: 4.3
ratings: 1318
reviews: 993
size: 49M
website: https://www.coinhako.com
repository: 
issue: 
icon: com.coinhako.png
bugbounty: 
verdict: custodial
date: 2021-04-23
signer: 
reviewArchive:


providerTwitter: coinhako
providerLinkedIn: coinhako
providerFacebook: coinhako
providerReddit: 

redirect_from:

---


Having a scan over the providers website and faq articles does not reveal any
claims regarding the management of private keys.
We would have to assume this wallet is custodial.

Our verdict: This “wallet” is probably custodial and therefore is **not verifiable**.

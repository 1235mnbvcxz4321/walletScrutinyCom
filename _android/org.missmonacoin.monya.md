---
wsId: 
title: "Monya - Monacoin & Altcoins Wallet"
altTitle: 
authors:

users: 1000
appId: org.missmonacoin.monya
released: 2018-01-02
updated: 2021-04-26
version: "3.10.4"
stars: 4.3
ratings: 96
reviews: 47
size: 4.4M
website: 
repository: 
issue: 
icon: org.missmonacoin.monya.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



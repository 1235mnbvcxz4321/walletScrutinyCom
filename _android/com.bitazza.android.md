---
wsId: 
title: "Bitazza - Bitcoin/Cryptocurrency Exchange & Wallet"
altTitle: 
authors:

users: 100000
appId: com.bitazza.android
released: 2020-09-29
updated: 2021-08-20
version: "1.8.0"
stars: 4.7
ratings: 1857
reviews: 355
size: 42M
website: 
repository: 
issue: 
icon: com.bitazza.android.jpg
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



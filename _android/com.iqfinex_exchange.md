---
wsId: 
title: "IQFinex Exchange"
altTitle: 
authors:

users: 100
appId: com.iqfinex_exchange
released: 2019-05-16
updated: 2019-10-30
version: "1.0.4"
stars: 1.7
ratings: 6
reviews: 3
size: 6.0M
website: 
repository: 
issue: 
icon: com.iqfinex_exchange.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: prizmbit
title: "Prizmbit wallet, p2p cryptocurrency exchange"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.prizmbit
released: 2019-06-28
updated: 2021-08-05
version: "1.4.24"
stars: 3.9
ratings: 523
reviews: 249
size: 15M
website: https://prizmbit.com/
repository: 
issue: 
icon: com.prizmbit.png
bugbounty: 
verdict: custodial
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: prizmbit
providerLinkedIn: 
providerFacebook: prizmbit
providerReddit: 

redirect_from:

---


There is no statement regarding how private keys are managed in the play store description or on the [providers website](https://prizmbit.com/) or FAQ.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.

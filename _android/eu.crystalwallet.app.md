---
wsId: 
title: "Crystal Wallet"
altTitle: 
authors:

users: 1000
appId: eu.crystalwallet.app
released: 2020-03-15
updated: 2021-07-27
version: "1.75"
stars: 4.9
ratings: 560
reviews: 523
size: 66M
website: 
repository: 
issue: 
icon: eu.crystalwallet.app.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



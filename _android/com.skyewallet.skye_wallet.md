---
wsId: 
title: "Skye Wallet: Buy & Sell Bitcoin and Cryptocurrency"
altTitle: 
authors:

users: 1000
appId: com.skyewallet.skye_wallet
released: 2021-05-01
updated: 2021-06-12
version: "2.7.1"
stars: 3.7
ratings: 27
reviews: 22
size: 65M
website: 
repository: 
issue: 
icon: com.skyewallet.skye_wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



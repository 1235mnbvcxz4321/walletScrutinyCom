---
wsId: 
title: "FlashTrade - Bitcoin, BCH, ETH, LTC on GDAX"
altTitle: 
authors:

users: 10
appId: co.ocdigital.flashtrade
released: 2018-01-02
updated: 2018-01-21
version: "1.2.2"
stars: 0.0
ratings: 
reviews: 
size: 3.6M
website: 
repository: 
issue: 
icon: co.ocdigital.flashtrade.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.2.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



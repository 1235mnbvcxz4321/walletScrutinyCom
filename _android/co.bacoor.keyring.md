---
wsId: keyring
title: "KEYRING PRO | Multichain Wallet Connect ETH, BSC"
altTitle: 
authors:

users: 1000
appId: co.bacoor.keyring
released: 2021-01-21
updated: 2021-08-13
version: "1.5.0"
stars: 4.7
ratings: 31
reviews: 19
size: 52M
website: 
repository: 
issue: 
icon: co.bacoor.keyring.png
bugbounty: 
verdict: wip
date: 2021-03-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



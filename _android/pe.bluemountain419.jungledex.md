---
wsId: 
title: "JungleDEX Wallet (Bitcoin,Ethereum,EOS,XRP)"
altTitle: 
authors:

users: 10
appId: pe.bluemountain419.jungledex
released: 2020-02-11
updated: 2020-02-20
version: "1.0.2"
stars: 0.0
ratings: 
reviews: 
size: 9.0M
website: 
repository: 
issue: 
icon: pe.bluemountain419.jungledex.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



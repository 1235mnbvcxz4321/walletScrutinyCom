---
wsId: SpotWalletapp
title: "Buy Bitcoin, cryptocurrency - Spot BTC wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.spot.spot
released: 2018-08-02
updated: 2021-08-11
version: "4.36.1.2698-1cc6e888"
stars: 4.3
ratings: 4130
reviews: 1994
size: 73M
website: https://www.spot-bitcoin.com
repository: 
issue: 
icon: com.spot.spot.png
bugbounty: 
verdict: nosource
date: 2020-11-16
signer: 
reviewArchive:


providerTwitter: spot_bitcoin
providerLinkedIn: spot-bitcoin
providerFacebook: spot.bitcoin
providerReddit: 

redirect_from:
  - /com.spot.spot/
---


On their website we read:

> **You control your Bitcoins.**
> 
> PayPal, Coinbase & Binance control your funds. We don't. You have entire
  control over your Bitcoins. We use the best technologies to ensure that your
  funds are always safe.

but as we cannot find any source code to check this claim, the wallet gets the
verdict **not verifiable**.

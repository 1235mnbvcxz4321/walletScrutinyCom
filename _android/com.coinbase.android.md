---
wsId: coinbaseBSB
title: "Coinbase – Buy & Trade Bitcoin, Ethereum & more"
altTitle: 
authors:
- leo
users: 10000000
appId: com.coinbase.android
released: 2013-03-01
updated: 2021-08-20
version: "9.37.2"
stars: 4.4
ratings: 502776
reviews: 148095
size: Varies with device
website: https://coinbase.com
repository: 
issue: 
icon: com.coinbase.android.jpg
bugbounty: 
verdict: custodial
date: 2019-10-30
signer: 
reviewArchive:


providerTwitter: coinbase
providerLinkedIn: coinbase
providerFacebook: Coinbase
providerReddit: CoinBase

redirect_from:
  - /coinbase/
  - /com.coinbase.android/
  - /posts/2019/10/coinbase/
  - /posts/com.coinbase.android/
---


The Coinbase app, not to be confused with [Coinbas Wallet](/coinbasewallet)
is one of the top two Bitcoin "wallets" on Google Play but beyond the
name, nothing indicates this app to be an actual wallet.

Historically Coinbase was an exchange and like almost all exchanges, they
allowed to hold Bitcoins in trading accounts. Later the Android app was released
and called a "wallet".

(On another historical note, Brian Armstrong, a co-founder of Coinbase did release
an [actual Bitcoin Wallet](https://github.com/barmstrong/bitcoin-android) back
[in June 2011](https://thenextweb.com/mobile/2011/07/06/bitcoin-payments-go-mobile-with-bitcoin-for-android/).
It was open source and downloaded the full blockchain to your phone.)

As the wallet setup does not involve a way to backup private keys, we assume those
private keys are under the sole control of Coinbase, making it
a custodial wallet or non-wallet.

Verdict: This app is **not verifiable**.

---
wsId: WalletofSatoshi
title: "Wallet of Satoshi"
altTitle: 
authors:
- leo
users: 100000
appId: com.livingroomofsatoshi.wallet
released: 2019-05-19
updated: 2021-08-10
version: "1.11.5"
stars: 3.8
ratings: 483
reviews: 265
size: 10M
website: http://www.walletofsatoshi.com
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: walletofsatoshi
providerLinkedIn: 
providerFacebook: walletofsatoshi
providerReddit: 

redirect_from:
  - /walletofsatoshi/
  - /com.livingroomofsatoshi.wallet/
  - /posts/2019/12/walletofsatoshi/
  - /posts/com.livingroomofsatoshi.wallet/
---


This is a custodial wallet according to their website's FAQ:

> It is a zero-configuration custodial wallet with a focus on simplicity and the
  best possible user experience.

and therefore **not verifiable**.

---
wsId: 
title: "Cryptomat. Cryptocurrency exchange in two clicks"
altTitle: 
authors:

users: 500
appId: com.unicorn.cryptomat
released: 2020-01-28
updated: 2020-04-24
version: "0.11"
stars: 4.8
ratings: 93
reviews: 89
size: 5.6M
website: 
repository: 
issue: 
icon: com.unicorn.cryptomat.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-01
  version: "0.11"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



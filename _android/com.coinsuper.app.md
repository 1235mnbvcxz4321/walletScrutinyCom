---
wsId: 
title: "Coinsuper"
altTitle: 
authors:

users: 10000
appId: com.coinsuper.app
released: 2018-04-24
updated: 2021-07-29
version: "2.5.4"
stars: 3.5
ratings: 145
reviews: 86
size: 13M
website: 
repository: 
issue: 
icon: com.coinsuper.app.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Dompet Bitcoin Indonesia"
altTitle: 
authors:
- leo
users: 500000
appId: co.id.dompet.btc.indonesia
released: 2017-01-15
updated: 2021-08-05
version: "Varies with device"
stars: 3.2
ratings: 4701
reviews: 2706
size: Varies with device
website: https://www.indodax.com
repository: 
issue: 
icon: co.id.dompet.btc.indonesia.png
bugbounty: 
verdict: custodial
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: indodax
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.id.dompet.btc.indonesia/
---


This app looks like a terminal for a website:

> Dompet Bitcoin Indonesia is a bitcoin wallet that is fully integrated with
  Indodax.com. You can now easily access every feature that is available in
  Indodax.com website through your smartphones only with a few clicks of your
  fingers.

So while this app claims that indodax.com is their website, that website does
not (anymore) link to this Play Store app but to
{% include walletLink.html wallet='android/id.co.bitcoin' %}. As both are provided by the same developer,
this looks like a predecessor-successor-thing.

As indodax.com is an exchange, we assume that like in many such cases the
app is a custodial offering and thus **not verifiable**.

---
wsId: 
title: "Coinorbis Wallet - Multi Cryptocurrency Wallet"
altTitle: 
authors:

users: 500
appId: com.coinorbis.wallet
released: 2018-11-27
updated: 2019-01-25
version: "1.2.1"
stars: 4.5
ratings: 8
reviews: 3
size: 9.0M
website: 
repository: 
issue: 
icon: com.coinorbis.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-13
  version: "1.2.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



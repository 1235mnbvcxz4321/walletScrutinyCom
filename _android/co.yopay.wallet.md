---
wsId: 
title: "YoPay Wallet"
altTitle: 
authors:

users: 50
appId: co.yopay.wallet
released: 
updated: 2020-06-01
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: 
repository: 
issue: 
icon: co.yopay.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "Varies with device"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: YouHodler
title: "YouHodler - Crypto and Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.youhodler.youhodler
released: 2019-07-11
updated: 2021-07-07
version: "2.17.0"
stars: 4.2
ratings: 1880
reviews: 720
size: 48M
website: https://youhodler.com
repository: 
issue: 
icon: com.youhodler.youhodler.png
bugbounty: 
verdict: custodial
date: 2020-06-20
signer: 
reviewArchive:


providerTwitter: youhodler
providerLinkedIn: youhodler
providerFacebook: YouHodler
providerReddit: 

redirect_from:
  - /com.youhodler.youhodler/
  - /posts/com.youhodler.youhodler/
---


This app is the interface to an exchange and might have a non-custodial part to
it but if so, it is not well advertised on their website and we assume it is
custodial and therefore **not verifiable**.

---
wsId: 
title: "Alpha Wallet by Ard"
altTitle: 
authors:

users: 10000
appId: com.bastionpay.alphawallet
released: 2019-05-12
updated: 2019-09-04
version: "v2.0.5"
stars: 3.8
ratings: 39
reviews: 14
size: 17M
website: 
repository: 
issue: 
icon: com.bastionpay.alphawallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "v2.0.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



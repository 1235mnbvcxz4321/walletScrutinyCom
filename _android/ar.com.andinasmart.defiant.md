---
wsId: 
title: "Defiant"
altTitle: 
authors:

users: 1000
appId: ar.com.andinasmart.defiant
released: 2019-11-19
updated: 2021-07-08
version: "1.2.7"
stars: 4.6
ratings: 46
reviews: 32
size: 27M
website: 
repository: 
issue: 
icon: ar.com.andinasmart.defiant.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: ZelCore
title: "Zelcore - Multi Asset Crypto Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.zelcash.zelcore
released: 2018-09-21
updated: 2021-08-11
version: "5.0.0"
stars: 3.9
ratings: 513
reviews: 352
size: 16M
website: https://zel.network/project/zelcore
repository: 
issue: 
icon: com.zelcash.zelcore.png
bugbounty: 
verdict: nosource
date: 2020-04-07
signer: 
reviewArchive:


providerTwitter: zelcash
providerLinkedIn: 
providerFacebook: 
providerReddit: ZelCash

redirect_from:
  - /com.zelcash.zelcore/
  - /posts/com.zelcash.zelcore/
---


This application claims on their Google Play description:

> Ultimate security provided with no personal user information being stored
> off-device.

and

> Single master seed backup (This will be the only seed you will ever need in
> the future for any coin we integrate)

which sounds like a non-custodial wallet but neither on Google Play nor on their
website do we find a link to the source code, so their claims are **not
verifiable**.

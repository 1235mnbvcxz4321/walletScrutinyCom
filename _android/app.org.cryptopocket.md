---
wsId: 
title: "Cryptopocket - Multi Currency Mobile Wallet"
altTitle: 
authors:

users: 1000
appId: app.org.cryptopocket
released: 2018-11-13
updated: 2019-09-16
version: "1.2.8"
stars: 4.1
ratings: 100
reviews: 60
size: 4.3M
website: 
repository: 
issue: 
icon: app.org.cryptopocket.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.2.8"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



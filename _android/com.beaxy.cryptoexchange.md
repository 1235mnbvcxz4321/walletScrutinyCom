---
wsId: 
title: "Beaxy Exchange"
altTitle: 
authors:

users: 10000
appId: com.beaxy.cryptoexchange
released: 2020-02-07
updated: 2021-06-15
version: "2.9"
stars: 4.1
ratings: 152
reviews: 69
size: 44M
website: 
repository: 
issue: 
icon: com.beaxy.cryptoexchange.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: 
title: "Moonstake Wallet"
altTitle: 
authors:

users: 10000
appId: io.moonstake.wallet
released: 2020-03-24
updated: 2021-08-17
version: "2.6.3"
stars: 4.7
ratings: 2199
reviews: 1100
size: 57M
website: 
repository: 
issue: 
icon: io.moonstake.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---



---
wsId: XcelPay
title: "XcelPay: Bitcoin, Crypto & Ethereum Wallet App"
altTitle: 
authors:
- leo
users: 50000
appId: com.XcelTrip.XcelPay
released: 2019-05-30
updated: 2021-07-28
version: "2.35.0"
stars: 4.4
ratings: 2908
reviews: 1487
size: 32M
website: http://www.xcelpay.io
repository: 
issue: 
icon: com.XcelTrip.XcelPay.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: XcelPayWallet
providerLinkedIn: in/xcelpaywallet
providerFacebook: xcelpay
providerReddit: 

redirect_from:

---


This wallet has no claim of being non-custodial in the app's description.

The one-star ratings over and over tell:

* there is a referral program, promising rewards
* the rewards are never reflected in the wallet
* funds cannot be sent to a different wallet
* SCAM

As a probably custodial app, it is **not verifiable**.

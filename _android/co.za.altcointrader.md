---
wsId: 
title: "AltCoinTrader - Secure easy crypto trading..."
altTitle: 
authors:

users: 100000
appId: co.za.altcointrader
released: 2020-05-06
updated: 2019-12-05
version: "1.0.1"
stars: 3.9
ratings: 494
reviews: 351
size: 1.2M
website: 
repository: 
issue: 
icon: co.za.altcointrader.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-08-02
  version: "1.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


